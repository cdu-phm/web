/* eslint-disable spaced-comment */
/// <reference types="vite/client" />

declare const BMap;
declare const BMAP_ANCHOR_TOP_LEFT;

declare const BMAP_NORMAL_MAP;
declare const BMAP_SATELLITE_MAP;
declare const BMAP_HYBRID_MAP;
declare const BMAP_ANCHOR_BOTTOM_RIGHT;

declare const BMapLib;

declare const APP_INFO: {
  pkg: {
    name: string;
    version: string;
    dependencies: Recordable<string>;
    devDependencies: Recordable<string>;
  };
  lastBuildTime: string;
};
