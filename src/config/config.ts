import { isProd } from '@/utils/is';


// export const apiUrl = isProd ? `${window.origin}/data` : 'http://127.0.0.1:65535';
export const apiUrl = isProd ? `${window.origin}/data` : 'http://101.42.172.70:65535';


// export const apiUrl1 = isProd ? `${window.origin}/data` : 'http://127.0.0.1:65535/data';
export const apiUrl1 = isProd ? `${window.origin}/data` : 'http://101.42.172.70:65535/data';

// export const fileUrl = isProd ? `${window.origin}/upload/` : 'http://127.0.0.1:65535/upload/';
export const fileUrl = isProd ? `${window.origin}/upload/` : 'http://101.42.172.70:65535/upload/';

export const getRealFileUrl = (url: string) => {
  if (url === null || url === undefined || url === '') {
    return '';
  }
  return url.indexOf('http://') !== -1 || url.indexOf('https://') !== -1 ? url : `${fileUrl}${url}`;
};
