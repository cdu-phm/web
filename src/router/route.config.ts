import loadable from '@loadable/component';
import fragmentLayout from '../layouts/FragmentLayout';
import securityLayout from '../layouts/SecurityLayout';

const routeConfigs = {
  securityLayout,
  fragmentLayout,
  login: loadable(() => import('@/pages/auth/Login/index')),
  logout: loadable(() => import('@/pages/auth/Logout/index')),
  redirect: loadable(() => import('@/pages/auth/Redirect/index')),
  404: loadable(() => import('@/pages/auth/404/index')),
  index: loadable(() => import('@/pages/Index/index')),
  userInfo: loadable(() => import('@/pages/user/UserInfo/index')),
  userProfile: loadable(() => import('@/pages/user/Profile/index')),
  roleInfo: loadable(() => import('@/pages/user/RoleInfo/index')),
  pageInfo: loadable(() => import('@/pages/user/PageInfo/index')),
  studentInfo: loadable(() => import('@/pages/teaching/StudentInfo/index')),
  majorInfo: loadable(() => import('@/pages/teaching/MajorInfo/index')),
  classInfo: loadable(() => import('@/pages/teaching/ClassInfo/index')),
  teachData: loadable(() => import('@/pages/teaching/TeachDataInfo/index')),
  teachDataType: loadable(() => import('@/pages/teaching/TeachDataTypeInfo/index')),
  // 雷达信息
  serverAreaInfo: loadable(() => import('@/pages/radar/ServerAreaInfo/index')),
  radarStationInfo: loadable(() => import('@/pages/radar/RadarStationInfo/index')),
  serverTypeInfo: loadable(() => import('@/pages/radar/ServerTypeInfo/index')),
  radarTypeInfo: loadable(() => import('@/pages/radar/RadarTypeInfo/index')),
  radarInfo: loadable(() => import('@/pages/radar/RadarInfo/index')),
  subSystemInfo: loadable(() => import('@/pages/radar/SubSystemInfo/index')),
  subExtensionInfo: loadable(() => import('@/pages/radar/SubExtensionInfo/index')),
  keyPartsTypeInfo: loadable(() => import('@/pages/radar/keyParts/KeyPartsTypeInfo/index')),
  keyPartsModelInfo: loadable(() => import('@/pages/radar/keyParts/KeyPartsModelInfo/index')),
  storageHomeInfo: loadable(() => import('@/pages/radar/keyParts/StorageHomeInfo/index')),
  // 系统信息
  logInfo: loadable(() => import('@/pages/system/LogInfo/index')),
  parameterInfo: loadable(() => import('@/pages/system/ParameterInfo/index')),
  parameterType: loadable(() => import('@/pages/system/ParameterType/index')),
  // 任务信息
  expertKnowledgeLibraryInfo: loadable(
    () => import('@/pages/task/ExpertKnowledgeLibraryInfo/index')
  ),
  intelligentLibraryInfo: loadable(() => import('@/pages/task/IntelligentLibraryInfo/index')),
  algorithmLibraryInfo: loadable(() => import('@/pages/task/AlgorithmLibraryInfo/index')),
  algorithmModelLibraryInfo: loadable(() => import('@/pages/task/AlgorithmModelLibraryInfo/index')),
  taskTypeInfo: loadable(() => import('@/pages/task/TaskTypeInfo/index')),
  taskInfo: loadable(() => import('@/pages/task/TaskInfo/index')),
  taskResult: loadable(() => import('@/pages/task/TaskResult/index')),
  // 维保
  repairMeasures: loadable(() => import('@/pages/repair/RepairMeasures/index')),
  repairDevice: loadable(() => import('@/pages/repair/RepairDevice/index')),
  repairMethod: loadable(() => import('@/pages/repair/RepairMethod/index')),
  repairEquipment: loadable(() => import('@/pages/repair/RepairEquipment/index')),
  repairProgramme: loadable(() => import('@/pages/repair/RepairProgramme/index')),
  repairRecord: loadable(() => import('@/pages/repair/RepairRecord/index')),
  faultPhenomenon: loadable(() => import('@/pages/repair/FaultPhenomenon/index')),
  faultRecord: loadable(() => import('@/pages/repair/FaultRecord/index')),
  technicalDocumentation: loadable(() => import('@/pages/repair/TechnicalDocumentation/index')),
  ensureMethod: loadable(() => import('@/pages/repair/EnsureMethod/index')),
  ensureBaseInfo: loadable(() => import('@/pages/repair/EnsureBaseInfo/index')),
  ensurePlan: loadable(() => import('@/pages/repair/EnsurePlan/index')),
  ensureRecord: loadable(() => import('@/pages/repair/EnsureRecord/index')),
  ensureType: loadable(() => import('@/pages/repair/EnsureType/index')),
  taskLevel: loadable(() => import('@/pages/repair/TaskLevel/index')),
  // 实时数据结构
  dataTypeInfo: loadable(() => import('@/pages/dataStructure/DataTypeInfo/index')),
  realFileStructureInfo: loadable(
    () => import('@/pages/dataStructure/RealFileStructureInfo/index')
  ),
  DisplayLocation: loadable(() => import('@/pages/dataStructure/DisplayLocation/index')),
  fileTypeInfo: loadable(() => import('@/pages/dataStructure/FileTypeInfo/index')),
  // 仿真中心
  dataSimulation: loadable(() => import('@/pages/simulation/DataSimulation/index')),
  indicatorSimulation: loadable(() => import('@/pages/simulation/IndicatorSimulation/index')),
  RULSimulation: loadable(() => import('@/pages/simulation/RULSimulation/index')),
  // 监控中心
  monitorIndex: loadable(() => import('@/pages/monitor/Index/index')),
  monitorRadar: loadable(() => import('@/pages/monitor/Radar/index')),
  monitorSubSystem: loadable(() => import('@/pages/monitor/SubSystem/index')),
  // 数据统计中心
  faultRadarType: loadable(() => import('@/pages/dataStatistics/FaultRadarType/index')),
  faultSubsystem: loadable(() => import('@/pages/dataStatistics/FaultSubsystem/index'))
};
export default routeConfigs;
