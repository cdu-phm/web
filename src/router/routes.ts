import {
  ClusterOutlined,
  DeploymentUnitOutlined,
  DesktopOutlined,
  FundViewOutlined,
  HddOutlined,
  RadarChartOutlined,
  ReadOutlined,
  SettingOutlined,
  TeamOutlined,
  ToolOutlined
} from '@ant-design/icons';
import { FC } from 'react';
import { getDevRoutes } from './development';
import routeConfigs from './route.config';
import { RouteConfig } from './type';

const routes: RouteConfig[] = [
  ...getDevRoutes(),
  {
    path: '/login',
    name: 'login',
    component: routeConfigs.login,
    meta: {
      title: '登录'
    }
  },
  {
    path: '/logout',
    name: 'logout',
    component: routeConfigs.logout,
    meta: {
      title: '退出'
    }
  },
  {
    path: '/user',
    name: 'user',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      title: '用户中心',
      icon: TeamOutlined
    },
    routes: [
      {
        path: '/user/userInfo',
        name: 'userInfo',
        exact: true,
        component: routeConfigs.userInfo,
        meta: {
          auth: true,
          title: '用户信息管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '角色绑定', router: 'resetPass' },
            { name: '角色绑定', router: 'roleBind' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/user/roleInfo',
        name: 'roleInfo',
        exact: true,
        component: routeConfigs.roleInfo,
        meta: {
          auth: true,
          title: '角色管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '权限绑定', router: 'pageBind' },
            { name: '删除', router: 'delete' }
          ]
        }
      }
    ]
  },
  {
    path: '/teaching',
    name: 'teaching',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      title: '教学中心',
      icon: ReadOutlined
    },
    routes: [
      {
        path: '/teaching/teachDataType',
        name: 'teachDataType',
        component: routeConfigs.teachDataType,
        meta: {
          auth: true,
          title: '资料类别管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/teaching/teachData',
        name: 'teachData',
        component: routeConfigs.teachData,
        meta: {
          auth: true,
          title: '教学资料管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '下载', router: 'download' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/teaching/student',
        name: 'student',
        component: routeConfigs.studentInfo,
        meta: {
          auth: true,
          title: '学生管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' },
            { name: '导入导出', router: 'importExport' },
            { name: '用户同步', router: 'sync' }
          ]
        }
      },
      {
        path: '/teaching/major',
        name: 'major',
        component: routeConfigs.majorInfo,
        meta: {
          auth: true,
          title: '专业管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/teaching/class',
        name: 'class',
        component: routeConfigs.classInfo,
        meta: {
          auth: true,
          title: '班级管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      }
    ]
  },
  {
    path: '/radar',
    name: 'radar',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      title: '装备中心',
      icon: HddOutlined
    },
    routes: [
      {
        path: '/radar/type',
        name: 'radarType',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '装备型号管理'
        },
        routes: [
          {
            path: '/radar/type/radarTypeInfo',
            name: 'radarTypeInfo',
            component: routeConfigs.radarTypeInfo,
            meta: {
              auth: true,
              title: '雷达型号',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/type/dataTypeInfo',
            name: 'dataTypeInfo',
            component: routeConfigs.dataTypeInfo,
            meta: {
              auth: true,
              title: '数据类型',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/type/fileTypeInfo',
            name: 'fileTypeInfo',
            component: routeConfigs.fileTypeInfo,
            meta: {
              auth: true,
              title: '文件类型',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/type/DisplayLocation',
            name: 'DisplayLocation',
            component: routeConfigs.DisplayLocation,
            meta: {
              auth: true,
              title: '显示位置',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/type/realFileStructureInfo',
            name: 'realFileStructureInfo',
            component: routeConfigs.realFileStructureInfo,
            meta: {
              auth: true,
              title: '实时文件结构',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' },
                { name: '状态数据新增', router: 'statusCreate' },
                { name: '状态数据编辑', router: 'statusEdit' },
                { name: '状态数据删除', router: 'statusDelete' }
              ]
            }
          }
        ]
      },
      {
        path: '/radar/structure',
        name: 'radarStructure',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '结构管理'
        },
        routes: [
          {
            path: '/radar/structure/subSystemInfo',
            name: 'subSystemInfo',
            component: routeConfigs.subSystemInfo,
            meta: {
              auth: true,
              title: '分系统',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/structure/subExtensionInfo',
            name: 'subExtensionInfo',
            component: routeConfigs.subExtensionInfo,
            meta: {
              auth: true,
              title: '分机',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/structure/keyPartsTypeInfo',
            name: 'keyPartsTypeInfo',
            component: routeConfigs.keyPartsTypeInfo,
            meta: {
              auth: true,
              title: '关重件名称',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/structure/keyPartsModelInfo',
            name: 'keyPartsModelInfo',
            component: routeConfigs.keyPartsModelInfo,
            meta: {
              auth: true,
              title: '关重件型号',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' },
                { name: '查看库存', router: 'loadStorage' },
                { name: '新增库存', router: 'createStorage' },
                { name: '编辑库存', router: 'editStorage' },
                { name: '删除库存', router: 'deleteStorage' }
              ]
            }
          },
          {
            path: '/radar/structure/storageHomeInfo',
            name: 'storageHomeInfo',
            component: routeConfigs.storageHomeInfo,
            meta: {
              auth: true,
              title: '库房',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' },
                { name: '查看库存', router: 'loadStorage' },
                { name: '新增库存', router: 'createStorage' },
                { name: '编辑库存', router: 'editStorage' },
                { name: '删除库存', router: 'deleteStorage' }
              ]
            }
          }
        ]
      },
      {
        path: '/radar/service',
        name: 'radarService',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '服务信息管理'
        },
        routes: [
          {
            path: '/radar/service/serverAreaInfo',
            name: 'serverAreaInfo',
            component: routeConfigs.serverAreaInfo,
            meta: {
              auth: true,
              title: '服务区域',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/service/radarStationInfo',
            name: 'radarStationInfo',
            component: routeConfigs.radarStationInfo,
            meta: {
              auth: true,
              title: '雷达站',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/radar/service/serverTypeInfo',
            name: 'serverTypeInfo',
            component: routeConfigs.serverTypeInfo,
            meta: {
              auth: true,
              title: '服务类型',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          }
        ]
      },
      {
        path: '/radar/radarInfo',
        name: 'radarInfo',
        component: routeConfigs.radarInfo,
        meta: {
          auth: true,
          title: '雷达信息管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' },
            { name: '查看结构', router: 'loadStructure' },
            { name: '编辑结构', router: 'editStructure' },
            { name: '关重件切换', router: 'switchKeyParts' },
            { name: '查看接入方式', router: 'loadAccessMethod' },
            { name: '新增接入方式', router: 'createAccessMethod' },
            { name: '编辑接入方式', router: 'editAccessMethod' },
            { name: '删除接入方式', router: 'deleteAccessMethod' }
          ]
        }
      }
    ]
  },
  {
    path: '/task',
    name: 'task',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      title: '任务中心',
      icon: ClusterOutlined
    },
    routes: [
      {
        path: '/task/base',
        name: 'taskBase',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '智能库管理'
        },
        routes: [
          {
            path: '/task/base/intelligentLibraryInfo',
            name: 'intelligentLibraryInfo',
            component: routeConfigs.intelligentLibraryInfo,
            meta: {
              auth: true,
              title: '智能库',
              buttonList: [
                { name: '下转', router: 'download' },
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/task/base/algorithmLibraryInfo',
            name: 'algorithmLibraryInfo',
            component: routeConfigs.algorithmLibraryInfo,
            meta: {
              auth: true,
              title: '算法库',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '绑定专家知识', router: 'bindEKL' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/task/base/algorithmModelLibraryInfo',
            name: 'algorithmModelLibraryInfo',
            component: routeConfigs.algorithmModelLibraryInfo,
            meta: {
              auth: true,
              title: '算法模型',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '绑定文件结构', router: 'bindDataStructure' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/task/base/expertKnowledgeLibraryInfo',
            name: 'expertKnowledgeLibraryInfo',
            component: routeConfigs.expertKnowledgeLibraryInfo,
            meta: {
              auth: true,
              title: '专家知识库',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          }
        ]
      },
      {
        path: '/task/taskTypeInfo',
        name: 'taskTypeInfo',
        component: routeConfigs.taskTypeInfo,
        meta: {
          auth: true,
          title: '任务类型管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/task/taskInfo',
        name: 'taskInfo',
        component: routeConfigs.taskInfo,
        meta: {
          auth: true,
          title: '任务信息管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/task/taskResult',
        name: 'taskResult',
        component: routeConfigs.taskResult,
        meta: {
          auth: true,
          title: '任务结果'
        }
      }
    ]
  },
  {
    path: '/ensureRepair',
    name: 'ensureRepair',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      title: '维保中心',
      icon: ToolOutlined
    },
    routes: [
      {
        path: '/ensureRepair/technicalDocumentation',
        name: 'technicalDocumentation',
        component: routeConfigs.technicalDocumentation,
        meta: {
          auth: true,
          title: '工卡管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '下载', router: 'download' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/ensureRepair/fault',
        name: 'ensureRepairFault',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '故障管理'
        },
        routes: [
          {
            path: '/ensureRepair/fault/phenomenon',
            name: 'faultPhenomenon',
            component: routeConfigs.faultPhenomenon,
            meta: {
              auth: true,
              title: '故障现象信息',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/fault/record',
            name: 'faultRecord',
            component: routeConfigs.faultRecord,
            meta: {
              auth: true,
              title: '故障记录',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '维修', router: 'service' },
                { name: '查看故障现象', router: 'loadFP' },
                { name: '绑定故障现象', router: 'bindFP' },
                { name: '删除', router: 'delete' }
              ]
            }
          }
        ]
      },
      {
        path: '/ensureRepair/repair',
        name: 'repair',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '维修管理'
        },
        routes: [
          {
            path: '/ensureRepair/repair/record',
            name: 'repairRecord',
            component: routeConfigs.repairRecord,
            meta: {
              auth: true,
              title: '维修记录'
            }
          },
          {
            path: '/ensureRepair/repair/programme',
            name: 'repairProgramme',
            component: routeConfigs.repairProgramme,
            meta: {
              auth: true,
              title: '维修方案',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '下载', router: 'download' },
                { name: '删除', router: 'delete' },
                { name: '查看方案明细', router: 'loadDetail' },
                { name: '新增方案明细', router: 'createDetail' },
                { name: '编辑方案明细', router: 'editDetail' },
                { name: '删除方案明细', router: 'deleteDetail' }
              ]
            }
          },
          {
            path: '/ensureRepair/repair/measures',
            name: 'repairMeasures',
            component: routeConfigs.repairMeasures,
            meta: {
              auth: true,
              title: '维修措施',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/repair/equipment',
            name: 'repairEquipment',
            component: routeConfigs.repairEquipment,
            meta: {
              auth: true,
              title: '维修器材',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/repair/repairMethod',
            name: 'repairMethod',
            component: routeConfigs.repairMethod,
            meta: {
              auth: true,
              title: '维修方式',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/repair/repairDevice',
            name: 'repairDevice',
            component: routeConfigs.repairDevice,
            meta: {
              auth: true,
              title: '维修设备',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          }
        ]
      },
      {
        path: '/ensureRepair/ensure',
        name: 'ensure',
        component: routeConfigs.fragmentLayout,
        meta: {
          auth: true,
          title: '保障管理'
        },
        routes: [
          {
            path: '/ensureRepair/ensure/taskLevel',
            name: 'taskLevel',
            component: routeConfigs.taskLevel,
            meta: {
              auth: true,
              title: '任务级别',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/ensure/baseInfo',
            name: 'ensureBaseInfo',
            component: routeConfigs.ensureBaseInfo,
            meta: {
              auth: true,
              title: '保障基础信息',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/ensure/ensureType',
            name: 'ensureType',
            component: routeConfigs.ensureType,
            meta: {
              auth: true,
              title: '保障类型',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/ensure/plan',
            name: 'ensurePlan',
            component: routeConfigs.ensurePlan,
            meta: {
              auth: true,
              title: '保障计划',
              buttonList: [{ name: '保障', router: 'maintain' }]
            }
          },
          {
            path: '/ensureRepair/ensure/ensureMethod',
            name: 'ensureMethod',
            component: routeConfigs.ensureMethod,
            meta: {
              auth: true,
              title: '保障方法',
              buttonList: [
                { name: '新增', router: 'create' },
                { name: '编辑', router: 'edit' },
                { name: '删除', router: 'delete' }
              ]
            }
          },
          {
            path: '/ensureRepair/ensure/ensureRecord',
            name: 'ensureRecord',
            component: routeConfigs.ensureRecord,
            meta: {
              auth: true,
              title: '保障记录'
            }
          }
        ]
      }
    ]
  },
  {
    path: '/simulation',
    name: 'simulation',
    component: routeConfigs.securityLayout,
    meta: {
      auth: true,
      icon: DeploymentUnitOutlined,
      title: '仿真中心'
    },
    routes: [
      {
        path: '/simulation/dataSimulation',
        name: 'dataSimulation',
        component: routeConfigs.dataSimulation,
        meta: {
          auth: true,
          title: '数据仿真'
        }
      },
      {
        path: '/simulation/indicatorSimulation',
        name: 'indicatorSimulation',
        component: routeConfigs.indicatorSimulation,
        meta: {
          auth: true,
          title: '量化指标仿真'
        }
      },
      {
        path: '/simulation/RULSimulation',
        name: 'RULSimulation',
        component: routeConfigs.RULSimulation,
        meta: {
          auth: true,
          title: 'RUL仿真'
        }
      }
    ]
  },
  {
    path: '/system',
    name: 'system',
    component: routeConfigs.securityLayout,
    meta: {
      title: '系统维护',
      auth: true,
      icon: SettingOutlined
    },
    routes: [
      {
        path: '/system/pageInfo',
        name: 'pageInfo',
        exact: true,
        component: routeConfigs.pageInfo,
        meta: {
          auth: true,
          title: '功能页面管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/system/logInfo',
        name: 'logInfo',
        component: routeConfigs.logInfo,
        meta: {
          auth: true,
          title: '日志审计'
        }
      },
      {
        path: '/system/parameterInfo',
        name: 'parameterInfo',
        component: routeConfigs.parameterInfo,
        meta: {
          auth: true,
          title: '系统参数信息管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      },
      {
        path: '/system/parameterType',
        name: 'parameterType',
        component: routeConfigs.parameterType,
        meta: {
          auth: true,
          title: '系统参数类型管理',
          buttonList: [
            { name: '新增', router: 'create' },
            { name: '编辑', router: 'edit' },
            { name: '删除', router: 'delete' }
          ]
        }
      }
    ]
  },
  {
    path: '/dataStatistics',
    name: 'dataStatistics',
    component: routeConfigs.securityLayout,
    meta: {
      title: '数据分析中心',
      auth: true,
      icon: FundViewOutlined
    },
    routes: [
      {
        path: '/dataStatistics/faultRadarType',
        name: 'faultRadarType',
        exact: true,
        component: routeConfigs.faultRadarType,
        meta: {
          auth: true,
          title: '雷达类型故障统计'
        }
      },
      {
        path: '/dataStatistics/faultSubsystem',
        name: 'faultSubsystem',
        exact: true,
        component: routeConfigs.faultSubsystem,
        meta: {
          auth: true,
          title: '分系统故障统计'
        }
      }
    ]
  },
  {
    path: '/monitor',
    name: 'monitor',
    component: routeConfigs.fragmentLayout,
    meta: {
      title: '监控中心',
      icon: RadarChartOutlined
    },
    routes: [
      {
        path: '/monitor/index',
        name: 'monitorIndex',
        component: routeConfigs.monitorIndex,
        meta: {
          title: '雷达地图'
        }
      },
      {
        path: '/monitor/radar',
        name: 'monitorRadar',
        component: routeConfigs.monitorRadar,
        meta: {
          showNavs: false,
          title: '雷达首页'
        }
      },
      {
        path: '/monitor/subSystem',
        name: 'monitorSubSystem',
        component: routeConfigs.monitorSubSystem,
        meta: {
          title: '分系统'
        }
      }
    ]
  },
  {
    path: '/',
    name: 'layout',
    component: routeConfigs.securityLayout,
    routes: [
      {
        name: 'index',
        path: '/',
        exact: true,
        component: routeConfigs.index,
        meta: {
          title: '首页',
          icon: DesktopOutlined
        }
      },
      {
        path: '/profile',
        name: 'userProfile',
        exact: true,
        component: routeConfigs.userProfile,
        meta: {
          title: '个人中心'
        }
      },
      {
        path: '/redirect',
        name: 'redirect',
        exact: true,
        component: routeConfigs.redirect,
        meta: {
          title: '重定向'
        }
      },
      {
        path: '*',
        component: routeConfigs['404'],
        meta: {
          title: '404'
        }
      }
    ]
  }
];

const getIconMap = (routes: RouteConfig[], map: Record<string, FC>) => {
  routes.forEach((i) => {
    const icon = i.meta?.icon;
    if (icon) {
      map[i.name] = icon;
    }
    if (i.routes) {
      getIconMap(i.routes, map);
    }
  });
};

export const iconMap: Record<string, FC> = {};
getIconMap(routes, iconMap);
export default routes;
