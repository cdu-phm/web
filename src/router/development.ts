import type { MenuInfo } from '@/store/menu';
import { isProd } from '@/utils/is';
import { RouteConfig } from './type';

const routes: RouteConfig[] = [
  // {
  //   path: '/dev',
  //   name: 'dev',
  //   component: routeConfigs.securityLayout,
  //   meta: {
  //     title: '开发中'
  //   },
  //   routes: [
  //     {
  //       path: '/dev/ensureType',
  //       name: 'ensureType',
  //       component: routeConfigs.ensureType,
  //       meta: {
  //         title: '保障类型'
  //       }
  //     }
  //   ]
  // },
];
/**
 * 获取开发中的路由信息
 * @returns {array} 开发中的路由信息
 */
export function getDevRoutes() {
  return isProd ? [] : routes;
  // return [];
  // return routes;
}

/**
 * 递归获取菜单信息
 * @param {array} mapRoutes 开发中的路由信息
 * @returns {array} 菜单信息
 */
function listNavigations(mapRoutes: typeof routes = routes): MenuInfo[] {
  return mapRoutes.map((i) => {
    return {
      key: i.name,
      name: i.meta.title,
      target: i.path as string,
      icon: undefined,
      children: i.routes ? listNavigations(i.routes) : undefined
    };
  });
}
/**
 * 获取开发模块的菜单信息
 * @returns {array} 开发模块的菜单信息
 */
export function getDevNavs() {
  return listNavigations(getDevRoutes());
}
