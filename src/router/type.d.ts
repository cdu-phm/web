import { Location } from 'history';
import type React from 'react';
import type { match, RouteComponentProps, SwitchProps } from 'react-router-dom';

export interface RouteConfigComponentProps<
  Params extends { [K in keyof Params]?: string } = Record<string, any>
> extends RouteComponentProps<Params> {
  // eslint-disable-next-line no-use-before-define
  route?: RouteConfig | undefined;
}
export interface RouteConfig {
  key?: React.Key | undefined;
  location?: Location<unknown> | undefined;
  component?: React.ComponentType<RouteConfigComponentProps<any>> | React.ComponentType | undefined;
  path?: string | string[] | undefined;
  exact?: boolean | undefined;
  strict?: boolean | undefined;
  routes?: RouteConfig[] | undefined;
  render?: ((props: RouteConfigComponentProps<any>) => React.ReactNode) | undefined;
  [propName: string]: any;
}
export interface MatchedRoute<
  Params extends { [K in keyof Params]?: string },
  TRouteConfig extends RouteConfig = RouteConfig
> {
  route: TRouteConfig;
  match: match<Params>;
}

export function matchRoutes<
  Params extends { [K in keyof Params]?: string },
  TRouteConfig extends RouteConfig = RouteConfig
>(routes: TRouteConfig[], pathname: string): MatchedRoute<Params, TRouteConfig>[];

export function renderRoutes(
  routes: RouteConfig[] | undefined,
  extraProps?: any,
  switchProps?: SwitchProps
  // eslint-disable-next-line no-undef
): JSX.Element;
