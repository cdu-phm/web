import 'antd/dist/antd.min.css';
import ReactDOM from 'react-dom';
import ThemeProvider from './design/ThemeProvider';
import BasicLayout from './layouts/BasicLayout';
import './permission';
import { RouteProvider } from './router';

ReactDOM.render(
  // <StrictMode>
  <ThemeProvider>
    <BasicLayout>
      <RouteProvider />
    </BasicLayout>
  </ThemeProvider>,
  // </StrictMode>,
  document.getElementById('root')
);
