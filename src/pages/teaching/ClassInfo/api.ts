import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { ClassModel } from '../data';
/**
 * 分页获取班级信息
 * @param data
 * @returns
 */
export function getClassList(data: PageParams<{ majorId?: string }>) {
  return request.list('/pc/student/v1/classByMajorId', data);
}

/**
 * 添加班级信息
 * @param data
 * @returns
 */
export function addClassInfo(data: ClassModel) {
  return request.post('/pc/student/v1/class', data, { success: '班级信息新增成功' });
}

/**
 * 编辑班级信息
 * @param data
 * @returns
 */
export function editClassInfo(data: ClassModel) {
  return request.put('/pc/student/v1/class', data, { success: '班级信息编辑成功' });
}
/**
 * 删除班级信息
 * @param id
 * @returns
 */
export function delClassInfo(id: string) {
  return request.delete(
    '/pc/student/v1/class',
    { id },
    { confirm: { content: '确认删除当前班级信息' }, success: '班级信息删除成功' }
  );
}
