import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { ClassModel } from '../../data';
import { getMajorList } from '../../MajorInfo/api';
import { addClassInfo, editClassInfo } from '../api';

const InfoModal: ModalFC<{ data?: ClassModel }> = (props) => {
  const { data = {} as ClassModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<ClassModel>
      title='班级信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addClassInfo : editClassInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        request={getMajorList}
        placeholder='请选择专业信息'
        label='专业'
        name='majorId'
        fieldProps={{
          defaultSelectedOptions: [{ id: data.majorId, majorName: data.majorName }],
          fieldNames: { label: 'majorName', value: 'id' }
        }}
        rules={[{ required: true, message: '请选择专业信息' }]}
      />
      <BaseFormText
        label='编号'
        name='classId'
        placeholder='请输入班级编号'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入班级编号' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='名称'
        name='className'
        placeholder='请输入班级名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入班级名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
