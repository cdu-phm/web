import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { download } from '@/utils';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, DownloadOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { TeachDataModel } from '../data';
import { getList as getTypeList } from '../TeachDataTypeInfo/api';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const TeachDataInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string; typeId?: string }>({});

  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        params={searchInfo}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSelect
            request={getTypeList}
            fieldNames={{ label: 'name', value: 'id' }}
            allowClear
            defaultValue={searchInfo.typeId}
            style={{ width: 200 }}
            placeholder='请选择资料类别'
            onChange={(value) => {
              setSearchInfo({ ...searchInfo, typeId: value });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入资料名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '资料名称'
            },
            {
              dataIndex: 'dataTypeName',
              title: '资料类别'
            },
            {
              dataIndex: 'creator',
              title: '上传人'
            },
            {
              dataIndex: 'createDateTime',
              title: '上传时间',
              valueType: 'dateTime'
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='blue'
                      code='download'
                      icon={<DownloadOutlined />}
                      title='下载'
                      onClick={() => {
                        download(record.url, { filename: record.name });
                      }}
                    >
                      下载
                    </BaseAction>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<TeachDataModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default TeachDataInfo;
