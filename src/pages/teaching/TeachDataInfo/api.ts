import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TeachDataModel } from '../data';
/**
 * 分页获取资料信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string; typeId?: string }>) {
  return request.list('/pc​/student​/v1​/teachDatas', data);
}

/**
 * 添加资料信息
 * @param data
 * @returns
 */
export function addInfo(data: TeachDataModel) {
  return request.post('/pc​/student​/v1​/teachDatas', data, { success: '资料信息新增成功' });
}

/**
 * 编辑资料信息
 * @param data
 * @returns
 */
export function editInfo(data: TeachDataModel) {
  return request.put('/pc​/student​/v1​/teachDatas', data, { success: '资料信息编辑成功' });
}
/**
 * 删除资料信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc​/student​/v1​/teachDatas',
    { id },
    { confirm: { content: '确认删除当前资料信息' }, success: '资料信息删除成功' }
  );
}
