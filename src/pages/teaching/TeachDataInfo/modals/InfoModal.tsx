import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseFormUpload,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { TeachDataModel } from '../../data';
import { getList } from '../../TeachDataTypeInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: TeachDataModel }> = (props) => {
  const { data = {} as TeachDataModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isedit = !isEmpty(id);
  return (
    <BaseModalForm<TeachDataModel>
      title='教学资料信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editInfo : addInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        request={getList}
        label='资料类别'
        name='dataTypeId'
        fieldProps={{
          defaultSelectedOptions: [{ name: data.dataTypeName, id: data.dataTypeId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择教学资料类别'
        rules={[{ required: true, message: '请选择教学资料类别' }]}
      />
      <BaseFormText
        label='资料名称'
        name='name'
        placeholder='请输入教学资料名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入教学资料名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormUpload
        label='文件'
        name='url'
        placeholder='请上传教学资料文件'
        rules={[{ required: true, message: '请上传教学资料文件' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
