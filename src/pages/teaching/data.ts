import { BaseModel } from '@/models';

export interface StudentModel extends BaseModel {
  name: string;
  studentId: string;
  classId: string;
  className: string;
  majorId: string;
  majorName: string;
  remarks?: string;
}

export interface ClassModel extends BaseModel {
  classId: string;
  className: string;
  majorId: string;
  majorName: string;
  remarks?: string;
}

export interface MajorModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface TeachDataTypeModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface TeachDataModel extends BaseModel {
  name: string;
  url: string;
  dataTypeId: string;
  dataTypeName: string;
  remarks?: string;
}
