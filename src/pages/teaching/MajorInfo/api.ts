import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { MajorModel } from '../data';
/**
 * 分页获取专业信息
 * @param data
 * @returns
 */
export function getMajorList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/student/v1/majors', data);
}

/**
 * 添加专业信息
 * @param data
 * @returns
 */
export function addMajorInfo(data: MajorModel) {
  return request.post('/pc/student/v1/majors', data, { success: '专业信息新增成功' });
}

/**
 * 编辑专业信息
 * @param data
 * @returns
 */
export function editMajorInfo(data: MajorModel) {
  return request.put('/pc/student/v1/majors', data, { success: '专业信息编辑成功' });
}
/**
 * 删除专业信息
 * @param id
 * @returns
 */
export function delMajorInfo(id: string) {
  return request.delete(
    '/pc/student/v1/majors',
    { id },
    { confirm: { content: '确认删除当前专业信息' }, success: '专业信息删除成功' }
  );
}
