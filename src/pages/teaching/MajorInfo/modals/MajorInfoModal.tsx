import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { MajorModel } from '../../data';
import { addMajorInfo, editMajorInfo } from '../api';

const InfoModal: ModalFC<{ data?: MajorModel }> = (props) => {
  const { data = {} as MajorModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isedit = !isEmpty(id);
  return (
    <BaseModalForm<MajorModel>
      title='专业信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editMajorInfo : addMajorInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='编号'
        name='majorId'
        placeholder='请输入专业编号'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入专业编号' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='名称'
        name='majorName'
        placeholder='请输入专业名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入专业名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
