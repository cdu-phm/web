import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TeachDataTypeModel } from '../data';
/**
 * 分页获取教学资料类别信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/student/v1/teachDataTypes', data);
}

/**
 * 添加教学资料类别信息
 * @param data
 * @returns
 */
export function addInfo(data: TeachDataTypeModel) {
  return request.post('/pc/student/v1/teachDataTypes', data, {
    success: '教学资料类别信息新增成功'
  });
}

/**
 * 编辑教学资料类别信息
 * @param data
 * @returns
 */
export function editInfo(data: TeachDataTypeModel) {
  return request.put('/pc/student/v1/teachDataTypes', data, {
    success: '教学资料类别信息编辑成功'
  });
}
/**
 * 删除教学资料类别信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/student/v1/teachDataTypes',
    { id },
    { confirm: { content: '确认删除当前教学资料类别信息' }, success: '教学资料类别信息删除成功' }
  );
}
