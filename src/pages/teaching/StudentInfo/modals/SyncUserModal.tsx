import { BaseModalForm, ProFormSelect } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import useUserStore from '@/store/user';
import { syncUserStudent } from '../api';

const SyncUserModal: ModalFC<{ idList: string[] }> = (props) => {
  const { idList, ...rest } = props;
  const {
    userinfo: { area }
  } = useUserStore();
  return (
    <BaseModalForm
      {...rest}
      title='用户信息同步'
      width={560}
      onFinish={async (values) => {
        const { ok } = await syncUserStudent(idList, values.areaId);
        return ok;
      }}
    >
      <ProFormSelect
        label='服务区域'
        name='areaId'
        placeholder='请选择服务区域'
        rules={[{ required: true, message: '请选择服务区域' }]}
        fieldProps={{ mode: 'multiple' }}
        options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
      />
    </BaseModalForm>
  );
};

export default SyncUserModal;
