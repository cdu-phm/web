import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormDependency,
  ProFormInstance
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { Alert } from 'antd';
import { useRef } from 'react';
import { getClassList } from '../../ClassInfo/api';
import { StudentModel } from '../../data';
import { getMajorList } from '../../MajorInfo/api';
import { addStudentInfo, editStudentInfo } from '../api';

const InfoModal: ModalFC<{ data?: StudentModel }> = (props) => {
  const { data = {} as StudentModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  const formRef = useRef<ProFormInstance<StudentModel>>();

  return (
    <BaseModalForm<StudentModel>
      title='学生信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      formRef={formRef}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addStudentInfo : editStudentInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='学号'
        name='studentId'
        placeholder='请输入学生学号'
        readonly={!isadd}
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入学生学号' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='姓名'
        name='name'
        placeholder='请输入学生姓名'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入学生姓名' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormSelect
        request={getMajorList}
        label='专业'
        placeholder='请选择学生专业'
        name='majorId'
        fieldProps={{
          fieldNames: { label: 'majorName', value: 'id' },
          defaultSelectedOptions: [
            {
              id: data.majorId,
              majorName: data.majorName
            }
          ],
          onChange: () => {
            formRef.current?.setFieldsValue({ classId: undefined });
          }
        }}
        rules={[{ required: true, message: '请选择学生专业' }]}
      />
      <ProFormDependency name={['majorId']}>
        {({ majorId }) => (
          <BaseFormSelect
            label='班级'
            name='classId'
            request={getClassList}
            placeholder='请选择学生班级'
            rules={[{ required: true, message: '请选择学生班级' }]}
            hiddenItem={isEmpty(majorId)}
            hiddenRender={() => <Alert message='请先选择专业' type='warning' />}
            fieldProps={{
              params: { majorId },
              fieldNames: { label: 'className', value: 'id' },
              defaultSelectedOptions: [
                {
                  id: data.classId,
                  majorName: data.className
                }
              ]
            }}
          />
        )}
      </ProFormDependency>
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
