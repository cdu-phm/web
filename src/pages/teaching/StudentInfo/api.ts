import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { StudentModel } from '../data';
/**
 * 分页获取学生信息
 * @param data
 * @returns
 */
export function getStudentList(data: PageParams<any>) {
  return request.list('/pc/student/v1/students', data);
}

/**
 * 添加学生信息
 * @param data
 * @returns
 */
export function addStudentInfo(data: StudentModel) {
  return request.post('/pc/student/v1/students', data, { success: '学生信息新增成功' });
}

/**
 * 编辑学生信息
 * @param data
 * @returns
 */
export function editStudentInfo(data: StudentModel) {
  return request.put('/pc/student/v1/students', data, { success: '学生信息编辑成功' });
}
/**
 * 删除学生信息
 * @param id
 * @returns
 */
export function delStudentInfo(id: string) {
  return request.delete(
    '/pc/student/v1/students',
    { id },
    { confirm: { content: '确认删除当前学生信息' }, success: '学生信息删除成功' }
  );
}
/**
 * 学生信息导出
 * @param data
 * @returns
 */
export function exportStudent(data: { majorId?: string; classId?: string; studentIds: string[] }) {
  return request.post('/pc/user/v1/exportStudent', data, { responseType: 'blob' });
}

/**
 * 学生信息批量删除
 * @param studentIds
 * @returns
 */
export function deleteStudent(studentIds: string[]) {
  return request.post(
    '/pc/user/v1/deleteStudent',
    { studentIds },
    { confirm: { content: '确认批量删除当前选中的学生信息' }, success: '学生信息批量删除成功' }
  );
}

/**
 * 学生信息导入进度
 * @param studentIds
 * @returns
 */
export function getImportStatus() {
  return request.get<number>('/pc/user/v1/importStatus');
}

/**
 * 学生信息同步用户
 * @param studentIds
 * @returns
 */
export function syncUserStudent(studentIds: string[], areaId: string) {
  return request.post(
    '/pc/user/v1/synStudentStudent',
    { studentIds, areaId },
    { success: '同步成功' }
  );
}
