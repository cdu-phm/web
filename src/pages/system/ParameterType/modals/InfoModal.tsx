import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { addInfo, editInfo } from '../api';
import { Model } from '../data';

const InfoModal: ModalFC<{ data?: any }> = (props) => {
  const { onSuccess, data = {} as Model, ...rest } = props;
  const { id } = data;
  const isedit = Boolean(id);
  return (
    <BaseModalForm<Model>
      title='系统参数类型信息'
      initialValues={{ ...data }}
      labelCol={{ style: { width: 80 } }}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editInfo : addInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
      {...rest}
    >
      <BaseFormText
        label='名称'
        placeholder='请输入参数类型名称'
        name='chineseName'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入参数名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='代码标识'
        name='codeName'
        placeholder='请输入代码标识'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入代码标识' },
          {
            pattern: /[a-zA-Z]{1}[a-zA-Z0-9_-]?$/,
            message: '代码标识只能输入 大小写字母、数字或-_'
          }
        ]}
      />
      <BaseFormTextArea
        label='备注'
        name='remarks'
        fieldProps={{ maxLength: 240, showCount: true }}
      />
    </BaseModalForm>
  );
};

export default InfoModal;
