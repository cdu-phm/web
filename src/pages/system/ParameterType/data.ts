import { BaseModel } from '@/models';

export interface Model extends BaseModel {
  chineseName: string;
  codeName: string;
  remarks?: string;
}
