import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { Model } from './data';
/**
 * 分页获取参数类型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/parameter/v1/parameterType', data);
}

/**
 * 添加参数类型信息
 * @param data
 * @returns
 */
export function addInfo(data: Model) {
  return request.post('/pc/parameter/v1/parameterType', data, { success: '参数类型信息新增成功' });
}

/**
 * 编辑参数类型信息
 * @param data
 * @returns
 */
export function editInfo(data: Model) {
  return request.put('/pc/parameter/v1/parameterType', data, { success: '参数类型信息编辑成功' });
}
/**
 * 删除参数类型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/parameter/v1/parameterType',
    { id },
    { confirm: { content: '确认删除当前参数类型信息' }, success: '参数类型信息删除成功' }
  );
}
