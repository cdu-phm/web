import { BaseModel } from '@/models';

export interface Model extends BaseModel {
  parameterCode: string;
  parameterName: string;
  parameterTypeId: string;
  parameterTypeName: string;
  remarks?: string;
}
