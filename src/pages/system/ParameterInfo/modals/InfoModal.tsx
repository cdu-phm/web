import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { getList } from '../../ParameterType/api';
import { addInfo, editInfo } from '../api';
import { Model } from '../data';

const InfoModal: ModalFC<{ data?: Model }> = (props) => {
  const { onSuccess, data = {} as Model, ...rest } = props;
  const { id } = data;
  const isedit = Boolean(id);
  return (
    <BaseModalForm<Model>
      title='系统参数信息'
      initialValues={{ ...data }}
      labelCol={{ style: { width: 80 } }}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editInfo : addInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
      {...rest}
    >
      <BaseFormSelect
        request={getList}
        label='参数类别'
        name='parameterTypeId'
        placeholder='请选择系统参数类别'
        fieldProps={{
          defaultSelectedOptions: [
            { chineseName: data.parameterTypeName, id: data.parameterTypeId }
          ],
          fieldNames: { label: 'chineseName', value: 'id' }
        }}
        rules={[{ required: true, message: '请选择系统参数类别' }]}
      />
      <BaseFormText
        label='名称'
        name='parameterName'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入参数名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />

      <BaseFormText
        label='参数值'
        name='parameterCode'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入参数值' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea name='remarks' label='备注' />
    </BaseModalForm>
  );
};

export default InfoModal;
