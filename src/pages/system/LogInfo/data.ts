import { BaseModel } from '@/models';
import { Enum } from '@/utils/enum';

export interface LogModel extends BaseModel {
  type: string;
  content: string;
  time: string;
}

export const LogType = Enum({
  登录: 0,
  添加: 1,
  删除: 2,
  修改: 3
});
