import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { DatePicker, Radio, Space } from 'antd';
import moment from 'moment';
import { useRef, useState } from 'react';
import { getList } from './api';
import { LogModel, LogType } from './data';

const LogInfo = () => {
  const tableRef = useRef<ActionType>();
  const [search, setSearch] = useState({
    startTime: moment().format('yyyy-MM-DD 00:00:00'),
    endTime: moment().add(1, 'd').format('yyyy-MM-DD 00:00:00'),
    ip: '',
    type: ''
  });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        headerTitle={
          <Space wrap>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div style={{ flexShrink: 1, width: 80 }}>日志类别</div>
              <Radio.Group
                defaultValue={search.type}
                optionType='button'
                options={[{ label: '全部', value: '' }, ...LogType.list]}
                onChange={(e) => {
                  setSearch({ ...search, type: e.target.value });
                }}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div style={{ flexShrink: 1, width: 80 }}>操作IP</div>
              <BaseSearch
                placeholder='请输入IP查询'
                defaultValue={search.ip}
                onChange={(ip) => {
                  setSearch({ ...search, ip });
                }}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <div style={{ flexShrink: 1, width: 80 }}>日志时间</div>
              <DatePicker.RangePicker
                showTime={{ format: 'HH:mm:ss' }}
                format='YYYY-MM-DD HH:mm:ss'
                defaultValue={[moment(search.startTime), moment(search.endTime)]}
                onChange={(values, dateStrings) => {
                  const [startTime, endTime] = dateStrings;
                  setSearch({
                    ...search,
                    startTime,
                    endTime
                  });
                }}
              />
            </div>
          </Space>
        }
        params={search}
        actionRef={tableRef}
        request={getList}
        columns={
          [
            {
              dataIndex: 'logType',
              title: '日志类别',
              float: 'left'
            },
            {
              dataIndex: 'ipAddress',
              title: 'IP'
            },
            {
              dataIndex: 'createDateTime',
              title: '记录时间',
              valueType: 'dateTime'
            },
            {
              dataIndex: 'creator',
              title: '操作人'
            },
            {
              dataIndex: 'newData',
              title: '请求参数',
              valueType: 'jsonCode'
            },
            {
              dataIndex: 'oldData',
              title: '返回参数',
              valueType: 'jsonCode'
            }
          ] as ProColumns<LogModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default LogInfo;
