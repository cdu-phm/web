import { PageParams } from '@/models';
import { EnumValueType } from '@/utils/enum';
import { request } from '@/utils/request';
import { LogModel, LogType } from './data';

/**
 * 分页获取信息
 * @param data
 * @returns
 */
export function getList(
  data: PageParams<{
    startTime?: string;
    endTime?: string;
    ip?: string;
    type?: EnumValueType<typeof LogType.enum>;
  }>
) {
  return request.list<LogModel>('/pc/operation/v1/logs', data);
}
