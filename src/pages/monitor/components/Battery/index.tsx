import { fixHeight, fixWidth } from '@/pages/monitor/util';
import styled from 'styled-components';

const Battery = styled.div<{ percent: number }>`
  height: ${fixHeight(160)};
  width: ${fixWidth(100)};
  box-sizing: border-box;
  border-radius: 15px 15px 5px 5px;
  filter: drop-shadow(0 1px 3px rgba(0, 0, 0, 0.22));
  background: ${(props) => (props.percent > 0 ? '#80e5ee' : '#cbc956')};
  .battery-info {
    line-height: ${fixHeight(160)};
    font-size: ${fixHeight(24)};
    z-index: 1;
    text-align: center;
    color: #fff;
    font-weight: bold;
  }
  &::before {
    content: '';
    position: absolute;
    width: ${fixWidth(26)};
    height: ${fixHeight(10)};
    left: 50%;
    top: -1px;
    transform: translate(-50%, -10px);
    border-radius: 5px 5px 0 0;
    background: ${(props) => (props.percent > 0 ? '#80e5ee' : '#cbc956')};
    z-index: -1;
  }

  &::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: ${(props) => 100 - props.percent * 100}%;
    background: linear-gradient(0deg, #066fa2 0%, #06c0c7 100%);
    z-index: -1;
    border-radius: 5px 5px 5px 5px;
    box-shadow: 0 14px 28px rgba(33, 150, 243, 0), 0 10px 10px rgba(9, 188, 215, 0.08);
  }
`;
export default Battery;
