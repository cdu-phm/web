import useNowDate from '@/hooks/useNowDate';
import styled from 'styled-components';
import { fixHeight, fixWidth } from '../util';

const DateWrap = styled.div`
  .date {
    font-size: ${fixHeight(14)};
  }
  .hour {
    font-size: ${fixHeight(30)};
    margin-right: ${fixWidth(26)};
  }
`;
const NowDate = () => {
  const date = useNowDate();
  return (
    <DateWrap>
      <span className='hour'>{date.format('HH:mm:ss')}</span>
      <span className='date'>{date.format('YYYY-MM-DD')}</span>
    </DateWrap>
  );
};

export default NowDate;
