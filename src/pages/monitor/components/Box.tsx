import { Space } from 'antd';
import classNames from 'classnames';
import { CSSProperties, FC, ReactNode } from 'react';
import styled from 'styled-components';
import { fixHeight, fixWidth } from '../util';

const BoxContainer = styled.div`
  display: flex;
  flex-direction: column;
  color: #fff;
  border-radius: ${fixWidth(10)} ${fixWidth(10)};
  overflow: hidden;
  .box-header {
    display: flex;
    flex-shrink: 0;
    justify-content: space-between;
    padding: ${fixHeight(10)};
    background: #085b73;
    padding-left: ${fixWidth(20)};
    &--title {
      font-size: ${fixHeight(20)};
      font-family: header-title !important;
    }
  }
  .box-wrap {
    flex: 1;
    overflow: hidden;
    padding: ${fixHeight(10)};
    background: #064462;
  }
`;

export interface BoxProps {
  className?: string;
  title?: ReactNode;
  headerAction?: ReactNode[];
  bodyClass?: string;
  bodyStyle?: CSSProperties;
}

const Box: FC<BoxProps> = (props) => {
  const { className, title, headerAction = [], bodyClass, bodyStyle, children } = props;
  return (
    <BoxContainer className={className}>
      <div className='box-header'>
        <div className='box-header--title'>{title}</div>
        <div className='box-header--action'>
          <Space children={headerAction} />
        </div>
      </div>

      <div className={classNames(['box-wrap', bodyClass])} style={bodyStyle}>
        {children}
      </div>
    </BoxContainer>
  );
};

export default Box;
