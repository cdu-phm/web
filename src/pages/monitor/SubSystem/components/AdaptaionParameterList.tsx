import BaseTable from '@/components/base/BaseTable';
import { ActionType } from '@ant-design/pro-table';
import { FC, useRef } from 'react';
import styled from 'styled-components';
import { DataModel, getAdaptationParametersList } from '../../api';
import { fixHeight, fixWidth } from '../../util';

const TableWrap = styled(BaseTable)`
  .ant-card,
  .ant-table,
  &&&&& table {
    background: transparent;
    border: none;
  }

  &&&&&&&& tr,
  &&&&&&&& .ant-table-cell {
    background: transparent;
    border: none;
  }
  &&&&&&&& .ant-table-cell,
  &&&&&&&& {
    color: #fff;
    padding: 0;
    .cell {
      border: ${fixHeight(2)}; solid;
      color: #fff;
      height: ${fixHeight(40)};
      line-height: ${fixHeight(40)};
      max-height: 45px;
      border-image: linear-gradient(0deg, #048494, #00acff) 10 10;
      background: linear-gradient(90deg, #066a8e 0%, #005883 100%);
      margin: ${fixHeight(4)};
      text-align: center;
    }
  }
  .ant-table-cell-scrollbar {
    display: none;
  }
  .ant-table-pagination.ant-pagination {
    color: #fff;
    margin: ${fixHeight(8)};
  }
  &&& .ant-pagination-item,
  &&& .ant-pagination-item-link:not(.ant-pagination-item-active) {
    border: ${fixHeight(2)}; solid;
    background: linear-gradient(90deg, #066a8e 0%, #005883 100%);
    border-image: linear-gradient(0deg, #066a8e, #00acff) 10 10;
  }
  &&& .ant-pagination-jump-next .ant-pagination-item-link {
    border: none;
  }
  &&& input {
    background: linear-gradient(90deg, #066a8e 0%, #005883 100%);
    border-image: linear-gradient(0deg, #066a8e, #00acff) 10 10;
    color: #fff;
    width: ${fixWidth(70)};
  }
  &&& .ant-pagination-item:not(.ant-pagination-item-active),
  &&& .ant-pagination-item-link {
    span,
    a {
      color: #fff;
    }
  }
`;
const AdaptaionParameterList: FC<{
  radarId: string;
  times?: [string, string];
  item?: DataModel;
}> = (props) => {
  const { radarId, times: [startTime, endTime] = [], item = {} as DataModel } = props;
  const { EName: field, CName: title, unit } = item;
  const tableRef = useRef<ActionType>();
  return (
    <TableWrap
      actionRef={tableRef}
      options={false}
      rowKey='uuid'
      cardProps={{ bodyStyle: { padding: 0 } }}
      height={fixHeight(400)}
      pagination={{ showSizeChanger: false, showQuickJumper: true }}
      params={{ radarId, field, startTime, endTime }}
      request={field ? getAdaptationParametersList : undefined}
      columns={[
        {
          title: <div className='cell'>字段</div>,

          render: () => <div className='cell'>{field}</div>
        },
        {
          title: <div className='cell'>字段名称</div>,

          render: () => <div className='cell'>{title}</div>
        },
        {
          dataIndex: field,
          title: <div className='cell'>值</div>,
          render: (text) => (
            <div className='cell'>
              {text} {unit}
            </div>
          )
        },
        {
          dataIndex: 'uptime',
          title: <div className='cell'>更新时间</div>,
          valueType: 'dateTime',
          render: (text) => <div className='cell'>{text}</div>
        }
      ]}
    />
  );
};

export default AdaptaionParameterList;
