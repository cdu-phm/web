import menuItemActive from '@/assets/screen/radar/menu-item-active.png';
import menuItem from '@/assets/screen/radar/menu-item.png';
import { getSubSystem } from '@/pages/monitor/api';
import { history, useQuery } from '@/router';
import { useRequest } from '@/utils/request';
import { Layout } from 'antd';
import classNames from 'classnames';
import { FC } from 'react';
import styled from 'styled-components';
import { fixHeight, fixWidth } from '../../util';

const { Sider } = Layout;

const MenuWrap = styled.div`
  padding-top: ${fixHeight(8)};
`;
const MenuItem = styled.div`
  color: #fff;
  background-image: url('${menuItem}');
  background-repeat: no-repeat;
  text-align: center;
  background-size: 100% 100%;
  padding-left: 0 !important;
  padding-right: 0 !important;
  font-size: ${fixHeight(20)};
  height: ${fixHeight(57)};
  line-height: ${fixHeight(57)};
  cursor: pointer;
  & + & {
    margin-top: ${fixHeight(12)};
  }
  &:hover,
  &.menu-item-active {
    color: #fff !important;
    background-image: url('${menuItemActive}');
  }
`;
const TheMenu: FC<{ radarId: string }> = (props) => {
  const radarSystem = useRequest(getSubSystem, {
    defaultParams: [{ radarId: props.radarId }]
  });
  const query = useQuery();
  return (
    <Sider width={fixWidth(177)} style={{ background: 'transparent' }}>
      <div className='menu-wrap'>
        <MenuWrap>
          <MenuItem
            title='首页'
            key='index'
            className={classNames({
              'menu-item-active': history.location.pathname === '/monitor/radar'
            })}
            onClick={() => {
              if (history.location.pathname === '/monitor/radar') return;
              window.location.href = `/monitor/radar?radarId=${props.radarId}`;
            }}
          >
            首页
          </MenuItem>
          {radarSystem.data?.map((i) => {
            const subSystemName = query.get('subSystemName');
            return (
              <MenuItem
                title={i.name}
                key={i.id}
                className={classNames({
                  'menu-item-active': subSystemName === i.name
                })}
                onClick={() => {
                  if (subSystemName === i.name) return;
                  window.location.href = `/monitor/subSystem?radarId=${props.radarId}&subsystemId=${i.id}&subSystemName=${i.name}`;
                }}
              >
                {i.name}
              </MenuItem>
            );
          })}
        </MenuWrap>
      </div>
    </Sider>
  );
};

export default TheMenu;
