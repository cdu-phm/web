import title from '@/assets/font/title.ttf';
import bg from '@/assets/screen/index/bg.png';
import topbar from '@/assets/screen/index/topbar.png';
import earth from '@/assets/screen/radar/earth.png';
import BaseModal from '@/components/base/BaseModal';
import { history, useQuery } from '@/router';
import useUserStore from '@/store/user';
import { useRequest } from '@/utils/request';
import { FC } from 'react';
import styled from 'styled-components';
import { getRadarBaseInfo } from '../api';
import NowDate from '../components/NowDate';
import { fixHeight, fixWidth } from '../util';
import TheMenu from './TheMenu';

const LayoutWrap = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  background-image: url('${bg}');
  background-size: 100% 100%;
  font-size: ${fixHeight(16)};
  @font-face {
    font-family: 'header-title';
    src: url(${title});
  }
  .header {
    height: ${fixHeight(89)};
    background-image: url('${topbar}');
    background-size: 100% 100%;
    display: flex;
    justify-content: center;
    color: #fff;
    font-size: ${fixHeight(20)};
    position: relative;
    .title {
      line-height: ${fixHeight(89)};
      font-size: ${fixHeight(36)};
      font-family: header-title !important;
    }
    .topbar-left {
      width: ${fixWidth(420)};
      position: absolute;
      bottom: ${fixHeight(4)};
      left: 0;
      padding-left: ${fixWidth(8)};
      line-height: ${fixHeight(30)};
    }
    .topbar-right {
      width: ${fixWidth(420)};
      position: absolute;
      bottom: ${fixHeight(8)};
      right: 0;
      text-align: right;
      padding-right: ${fixWidth(16)};
      font-size: ${fixHeight(13)};
      line-height: ${fixHeight(28)};
    }
  }
  .wrap {
    flex: 1;
    display: flex;
    overflow: hidden;
    .sider {
      background: #064462;
      overflow: auto;
      margin-top: ${fixHeight(10)};
      padding-right: ${fixWidth(10)};
      border-radius: ${fixWidth(10)};
      .earth {
        background-image: url('${earth}');
        background-repeat: no-repeat;
        background-size: 100% 100%;
        width: ${fixWidth(179)};
        height: ${fixHeight(65)};
        line-height: ${fixHeight(65)};
        color: #fff;
        font-size: ${fixHeight(30)};
        text-align: center;
        cursor: pointer;
        padding-right: ${fixWidth(16)};
        &:hover {
          filter: grayscale(0.2);
        }
      }
    }
    .content {
      flex: 1;
      overflow: auto;
    }
  }
  *::-webkit-scrollbar {
    width: 6px;
    height: 6px;
    border-radius: 3px;
  }

  *::-webkit-scrollbar-track {
    background: transparent;
  }

  *::-webkit-scrollbar-thumb {
    background: rgba(10, 180, 237, 0.2);
  }

  *::-webkit-scrollbar-thumb:hover {
    background: rgba(20, 244, 199, 0.4);
  }
  .ant-modal-content {
    max-height: 90vh;
  }
`;

const ScreenLayout: FC = (props) => {
  const query = useQuery();
  const radar = useRequest(getRadarBaseInfo, {
    defaultParams: [{ radarId: query.get('radarId')! }],
    onSuccess(data) {
      const { area } = useUserStore.getState().userinfo;
      if (area.every((i) => i.areaId !== data.areaId)) {
        let seconds = 5;
        const modal = BaseModal.warning({
          title: '权限警告',
          content: `您的区域与 ${data.name} 雷达不相同，无法访问，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`,
          okText: '快速跳转',
          onOk() {
            window.location.href = '/monitor/index';
          }
        });
        const timer = setInterval(() => {
          seconds -= 1;
          modal.update({
            content: `您的区域与 ${data.name} 雷达不相同，无法访问，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`
          });
        }, 1000);
        setTimeout(() => {
          clearInterval(timer);
          modal.destroy();
          window.location.href = '/monitor/index';
        }, seconds * 1000);
      } else if (data.isTableData === 0) {
        if (data.status === 0) {
          let seconds = 5;
          const modal = BaseModal.warning({
            title: '雷达接入警告',
            content: `雷达 ${data.name} 暂未接入，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`,
            okText: '快速跳转',
            onOk() {
              window.location.href = '/monitor/index';
            }
          });
          const timer = setInterval(() => {
            seconds -= 1;
            modal.update({
              content: `雷达 ${data.name} 暂未接入，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`
            });
          }, 1000);
          setTimeout(() => {
            clearInterval(timer);
            modal.destroy();
            window.location.href = '/monitor/index';
          }, seconds * 1000);
        } else {
          let seconds = 5;
          const modal = BaseModal.warning({
            title: '雷达接入警告',
            content: `雷达 ${data.name} 暂无数据，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`,
            okText: '快速跳转',
            onOk() {
              window.location.href = '/monitor/index';
            }
          });
          const timer = setInterval(() => {
            seconds -= 1;
            modal.update({
              content: `雷达 ${data.name} 暂未无数据，${seconds}秒后自动返回雷达地图页面，或点击下方按钮快速跳转。`
            });
          }, 1000);
          setTimeout(() => {
            clearInterval(timer);
            modal.destroy();
            window.location.href = '/monitor/index';
          }, seconds * 1000);
        }
      }
    }
  });
  return (
    <LayoutWrap>
      <div className='header'>
        <div className='topbar-left'>
          <NowDate />
        </div>
        {radar.data && (
          <div className='title'>
            {radar.data?.areaName}
            {radar.data?.stationName}-{radar.data?.name}
          </div>
        )}
        <div className='topbar-right'>
          <div>
            {radar.data?.areaName}
            {radar.data?.stationName}
          </div>
          <div>{radar.data?.radarTypeName}</div>
        </div>
      </div>
      <div className='wrap'>
        <div className='sider'>
          <div
            className='earth'
            onClick={() => {
              history.push('/monitor/index');
            }}
          >
            地图
          </div>
          <TheMenu radarId={query.get('radarId')!} />
        </div>
        <div className='content'>{radar.data?.status === 1 && props.children}</div>
      </div>
    </LayoutWrap>
  );
};

export default ScreenLayout;
