import { PageParams } from '@/models';
import { request } from '@/utils/request';

export const pollingInterval = 60 * 1000 * 6;
export interface RadarBaseInfoResponse {
  id: string;
  name: string;
  stationName: string;
  radarTypeName: string;
  areaName: string;
  areaId: string;
  status: number;
  isTableData: number;
}
export const getRadarBaseInfo = (data: { radarId: string }) => {
  return request.get<RadarBaseInfoResponse>('/pc/radar/v1/raRadarsById', data);
};

export interface GetRealDataRequest {
  field: string;
  radarId: string;
}
export const getRealData = (data: GetRealDataRequest) => {
  return request.get<Record<string, { data: number; unit: string; status: number }>>(
    '/pc/screen/v1/realTimeFiled',
    data
  );
};

export const getRealTimeRadarScoreData = (data: { radarId: string }) => {
  return request.get<Record<string, number>>('/pc/screen/v1/realTimeRadarScoreData', data);
};

export const getRadarScoreHistoricalData = (data: {
  startTime?: string;
  endTime?: string;
  previousTime?: string;
  radarId: string;
  type?: '0' | '1' | '2';
}) => {
  return request.get('/pc/screen/v1/radarScoreHistoricalData', data);
};

export interface QueryParams {
  startTime?: string;
  endTime?: string;
  previousTime?: string;
  radarId: string;
  field: string;
  type?: '0' | '1' | '2';
}
export type GetHisResponse = {
  dateList: string[];
} & Record<string, { valueList: string[]; unit: string }>;
export const getHisData = (data: QueryParams) => {
  return request.get<GetHisResponse>('/pc/screen/v1/historicalData', data);
};
/**
 * 查询雷达分系统列表
 * @param data
 * @returns
 */
export const getSubSystem = (data: { radarId: string }) => {
  return request.get<{ id: string; name: string }[]>('/pc/screen/v1/subsystem', data);
};
export interface GetSubSystemDataRequest {
  subsystemId: string;
  radarId: string;
}
export interface DataModel {
  EName: string;
  unit: string;
  displayLocationName: string;
  CName: string;
  value: number | string;
  order: number;
  status: number;
  picture?: string;
}
export interface GetSubSystemDataResponse {
  time: string;
  importData: DataModel[];
  showData: { displayName: string; order: number; list: DataModel[] }[];
  status: string;
  pictureStatus?: string;
}
/**
 * 查询分系统数据
 * @param data
 * @returns
 */
export const getSubSystemData = (data: GetSubSystemDataRequest) => {
  return request.get<GetSubSystemDataResponse>('/pc/screen/v1/subsystemData', data);
};
/**
 * 查询分系统月状态
 * @param data
 * @returns
 */
export const getSubSystemMonthStatusData = (data: {
  radarId: string;
  subsystemId: string;
  time: string;
}) => {
  return request.get<Record<string, number>>('/pc/screen/v1/subsystemMonthlyStatus', data);
};
export interface CalibrationItem {
  cname: string;
  ename: string;
  unit: string;
  picture?: string;
  expectData: string;
  actualData: string;
}
export interface GetSystemCalibrationResponse {
  time: string;
  importantParameters?: CalibrationItem[];
  other?: CalibrationItem[];
  kd?: CalibrationItem[];
  cw?: CalibrationItem[];
  rfd?: CalibrationItem[];
}

export interface EnvItem {
  unit: string | null;
  value: number;
  status: number;
}
export interface GetEnvironmentalParametersResponse {
  RoomHumi: EnvItem;
  RoomTemp: EnvItem;
  XMTR_AIR_TEMP: EnvItem;
  XMTR_AIR_HUMIDITY: EnvItem;
  RADOME_TEMP: EnvItem;
  RADOME_HUMIDITY: EnvItem;
}
/**
 * 查询环境参数
 * @param data
 * @returns
 */
export const getEnvironmentalParametersData = (data: { radarId: string; field?: string }) => {
  return request.get<GetEnvironmentalParametersResponse>(
    '​/pc​/screen​/v1​/environmentalParameters',
    data
  );
};

export interface GetSubsystemStatusResponse {
  roomHumi: string;
  roomTemp: string;
}

/**
 * 查询雷达分系统状态
 * @param data
 * @returns
 */
export const getSubsystemStatus = (data: { radarId: string }) => {
  return request.get<
    { radar: { score: number; status: number } } & Record<
      string,
      { picture: string; status: number }
    >
  >('/pc/screen/v1/fixedSubSystemStatus', data);
};

/**
 * 查询业务过程数据
 * @param data
 * @returns
 */
export const getBusinessProcess = (data: { radarId: string }) => {
  return request.get<any[]>('/pc/screen/v1/businessProcess', data);
};
/**
 * 查询业务过程数据
 * @param data
 * @returns
 */
export const getAdaptationParametersList = (
  data: PageParams<{ field: string; radarId: string }>
) => {
  return request.list('/pc/screen/v1/adaptationParameters', data);
};
export interface PerformanceItem {
  unit: string;
  data: number;
  max: number;
  min: number;
  cName: string;
  displayName: string;
  displayPName: string;
}
export interface PerformanceResponse {
  MEASURED_VELOCITY4: PerformanceItem;
  TE: PerformanceItem;
  MEASURED_VELOCITY2: PerformanceItem;
  MEASURED_VELOCITY3: PerformanceItem;
  MEASURED_VELOCITY1: PerformanceItem;
  ElMeasured: PerformanceItem;
  AzMeasured: PerformanceItem;
  TE_V: PerformanceItem;
  time: { time: string };
}
/**
 * 查询雷达性能指标
 * @returns
 */
export const getPerformanceData = (data: { radarId: string }) => {
  return request.get<PerformanceResponse>('​/pc​/screen​/v1​/fixedRealTimeFiled', data);
};
