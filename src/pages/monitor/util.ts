export const fixHeight = (px: number) => {
  return `${(px / 1080) * 100}vh`;
};
export const fixWidth = (px: number) => {
  return `${(px / 1920) * 100}vw`;
};

export const colors = [
  {
    color: '#22E8FF',
    borderColor: '#2F7FB2'
  },
  {
    color: '#FF6A6D',
    borderColor: '#C0272A'
  },
  {
    color: '#D6BF6F',
    borderColor: '#8D7932'
  }
];
