import arrowRight from '@/assets/screen/radar/arrow-right.gif';
import dataItem from '@/assets/screen/radar/data-item.png';
import importData from '@/assets/screen/radar/import-data.png';
import rootEnvItemContentError from '@/assets/screen/radar/root-env-item-content-error.png';
import rootEnvItemContent from '@/assets/screen/radar/root-env-item-content.png';
import rootEnvItemLabel from '@/assets/screen/radar/root-env-item-label.png';
import statusBg from '@/assets/screen/radar/status-bg.png';
import BaseEchart from '@/components/base/BaseEchart';
import { getRealFileUrl } from '@/config/config';
import { useQuery } from '@/router';
import useScreenStore from '@/store/screen';
import { useRequest } from '@/utils/request';
import classNames from 'classnames';
import { graphic } from 'echarts';
import { useMemo } from 'react';
import styled from 'styled-components';
import {
  getEnvironmentalParametersData,
  getRealData,
  getSubsystemStatus,
  pollingInterval
} from '../api';
import Box from '../components/Box';
import ScreenLayout from '../Screen/Layout';
import { fixHeight, fixWidth } from '../util';
import EnsureProcess from './components/EnsureProcess';
import KeyPerformance from './components/KeyPerformance';
import LifetimePrediction from './components/LifetimePrediction';
import Performance from './components/Performance';

const ScreenContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  .box-left {
    width: ${fixWidth(1120)};
    margin-left: ${fixWidth(10)};
    > * {
      margin: ${fixHeight(10)} 0;
    }
  }
  .box-right {
    width: ${fixWidth(580)};
    flex-shrink: 0;
    margin: 0 ${fixWidth(10)};
    position: relative;
    > * {
      margin: ${fixHeight(10)} 0;
    }
  }
  .box1,
  .box4,
  .box2,
  .box5,
  .box3,
  .box6 {
    height: ${fixHeight(316)};
  }
  .box1 {
    position: relative;
    padding: 0 ${fixWidth(40)};
    z-index: 0;

    &-top {
      height: 100%;
      display: flex;
      align-items: center;

      .device-list {
        display: flex;
        align-items: center;
        justify-content: space-around;
        flex: 1;
        height: 60%;
        .device {
          width: 16%;
          height: 100%;
          margin: ${fixHeight(10)};
          display: flex;
          flex-direction: column;
          color: #fff;
          .device-img {
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
            flex: 1;
          }
          .device-title {
            text-align: center;
            font-size: ${fixHeight(13)};
          }
          &.success {
            color: #17c817;
          }
          &.warning {
            color: #b3c935;
          }
          &.error {
            color: #c95835;
          }
          &.none {
            /*  */
            color: #fff;
            filter: grayscale(0.8);
          }
        }
        .arrow {
          width: ${fixWidth(128)};
          height: ${fixHeight(72)};
          background: url('${arrowRight}');
          background-size: 100% 100%;
          background-repeat: no-repeat;
          background-position: center center;
        }
      }
    }
    .import-data {
      position: absolute;
      left: 50%;
      bottom: 0;
      transform: translateX(-50%);
      background-size: 100% 100%;
      background-image: url('${importData}');
      height: ${fixHeight(54)};
      width: ${fixWidth(141)};
      .data-item {
        background-size: 100% 100%;
        background-image: url('${dataItem}');
        height: ${fixHeight(31)};
        line-height: ${fixHeight(31)};
        width: ${fixWidth(163)};
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        display: flex;
        text-align: center;
        color: #fff;
        &-label,
        &-content {
          width: 50%;
        }
      }
      .left-item {
        right: 100%;
      }
      .right-item {
        left: 100%;
      }
    }
  }

  .box3 {
    display: flex;
    flex-direction: column;
    .root-env {
      width: 60%;
      display: flex;
      justify-content: space-around;
      .root-env-item {
        display: flex;
        align-items: center;
        &-bg {
          background-size: 100% 100%;
          width: ${fixWidth(143)};
          height: ${fixHeight(178)};
        }
        &-value {
          margin-left: ${fixWidth(16)};
          width: ${fixWidth(100)};
          height: ${fixHeight(70)};
          position: relative;
          text-align: center;
          .value-label {
            line-height: ${fixHeight(30)};
            height: ${fixHeight(30)};
            background-image: url('${rootEnvItemLabel}');
            background-size: 100% 100%;
          }
          .value-content {
            height: ${fixHeight(30)};
            background-image: url('${rootEnvItemContent}');
            background-size: 100% 100%;
            line-height: ${fixHeight(30)};
            position: absolute;
            bottom: 0;
            width: 100%;
            &.error {
              background-image: url('${rootEnvItemContentError}');
            }
          }
        }
      }
    }
    .box3-wrap {
      display: flex;
      height: 100%;
      .env-list {
        flex: 1;
        margin-left: ${fixHeight(10)};
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        &--item {
          height: ${fixHeight(45)};
          line-height: ${fixHeight(45)};
          padding-left: ${fixWidth(12)};
          width: ${fixWidth(293)};
          border-radius: 0px ${fixHeight(22)} ${fixHeight(22)} 0px;
          background: linear-gradient(90deg, #5cbfb5 0%, #3491d0 100%);
          background-repeat: no-repeat;
          display: flex;
          &.error {
          }
          .value-label {
            width: 50%;
          }
          .value-content {
            width: 50%;
          }
        }
      }
    }
  }
`;
const StatusBox = styled.div`
  text-align: center;
  width: fit-content;
  transition: all ease-in-out 0.2s;

  color: #fff;
  :hover {
    .status-wrap {
      transform: scale(1.02);
    }
  }
  .status-wrap {
    position: relative;
    width: ${fixHeight(150)};
    height: ${fixHeight(150)};
    border-radius: 50%;
    border: 5px solid transparent;
    line-height: ${fixHeight(140)};
    font-size: ${fixHeight(32)};
    text-align: center;
    box-sizing: border-box;
    background-image: url('${statusBg}');
    background-size: 100% 100%;
    ::after {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: -1;
      transform: translate(-50%, -50%);
      content: ' ';
      background-color: transparent;
      width: 90%;
      height: 90%;
      border-radius: 50%;
    }
  }
  .status-text {
    margin-top: ${fixHeight(24)};
    font-size: ${fixHeight(32)};
  }
  &.success {
    color: #17c817;
  }
  &.warning {
    color: #b3c935;
  }
  &.error {
    color: #c95835;
  }
`;
const status = ['状态良好', '弱化', '故障', '未接入'];
const classnames = ['success', 'warning', 'error', 'none'];

const Index = () => {
  const query = useQuery();
  const radarId = query.get('radarId')!;
  const { score } = useScreenStore();
  const realData = useRequest(getRealData, {
    pollingInterval,
    defaultParams: [{ radarId, field: 'Frequency,PULSE_WIDTH' }]
  });
  const envData = useRequest(getEnvironmentalParametersData, {
    pollingInterval,
    defaultParams: [{ radarId }]
  });
  const subsystemStatus = useRequest(getSubsystemStatus, {
    pollingInterval,
    defaultParams: [{ radarId }]
  });
  const tempOption = useMemo(() => {
    const kd = [];
    const TP_value = Number(envData.data?.RoomTemp.value);
    const Gradient = [];
    // 刻度使用柱状图模拟，短设置1，长的设置3；构造一个数据
    for (let i = 0, len = 60; i <= len; i += 1) {
      if (i < 10) {
        kd.push('');
      } else if ((i - 10) % 10 === 0) {
        kd.push('-3');
      } else {
        kd.push('-1');
      }
    }
    // 中间线的渐变色和文本内容
    if (TP_value > 24) {
      Gradient.push(
        {
          offset: 0,
          color: '#93FE94'
        },
        {
          offset: 0.5,
          color: '#E4D225'
        },
        {
          offset: 1,
          color: '#E01F28'
        }
      );
    } else if (TP_value >= 14) {
      Gradient.push(
        {
          offset: 0,
          color: '#93FE94'
        },
        {
          offset: 1,
          color: '#E4D225'
        }
      );
    } else {
      Gradient.push({
        offset: 1,
        color: '#93FE94'
      });
    }
    return {
      grid: {
        top: 0,
        bottom: 20,
        left: '50%',
        x: 30,
        y: -110,
        x2: -120,
        y2: 20
      },
      title: {
        text: '温度计',
        show: false
      },
      yAxis: [
        {
          show: false,
          data: [],
          min: 0,
          max: 70,
          axisLine: {
            show: false
          }
        },
        {
          show: false,
          min: 0,
          max: 50
        },
        {
          type: 'category',
          data: [],
          position: 'left',
          offset: -80,
          axisLabel: {
            fontSize: 10,
            color: 'white'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        }
      ],
      xAxis: [
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -5,
          max: 80
        }
      ],
      series: [
        {
          name: '条',
          type: 'bar',
          // 对应上面XAxis的第一个对象配置
          xAxisIndex: 0,
          data: [
            {
              value: TP_value + 10
            }
          ],
          barWidth: 8,
          itemStyle: {
            color: new graphic.LinearGradient(0, 1, 0, 0, Gradient)
          },
          z: 2
        },
        {
          name: '白框',
          type: 'bar',
          xAxisIndex: 1,
          barGap: '-100%',
          data: [68],
          barWidth: 14,
          itemStyle: {
            color: '#0C2E6D',
            barBorderRadius: 50
          },
          z: 1
        },
        {
          name: '外框',
          type: 'bar',
          xAxisIndex: 2,
          barGap: '-100%',
          data: [74],
          barWidth: 24,
          itemStyle: {
            color: '#4577BA',
            barBorderRadius: 50
          },
          z: 0
        },
        {
          name: '圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 0,
          symbolSize: 20,
          itemStyle: {
            color: '#93FE94',
            opacity: 1
          },
          z: 2
        },
        {
          name: '白圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 1,
          symbolSize: 24,
          itemStyle: {
            color: '#0C2E6D',
            opacity: 1
          },
          z: 1
        },
        {
          name: '外圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 2,
          symbolSize: 30,
          itemStyle: {
            color: '#4577BA',
            opacity: 1
          },
          z: 0
        },
        {
          name: '刻度',
          type: 'bar',
          yAxisIndex: 0,
          xAxisIndex: 3,
          label: {
            show: true,
            position: 'left',
            distance: 10,
            color: 'white',
            fontSize: 14,
            formatter(params: any) {
              if (params.dataIndex > 60 || params.dataIndex < 10) {
                return '';
              }
              if ((params.dataIndex - 10) % 10 === 0) {
                return params.dataIndex - 10;
              }
              return '';
            }
          },
          barGap: '-100%',
          data: kd,
          barWidth: 1,
          itemStyle: {
            color: 'white',
            barBorderRadius: 120
          },
          z: 0
        }
      ]
    };
  }, [envData.data?.RoomTemp]);
  const humiOption = useMemo(() => {
    const kd = [];
    const TP_value = Number(envData.data?.RoomHumi.value);
    const Gradient = [];
    // 刻度使用柱状图模拟，短设置1，长的设置3；构造一个数据
    for (let i = 0, len = 110; i <= len; i += 1) {
      if (i < 10) {
        kd.push('');
      } else if ((i - 10) % 20 === 0) {
        kd.push('-3');
      } else if ((i - 10) % 4 === 0) {
        kd.push('-1');
      } else {
        kd.push('');
      }
    }
    // 中间线的渐变色和文本内容
    if (TP_value > 75) {
      Gradient.push(
        {
          offset: 0,
          color: '#93FE94'
        },
        {
          offset: 0.5,
          color: '#E4D225'
        },
        {
          offset: 1,
          color: '#E01F28'
        }
      );
    } else if (TP_value > 50) {
      Gradient.push(
        {
          offset: 0,
          color: '#93FE94'
        },
        {
          offset: 1,
          color: '#E4D225'
        }
      );
    } else {
      Gradient.push({
        offset: 1,
        color: '#93FE94'
      });
    }
    // 因为柱状初始化为0，温度存在负值，所以加上负值60和空出距离10
    return {
      grid: {
        top: 0,
        bottom: 20,
        left: '50%',
        x: 30,
        y: -110,
        x2: -120,
        y2: 20
      },
      title: {
        text: '湿度计',
        show: false
      },
      yAxis: [
        {
          show: false,
          data: [],
          min: 0,
          max: 135,
          axisLine: {
            show: false
          }
        },

        {
          show: false,
          min: 0,
          max: 50
        },

        {
          type: 'category',
          data: [],
          position: 'left',
          offset: -80,
          axisLabel: {
            fontSize: 10,
            color: 'white'
          },
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          }
        }
      ],
      xAxis: [
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -10,
          max: 80,
          data: []
        },
        {
          show: false,
          min: -5,
          max: 80
        }
      ],
      series: [
        {
          name: '条',
          type: 'bar',
          // 对应上面XAxis的第一个对)象配置
          xAxisIndex: 0,
          data: [
            {
              value: TP_value + 10
            }
          ],

          barWidth: 8,
          itemStyle: {
            normal: {
              color: new graphic.LinearGradient(0, 1, 0, 0, Gradient)
            }
          },
          z: 2
        },
        {
          name: '白框',
          type: 'bar',
          xAxisIndex: 1,
          barGap: '-100%',
          data: [130],
          barWidth: 14,
          itemStyle: {
            normal: {
              color: '#0C2E6D',
              barBorderRadius: 50
            }
          },
          z: 1
        },
        {
          name: '外框',
          type: 'bar',
          xAxisIndex: 2,
          barGap: '-100%',
          data: [136],
          barWidth: 24,
          itemStyle: {
            normal: {
              color: '#4577BA',
              barBorderRadius: 50
            }
          },
          z: 0
        },
        {
          name: '圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 0,
          symbolSize: 20,
          itemStyle: {
            normal: {
              color: '#93FE94',
              opacity: 1
            }
          },
          z: 2
        },
        {
          name: '白圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 1,
          symbolSize: 24,
          itemStyle: {
            normal: {
              color: '#0C2E6D',
              opacity: 1
            }
          },
          z: 1
        },
        {
          name: '外圆',
          type: 'scatter',
          hoverAnimation: false,
          data: [0],
          xAxisIndex: 2,
          symbolSize: 30,
          itemStyle: {
            normal: {
              color: '#4577BA',
              opacity: 1
            }
          },
          z: 0
        },
        {
          name: '刻度',
          type: 'bar',
          yAxisIndex: 0,
          xAxisIndex: 3,
          label: {
            normal: {
              show: true,
              position: 'left',
              distance: 10,
              color: 'white',
              fontSize: 14,
              formatter(params: any) {
                if (params.dataIndex > 111 || params.dataIndex < 10) {
                  return '';
                }
                if ((params.dataIndex - 10) % 20 === 0) {
                  return params.dataIndex - 10;
                }
                return '';
              }
            }
          },
          barGap: '-100%',
          data: kd,
          barWidth: 1,
          itemStyle: {
            normal: {
              color: 'white',
              barBorderRadius: 120
            }
          },
          z: 0
        }
      ]
    };
  }, [envData.data?.RoomHumi]);
  return (
    <ScreenLayout>
      <ScreenContainer>
        <div className='box-left'>
          <div className='box1'>
            <div className='box1-top'>
              <StatusBox
                className={
                  subsystemStatus.data && subsystemStatus.data.radar
                    ? classnames[subsystemStatus.data.radar.status]
                    : ''
                }
              >
                <div className='status-wrap'>{score}分</div>
                <div className='status-text'>
                  {subsystemStatus.data && subsystemStatus.data.radar
                    ? status[subsystemStatus.data.radar.status]
                    : '暂无数据'}
                </div>
              </StatusBox>
              <div className='device-list'>
                <div
                  className={classNames(
                    'device',
                    subsystemStatus.data && subsystemStatus.data.发射分系统
                      ? classnames[subsystemStatus.data.发射分系统.status]
                      : ''
                  )}
                >
                  <div
                    className='device-img'
                    style={{
                      backgroundImage: `url('${getRealFileUrl(
                        subsystemStatus.data?.发射分系统?.picture || ''
                      )}')`
                    }}
                  />
                  <div className='device-title'>
                    发射分系统{' '}
                    {subsystemStatus.data?.发射分系统 &&
                      status[subsystemStatus.data?.发射分系统?.status]}
                  </div>
                </div>
                <div className='arrow' />
                <div
                  className={classNames(
                    'device',
                    subsystemStatus.data && subsystemStatus.data.天线伺服分系统
                      ? classnames[subsystemStatus.data.天线伺服分系统.status]
                      : ''
                  )}
                >
                  <div
                    className='device-img'
                    style={{
                      backgroundImage: `url('${getRealFileUrl(
                        subsystemStatus.data?.天线伺服分系统?.picture || ''
                      )}')`
                    }}
                  />
                  <div className='device-title'>
                    天线伺服分系统{' '}
                    {subsystemStatus.data?.天线伺服分系统 &&
                      status[subsystemStatus.data?.天线伺服分系统?.status]}
                  </div>
                </div>
                <div className='arrow' />
                <div
                  className={classNames(
                    'device',
                    subsystemStatus.data && subsystemStatus.data.接收分系统
                      ? classnames[subsystemStatus.data.接收分系统.status]
                      : ''
                  )}
                >
                  <div
                    className='device-img'
                    style={{
                      backgroundImage: `url('${getRealFileUrl(
                        subsystemStatus.data?.接收分系统?.picture || ''
                      )}')`
                    }}
                  />
                  <div className='device-title'>
                    接收分系统{' '}
                    {subsystemStatus.data?.接收分系统 &&
                      status[subsystemStatus.data?.接收分系统?.status]}
                  </div>
                </div>
                <div className='arrow' />

                <div
                  className={classNames(
                    'device',
                    subsystemStatus.data && subsystemStatus.data.配电分系统
                      ? classnames[subsystemStatus.data.配电分系统.status]
                      : ''
                  )}
                >
                  <div
                    className='device-img'
                    style={{
                      backgroundImage: `url('${getRealFileUrl(
                        subsystemStatus.data?.配电分系统?.picture || ''
                      )}')`
                    }}
                  />
                  <div className='device-title'>
                    配电分系统{' '}
                    {subsystemStatus.data?.配电分系统 &&
                      status[subsystemStatus.data?.配电分系统?.status]}
                  </div>
                </div>
              </div>
            </div>
            <div className='import-data'>
              <div className='data-item left-item'>
                <div className='data-item-label'>工作频率</div>
                <div className='data-item-content'>
                  {realData.data?.Frequency?.data}
                  {realData.data?.Frequency?.unit}
                </div>
              </div>
              <div className='data-item right-item'>
                <div className='data-item-label'>脉冲宽度</div>
                <div className='data-item-content'>
                  {realData.data?.PULSE_WIDTH?.data}
                  {realData.data?.PULSE_WIDTH?.unit}
                </div>
              </div>
            </div>
          </div>
          <KeyPerformance className='box2' />
          <Box className='box3' title='运行环境'>
            <div className='box3-wrap'>
              <div className='root-env'>
                <div className='root-env-item'>
                  <div className='root-env-item-bg'>
                    <BaseEchart
                      option={tempOption as any}
                      style={{ width: '100%', height: '100%' }}
                    />
                  </div>
                  <div className='root-env-item-value'>
                    <div className='value-label'>温度</div>
                    <div className={classNames('value-content', { error: false })}>
                      {envData.data?.RoomTemp.value} {envData.data?.RoomTemp.unit}
                    </div>
                  </div>
                </div>
                <div className='root-env-item'>
                  <div className='root-env-item-bg'>
                    <BaseEchart
                      option={humiOption as any}
                      style={{ width: '100%', height: '100%' }}
                    />
                  </div>

                  <div className='root-env-item-value'>
                    <div className='value-label'>湿度</div>
                    <div className={classNames('value-content', { error: false })}>
                      {envData.data?.RoomHumi.value} {envData.data?.RoomHumi.unit}
                    </div>
                  </div>
                </div>
              </div>
              <div className='env-list'>
                <div className='env-list--item'>
                  <div className='value-label'>发射机温度</div>
                  <div className='value-content'>
                    {envData.data?.XMTR_AIR_TEMP.value} {envData.data?.XMTR_AIR_TEMP.unit}
                  </div>
                </div>
                <div className='env-list--item'>
                  <div className='value-label'>发射机湿度</div>
                  <div className='value-content'>
                    {envData.data?.XMTR_AIR_HUMIDITY.value} {envData.data?.XMTR_AIR_HUMIDITY.unit}
                  </div>
                </div>
                <div className='env-list--item'>
                  <div className='value-label'>天线罩温度</div>
                  <div className='value-content'>
                    {envData.data?.RADOME_TEMP.value} {envData.data?.RADOME_TEMP.unit}
                  </div>
                </div>
                <div className='env-list--item'>
                  <div className='value-label'>天线罩湿度</div>
                  <div className='value-content'>
                    {envData.data?.RADOME_HUMIDITY.value} {envData.data?.RADOME_HUMIDITY.unit}
                  </div>
                </div>
              </div>
            </div>
          </Box>
        </div>
        <div className='box-right'>
          <Performance className='box4' radarId={radarId} />

          <LifetimePrediction className='box5' radarId={radarId} />
          <EnsureProcess className='box6' radarId={radarId} />
        </div>
        {/* <CustomLine {...activeData} /> */}
      </ScreenContainer>
    </ScreenLayout>
  );
};

export default Index;
