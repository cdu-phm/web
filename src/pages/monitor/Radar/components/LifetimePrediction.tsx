import { getLifePrediction } from '@/pages/radar/RadarInfo/api';
import { useRequest } from '@/utils/request';
import { FC } from 'react';
import styled from 'styled-components';
import { pollingInterval } from '../../api';
import Battery from '../../components/Battery';
import Box from '../../components/Box';
import { fixHeight, fixWidth } from '../../util';

const LifeContainer = styled(Box)`
  .box-wrap {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    overflow: auto;
  }
`;
const LifeItem = styled.div`
  display: flex;
  width: 50%;
  align-items: center;
  padding: ${fixHeight(6)} ${fixWidth(6)};
`;
const LifeInfo = styled.div`
  flex: 1;
  padding-left: ${fixWidth(6)};
`;
const LifeInfoItem = styled.div`
  display: flex;
  width: 100%;
  span {
    width: 50%;
    font-size: ${fixHeight(14)};
  }
`;

const LifetimePrediction: FC<{ className?: string; radarId: string }> = (props) => {
  const { radarId, className } = props;
  const lifeData = useRequest(getLifePrediction, {
    pollingInterval,
    defaultParams: [{ radarId }]
  });
  return (
    <LifeContainer className={className} title='寿命预测'>
      {(lifeData.data || []).map((i) => (
        <LifeItem key={i.typeName}>
          <Battery percent={i.residualLife / (i.activeLife + i.residualLife)}>
            <div className='battery-info'>
              {((i.residualLife * 100) / (i.activeLife + i.residualLife)).toFixed(2)}%
            </div>
          </Battery>
          <LifeInfo>
            <LifeInfoItem>
              {i.typeName}
              {i.name}
            </LifeInfoItem>
            <LifeInfoItem>
              <span>总寿命</span>
              <span>{i.totalLife} h</span>
            </LifeInfoItem>
            <LifeInfoItem>
              <span>已使用寿命</span>
              <span>{i.activeLife} h</span>
            </LifeInfoItem>
            <LifeInfoItem>
              <span>预计剩余寿命</span>
              <span>{i.residualLife} h</span>
            </LifeInfoItem>
            <LifeInfoItem>
              <span>失效时间</span>
              <span>{i.failureTime}</span>
            </LifeInfoItem>
          </LifeInfo>
        </LifeItem>
      ))}
    </LifeContainer>
  );
};
export default LifetimePrediction;
