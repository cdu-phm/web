import keyItemActive from '@/assets/screen/radar/key-item-active.png';
import keyItemErrorActive from '@/assets/screen/radar/key-item-error-active.png';
import keyItemError from '@/assets/screen/radar/key-item-error.png';
import keyItem from '@/assets/screen/radar/key-item.png';
import { useQuery } from '@/router';
import useScreenStore from '@/store/screen';
import { useSetState } from 'ahooks';
import { Radio } from 'antd';
import classNames from 'classnames';
import { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { getRadarScoreHistoricalData, getRealData, getRealTimeRadarScoreData } from '../../api';
import Box from '../../components/Box';
import HisData from '../../components/HisData';
import { fixHeight, fixWidth } from '../../util';

const Wrap = styled(Box)`
  .select-type {
    .ant-radio-wrapper {
      color: #fff;
      font-size: ${fixHeight(14)};
      height: ${fixHeight(22)};
      .ant-radio {
        display: none;
      }
    }
    .ant-radio-wrapper-checked {
      border-radius: ${fixHeight(22)};
      background: #76c8ce;
    }
  }
  .box-wrap {
    padding: 0;
    height: 100%;
    overflow: hidden;
    display: flex;
    flex-direction: column;
  }
  .status-list {
    display: flex;
    justify-content: space-between;
    &--item {
      width: ${fixWidth(143)};
      height: ${fixHeight(75)};
      text-align: center;
      background-image: url('${keyItem}');
      background-repeat: no-repeat;
      background-size: 100% 100%;
      line-height: ${fixHeight(38)};
      font-size: ${fixHeight(14)};
      &.active {
        background-image: url('${keyItemActive}');
      }
      &.error {
        background-image: url('${keyItemError}');
        &.active {
          background-image: url('${keyItemErrorActive}');
        }
      }
      .label {
        padding: 0 ${fixWidth(10)};
      }
      .content {
        padding: 0 ${fixWidth(14)};
      }
    }
  }
  .status-chart {
    flex: 1;
    display: flex;
    overflow: hidden;
    flex-direction: column;
  }
`;

const data = [
  {
    title: '健康分数',
    key: 'radar_score',
    unit: '分',
    legend: false
  },
  {
    title: '发射机峰值功率',
    key: 'XMTR_PEAK_PWR',
    unit: 'KW',
    legend: false
  },
  {
    title: '水平通道相位噪声',
    key: 'PHASE_NOISE',
    unit: 'dBc/Hz',
    legend: false
  },
  {
    title: '垂直通道相位噪声',
    key: 'PHASE_NOISE_V',
    unit: 'dBc/Hz',
    legend: false
  },
  {
    title: '系统标定常数变化',
    key: 'SYSCAL_DELTA',
    unit: 'dB',
    legend: false
  },
  {
    title: '发射机功率调零',
    key: 'XMTR_PWR_MTR_ZERO',
    unit: 'W',
    legend: false
  },
  {
    title: '机房温度',
    key: 'RoomTemp',
    unit: '°C',
    legend: false
  },
  {
    title: '机房湿度',
    key: 'RoomHumi',
    unit: '%',
    legend: false
  }
];
const realDataKey = data
  .map((i) => i.key)
  .filter((i) => i !== '' && i !== 'radar_score')
  .join(',');
const KeyPerformance: FC<{ className?: string }> = (props) => {
  const { className } = props;
  const [type, setType] = useState<'0' | '1' | '2'>('0');
  const [activeData, setActiveData] = useState(data[0]);
  const query = useQuery();
  const [realData, setRealData] = useSetState<
    Record<string, { status: number; data: number; unit: string }>
  >({});
  useEffect(() => {
    let timer: number | undefined;
    const timerFn = () => {
      getRealData({
        field: realDataKey,
        radarId: query.get('radarId')!
      }).then((res) => {
        if (res.ok) {
          setRealData({ ...res.data });
        }
        console.log(res.data);
      });
      getRealTimeRadarScoreData({
        radarId: query.get('radarId')!
      }).then((res) => {
        if (res.ok) {
          if (res.data) {
            const { radar_score, status } = res.data;
            useScreenStore.getState().setScore(radar_score);
            setRealData({ radar_score: { status, data: radar_score, unit: '分' } });
          }
        }
      });
      timer = setTimeout(() => {
        timerFn();
      }, 60000);
    };
    timerFn();
    return () => {
      clearTimeout(timer);
    };
  }, [query]);
  return (
    <Wrap
      className={className}
      title='关键性能'
      headerAction={[
        <Radio.Group
          defaultValue={type}
          className='select-type'
          options={[
            { label: '日', value: '0' },
            { label: '月', value: '1' },
            { label: '季度', value: '2' }
          ]}
          onChange={(e) => {
            setType(e.target.value);
          }}
        />
      ]}
    >
      <div className='status-list'>
        {data.map((i) => (
          <div
            key={i.title}
            className={classNames([
              'status-list--item',
              { active: activeData.title === i.title, error: realData[i.key]?.status === 1 }
            ])}
            onClick={() => i.key !== '' && setActiveData(i)}
          >
            <div className='label'>{i.title}</div>
            <div className='content'>
              {i.key && realData[i.key]
                ? (realData[i.key].data || 0) + realData[i.key].unit
                : 'N/A'}
            </div>
          </div>
        ))}
      </div>
      <div className='status-chart'>
        <HisData
          type={type}
          radarId={query.get('radarId')}
          request={activeData.key === 'radar_score' ? getRadarScoreHistoricalData : undefined}
          data={
            activeData
              ? [{ EName: activeData.key, CName: activeData.title, unit: activeData.unit }]
              : []
          }
        />
      </div>
    </Wrap>
  );
};

export default KeyPerformance;
