import faultShow from '@/assets/screen/radar/fault-show.png';
import fault from '@/assets/screen/radar/fault.png';
import lowShow from '@/assets/screen/radar/low-show.png';
import low from '@/assets/screen/radar/low.png';
import noneShow from '@/assets/screen/radar/none-show.png';
import none from '@/assets/screen/radar/none.png';
import perfectShow from '@/assets/screen/radar/perfect-show.png';
import perfect from '@/assets/screen/radar/perfect.png';
import { range } from '@/utils/array';
import { useRequest } from '@/utils/request';
import classNames from 'classnames';
import moment from 'moment';
import { FC } from 'react';
import styled from 'styled-components';
import { getBusinessProcess, pollingInterval } from '../../api';
import Box from '../../components/Box';
import { fixHeight, fixWidth } from '../../util';

const Wrap = styled(Box)`
  .box-wrap {
    overflow: hidden;
    display: flex;
    flex-direction: column;
  }
  .month-wrap {
    /* display: flex; */
    flex: 1;
    overflow: auto;
  }

  .month-list {
    display: flex;
    align-items: center;
    .name-list-item {
      flex: 1;
      /*  */
      text-align: center;
      font-size: ${fixHeight(13)};
      margin: ${fixHeight(2)} ${fixWidth(1)};
      line-height: ${fixHeight(30)};
    }
    .month-list-item {
      width: ${fixWidth(36)};
      margin: ${fixHeight(2)} ${fixWidth(1)};
      height: ${fixHeight(30)};
      text-align: center;
      line-height: ${fixHeight(30)};
      background-size: 100% 100%;

      &.none {
        background-image: url('${none}');
      }
      &.perfect {
        background-image: url('${perfect}');
      }
      &.low {
        background-image: url('${low}');
      }
      &.fault {
        background-image: url('${fault}');
      }
    }
  }
  .status-show-list {
    display: flex;
    width: 100%;
    padding: 0 ${fixWidth(60)};
    justify-content: space-around;
    &-item {
      width: ${fixWidth(90)};
      height: ${fixHeight(30)};
      text-align: center;
      line-height: ${fixHeight(30)};
      background-size: 100% 100%;
      &.none {
        background-image: url('${noneShow}');
      }
      &.perfect {
        background-image: url('${perfectShow}');
      }
      &.low {
        background-image: url('${lowShow}');
      }
      &.fault {
        background-image: url('${faultShow}');
      }
    }
  }
`;
const EnsureProcess: FC<{ className: string; radarId: string }> = (props) => {
  const { radarId, className } = props;
  const businessProcess = useRequest(getBusinessProcess, {
    pollingInterval,
    defaultParams: [{ radarId }]
  });
  const month = range(12, 0, (num) => {
    const date = moment({ M: num });
    return {
      text: date.format('M月'),
      key: date.format('yyyy-MM')
    };
  });
  const currentMonth = moment().format('yyyy-MM');
  return (
    <Wrap className={className} title='保障过程'>
      <div className='month-wrap'>
        <div className='month-list'>
          <div className='name-list-item' />
          {month.map((i) => {
            return (
              <div key={i.key} className='month-list-item'>
                {i.text}
              </div>
            );
          })}
        </div>
        {businessProcess.data?.map((i) => {
          return (
            <div className='month-list' key={i.id}>
              <div className='name-list-item'>{i.name}</div>
              {month.map((m) => (
                <div
                  className={classNames('month-list-item none', {
                    low: i.data[m.key] === 0 && m.key <= currentMonth,
                    perfect: i.data[m.key] === 1
                  })}
                />
              ))}
            </div>
          );
        })}
      </div>
      <div className='status-show-list'>
        <div className='status-show-list-item low'>未完成</div>
        {/* <div className='status-show-list-item none'>未开始</div> */}
        <div className='status-show-list-item perfect'>正常</div>
        {/* <div className='status-show-list-item fault'>异常</div> */}
      </div>
    </Wrap>
  );
};

export default EnsureProcess;
