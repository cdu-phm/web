import BaseEchart from '@/components/base/BaseEchart';
import useScreenStore from '@/store/screen';
import { useRequest } from '@/utils/request';
import { Empty, Spin } from 'antd';
import { FC, useState } from 'react';
import styled from 'styled-components';
import { getPerformanceData, PerformanceItem, pollingInterval } from '../../api';
import Box from '../../components/Box';
import { colors } from '../../util';

const BoxWrap = styled(Box)`
  .ant-spin-nested-loading,
  .ant-spin-container {
    height: 100%;
  }
`;
const Performance: FC<{ className?: string; radarId: string }> = (props) => {
  const { className, radarId } = props;
  const [radarData, setRadarData] = useState<number[]>([]);
  const [indicator, setIndicator] = useState<
    { key: string; name: string; max: number; min: number }[]
  >([]);
  const performanceData = useRequest(getPerformanceData, {
    pollingInterval,
    defaultParams: [{ radarId }],
    onSuccess(data) {
      const keys = Object.keys(data).sort((a, b) => a.localeCompare(b));
      const radarData: number[] = [];
      const indicator: { key: string; name: string; max: number; min: number }[] = [];
      keys.forEach((key) => {
        if (key === 'time') {
          useScreenStore.getState().setTime(data.time.time);
        } else {
          const item = data[key as keyof typeof data] as PerformanceItem;
          radarData.push(item.data);
          indicator.push({
            key: `${item.cName}_${item.unit}`,
            name: `${item.cName} ${item.data} ${item.unit}`,
            max: item.max,
            min: item.min
          });
        }
      });
      setRadarData(radarData);
      setIndicator(indicator);
    }
  });
  const { time } = useScreenStore();

  return (
    <BoxWrap title='性能指标' className={className} headerAction={[<div>更新时间：{time}</div>]}>
      <Spin spinning={performanceData.loading}>
        {indicator.length > 0 ? (
          <BaseEchart
            style={{ height: '100%', width: '100%' }}
            option={{
              grid: {
                top: 20,
                bottom: 20
              },
              radar: {
                // shape: 'circle',
                indicator
              },
              tooltip: {
                trigger: 'axis'
              },

              series: [
                {
                  name: '性能指标',
                  type: 'radar',
                  tooltip: {
                    trigger: 'item',
                    position: ['0%', '0%'],
                    formatter: (params: any) => {
                      const str = `性能指标<br/>
                      ${indicator
                        .map((i, index) => `<div>${i.key.replace('_', params.value[index])}</div>`)
                        .join('')}
                      `;
                      return str;
                    }
                  },
                  lineStyle: {
                    color: colors[0].color
                  },
                  itemStyle: {
                    color: colors[0].color,
                    borderColor: colors[0].borderColor
                  },
                  data: [
                    {
                      value: radarData
                    }
                  ]
                }
              ]
            }}
          />
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} style={{ color: colors[0].color }} />
        )}
      </Spin>
    </BoxWrap>
  );
};

export default Performance;
