import classNames from 'classnames';
import { FC, useState } from 'react';
import styled from 'styled-components';
import Box from '../../components/Box';
import { fixHeight } from '../../util';

const Wrap = styled.div`
  position: relative;
  overflow: hidden;
  .box {
    height: 100%;
    display: flex;
    flex-direction: column;
    border: none;
    &.absolute {
      position: absolute;
      top: 0;
      left: 100%;
      right: 0;
      width: 100%;
      transition: all ease-in-out 0.5s;
    }
    &.show {
      left: 0;
    }
    .box-wrap {
      /* height: 100%; */
      display: flex;
      .box-chart {
        width: 30%;
        border: 1px solid #ccc;
      }
      .index-list {
        flex: 1;
        margin-left: ${fixHeight(10)};
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        &--item {
          height: 30%;
          width: 49%;
          border: 1px solid #ccc;
        }
      }
    }
  }
`;
export interface DeviceSimpleInfoProps {
  className?: string;
  active?: 'status' | 'transmitter' | 'antenna' | 'receiver' | 'signalProcessing';
}
const DeviceSimpleInfo: FC<DeviceSimpleInfoProps> = (props) => {
  const { className, active } = props;
  const [realData, setRealData] = useState({});
  // useEffect(() => {
  //   let timer: number | undefined;
  //   const timerFn = () => {
  //     timer = setTimeout(() => {
  //       getRealData({
  //         table: 'cband_realdata',
  //         field: '',
  //         radarId: 'Z9852'
  //       }).then((res) => {
  //         if (res.ok) {
  //           setRealData(res.data);
  //         }
  //       });
  //       timerFn();
  //     }, 20000);
  //   };
  //   timerFn();
  //   return () => {
  //     clearTimeout(timer);
  //   };
  // }, []);
  return (
    <Wrap className={className}>
      {/* <Box className='box' /> */}
      <Box className={classNames(['box absolute', { show: active === 'status' }])} title='性能指标'>
        <div className='box-chart' />
        <div className='index-list'>
          <div className='index-list--item' />
          <div className='index-list--item' />
          <div className='index-list--item' />
          <div className='index-list--item' />
        </div>
      </Box>
      <Box
        className={classNames(['box absolute', { show: active === 'antenna' }])}
        title='天线状态'
      >
        <div className='box-chart' />
        <div className='index-list'>
          <div className='index-list--item' style={{ flex: 1 }}>
            正常运行时间
          </div>
          <div className='index-list--item'>温度</div>
          <div className='index-list--item'>湿度</div>
          <div className='index-list--item'>Pt-h</div>
          <div className='index-list--item'>Pa-h</div>
        </div>
      </Box>
      <Box
        className={classNames(['box absolute', { show: active === 'transmitter' }])}
        title='发射机'
      >
        <div className='box-wrap'>
          <div className='box-chart' />
          <div className='index-list'>
            <div className='index-list--item'>正常运行时间</div>
            <div className='index-list--item'>温度</div>
            <div className='index-list--item'>湿度</div>
            <div className='index-list--item'>Pt</div>
            <div className='index-list--item'>PN-h</div>
          </div>
        </div>
      </Box>
      <Box className={classNames(['box absolute', { show: active === 'receiver' }])} title='接收机'>
        <div className='box-wrap'>
          <div className='box-chart' />
          <div className='index-list'>
            <div className='index-list--item'>F-h</div>
            <div className='index-list--item'>NL-h</div>
            <div className='index-list--item'>Syscal</div>
            <div className='index-list--item'>Dyr</div>
          </div>
        </div>
      </Box>
      <Box
        className={classNames(['box absolute', { show: active === 'signalProcessing' }])}
        title='信号处理'
      >
        <div className='box-wrap'>
          <div className='box-chart' />
          <div className='index-list'>
            <div className='index-list--item'>正常运行时间</div>
            <div className='index-list--item'>CW</div>
            <div className='index-list--item'>FRD</div>
            <div className='index-list--item'>KD</div>
            <div className='index-list--item'>VW</div>
          </div>
        </div>
      </Box>
    </Wrap>
  );
};

export default DeviceSimpleInfo;
