import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { Model } from './data';
/**
 * 分页获取信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('', data);
}

/**
 * 添加信息
 * @param data
 * @returns
 */
export function addInfo(data: Model) {
  return request.post('', data, { success: '信息新增成功' });
}

/**
 * 编辑信息
 * @param data
 * @returns
 */
export function editInfo(data: Model) {
  return request.put('', data, { success: '信息编辑成功' });
}
/**
 * 删除信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '',
    { id },
    { confirm: { content: '确认删除当前信息' }, success: '信息删除成功' }
  );
}
