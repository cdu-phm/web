import { BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { addInfo, editInfo } from '../api';
import { Model } from '../data';

const InfoModal: ModalFC<{ data?: Model }> = (props) => {
  const { data = {} as Model, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<Model>
      title='信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      {/* form item */}
    </BaseModalForm>
  );
};

export default InfoModal;
