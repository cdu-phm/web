import { PageParams, PageTypeEnum } from '@/models';
import { EnumValueType } from '@/utils/enum';
import { request } from '@/utils/request';
import { FaultPhenomenonModel } from '../data';
/**
 * 分页获取故障现象
 * @param data
 * @returns
 */
export function getList(
  data: PageParams<{ radarTypeId?: string; type?: EnumValueType<typeof PageTypeEnum.enum> }>
) {
  return request.list('/pc/ensureRepair/v1/phenomenon', { type: PageTypeEnum.enum.page, ...data });
}

/**
 * 添加故障现象
 * @param data
 * @returns
 */
export function addInfo(data: FaultPhenomenonModel) {
  return request.post('/pc/ensureRepair/v1/phenomenon', data, { success: '故障现象新增成功' });
}

/**
 * 编辑故障现象
 * @param data
 * @returns
 */
export function editInfo(data: FaultPhenomenonModel) {
  return request.put('/pc/ensureRepair/v1/phenomenon', data, { success: '故障现象编辑成功' });
}
/**
 * 删除故障现象
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/phenomenon',
    { id },
    { confirm: { content: '确认删除当前故障现象' }, success: '故障现象删除成功' }
  );
}
