import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect';
import BaseTable from '@/components/base/BaseTable';
import { getList as getTypeList } from '@/pages/radar/RadarTypeInfo/api';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { FaultPhenomenonModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ radarTypeId?: string; specialId?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSelect
            request={getTypeList}
            style={{ width: 300 }}
            allowClear
            placeholder='请选择雷达型号'
            defaultValue={searchInfo.radarTypeId}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, radarTypeId: v });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.specialId}
            placeholder='请输入现象ID进行搜索'
            onChange={(specialId) => {
              setSearchInfo({ ...searchInfo, specialId: specialId === '' ? undefined : specialId });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'radarTypeName',
              title: '雷达型号'
            },
            {
              dataIndex: 'specialId',
              title: '现象ID'
            },
            {
              dataIndex: 'status',
              title: '现象状态'
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<FaultPhenomenonModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
