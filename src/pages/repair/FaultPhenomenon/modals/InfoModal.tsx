import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormSelect
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { getList } from '@/pages/radar/RadarTypeInfo/api';
import { isEmpty } from '@/utils/is';
import { FaultPhenomenonModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: FaultPhenomenonModel }> = (props) => {
  const { data = {} as FaultPhenomenonModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<FaultPhenomenonModel>
      title='故障现象信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='雷达型号'
        name='radarType'
        request={getList}
        placeholder='请选择雷达型号'
        fieldProps={{
          defaultSelectedOptions: [{ name: data.radarTypeName, id: data.radarType }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        rules={[{ required: true, message: '请选择雷达型号' }]}
      />
      <ProFormSelect
        label='状态'
        name='status'
        options={['强制维护', '不可操作', '请求维护']}
        rules={[{ required: true, message: '请选择现象状态' }]}
      />
      <BaseFormText
        label='现象ID'
        name='specialId'
        placeholder='请输入故障现象ID'
        rules={[{ required: true, message: '请输入故障现象ID' }]}
      />
      <BaseFormTextArea
        label='描述'
        name='remarks'
        placeholder='请输入故障现象描述'
        rules={[{ required: true, message: '请输入故障现象描述' }]}
      />
    </BaseModalForm>
  );
};

export default InfoModal;
