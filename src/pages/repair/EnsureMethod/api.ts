import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { SafeguardMethodModel } from '../data';
/**
 * 分页获取保障方法
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc​/ensureRepair​/v1​/ensureWays', data);
}

/**
 * 添加保障方法
 * @param data
 * @returns
 */
export function addInfo(data: SafeguardMethodModel) {
  return request.post('/pc​/ensureRepair​/v1​/ensureWays', data, { success: '保障方法新增成功' });
}

/**
 * 编辑保障方法
 * @param data
 * @returns
 */
export function editInfo(data: SafeguardMethodModel) {
  return request.put('/pc​/ensureRepair​/v1​/ensureWays', data, { success: '保障方法编辑成功' });
}
/**
 * 删除保障方法
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc​/ensureRepair​/v1​/ensureWays',
    { id },
    { confirm: { content: '确认删除当前保障方法' }, success: '保障方法删除成功' }
  );
}
