import { PageParams } from '@/models';
import { request } from '@/utils/request';
/**
 * 分页获取信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/ensureRecord', data);
}

/**
 * 保障记录导出
 * @param data
 * @returns
 */
export function exportExecl(data: any) {
  return request.post('/pc/ensureRepair/v1/exportEnsureRecord', data, { responseType: 'blob' });
}
