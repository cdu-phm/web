import BaseButton from '@/components/base/BaseButton';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { ExportOutlined } from '@ant-design/icons';
import { ProColumns } from '@ant-design/pro-table';
import { DatePicker } from 'antd';
import moment from 'moment';
import { useState } from 'react';
import { getList as getMethodList } from '../EnsureMethod/api';
import { getList as getDocList } from '../TechnicalDocumentation/api';
import { exportExecl, getList } from './api';

const Info = () => {
  const [searchInfo, setSearchInfo] = useState<{
    ensureDateStart?: string;
    ensureDateEnd?: string;
    documentationId?: string;
    wayId?: string;
  }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        toolBarRender={() => [
          <BaseSelect
            placeholder='请选择保障方法'
            allowClear
            request={getMethodList}
            style={{ width: 150 }}
            fieldNames={{ label: 'name', value: 'id' }}
            defaultValue={searchInfo.wayId}
            onChange={(v) => {
              setSearchInfo({
                ...searchInfo,
                wayId: v
              });
            }}
          />,
          <BaseSelect
            placeholder='请选择保障工卡'
            style={{ width: 180 }}
            allowClear
            request={getDocList}
            fieldNames={{ label: 'name', value: 'id' }}
            defaultValue={searchInfo.documentationId}
            onChange={(v) => {
              setSearchInfo({
                ...searchInfo,
                documentationId: v
              });
            }}
          />,
          <DatePicker.RangePicker
            placeholder={['请选择保障时间段', '请选择保障时间段']}
            showTime={{ format: 'HH:mm:ss' }}
            format='YYYY-MM-DD HH:mm:ss'
            ranges={{
              今天: [moment().startOf('day'), moment().endOf('day')],
              本月: [moment().startOf('month'), moment().endOf('month')]
            }}
            onChange={(values, dateStrings) => {
              const [ensureDateStart, ensureDateEnd] = dateStrings;
              setSearchInfo({
                ...searchInfo,
                ensureDateStart,
                ensureDateEnd
              });
            }}
          />,
          <BaseButton
            type='primary'
            icon={<ExportOutlined />}
            onClick={() => {
              exportExecl(searchInfo);
            }}
          >
            导出
          </BaseButton>
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'radarName',
              title: '雷达'
            },
            {
              title: '所属结构',
              renderText: (text, record) => {
                const { systemName, extensionName, keyPartsModelName } = record;

                return [systemName, extensionName, keyPartsModelName].filter((i) => i).join('-');
              }
            },
            {
              dataIndex: 'documentationName',
              title: '工卡'
            },
            {
              dataIndex: 'wayName',
              title: '保障方法'
            },
            {
              dataIndex: 'ensureDate',
              title: '保障时间',
              valueType: 'dateTime'
            }
          ] as ProColumns[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
