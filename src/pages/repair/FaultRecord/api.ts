import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { FaultRecordModel } from '../data';
/**
 * 分页获取故障记录
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/faultRecord', data);
}

/**
 * 添加故障记录
 * @param data
 * @returns
 */
export function addInfo(data: FaultRecordModel) {
  return request.post('/pc/ensureRepair/v1/faultRecord', data, { success: '故障记录新增成功' });
}

/**
 * 编辑故障记录
 * @param data
 * @returns
 */
export function editInfo(data: FaultRecordModel) {
  return request.put('/pc/ensureRepair/v1/faultRecord', data, { success: '故障记录编辑成功' });
}
/**
 * 删除故障记录
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/faultRecord',
    { id },
    { confirm: { content: '确认删除当前故障记录' }, success: '故障记录删除成功' }
  );
}
/**
 * 获取绑定的故障现象
 * @param faultRecordId
 * @returns
 */
export function getBindList(faultRecordId: string) {
  return request.get<any[]>('/pc​/ensureRepair​/v1​/faultRecord​/phm2Phenomenon', {
    faultRecordId
  });
}

/**
 * 故障记录绑定故障现象
 * @param phmRecordId
 * @param phenomenonIds
 * @returns
 */
export function saveBindList(phmRecordId: string, phenomenonIds: (string | number)[]) {
  return request.post(
    '/pc​/ensureRepair​/v1​/faultRecord​/phm2Phenomenon',
    {
      phmRecordId,
      phenomenonIds
    },
    { success: '故障现象绑定操作成功' }
  );
}
/**
 * 故障记录导出
 * @param data
 * @returns
 */
export function exportExecl(data: any) {
  return request.post('/pc/ensureRepair/v1/exportFaultRecord', data, { responseType: 'blob' });
}
