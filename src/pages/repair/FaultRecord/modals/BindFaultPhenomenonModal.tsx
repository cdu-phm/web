import BaseButton from '@/components/base/BaseButton';
import BaseModal, { ModalFC } from '@/components/base/BaseModal';
import { PageTypeEnum } from '@/models';
import { useRequest } from '@/utils/request';
import ProList from '@ant-design/pro-list';
import { Space } from 'antd';
import { useState } from 'react';
import { getList } from '../../FaultPhenomenon/api';
import { saveBindList } from '../api';

const BindFaultPhenomenonModal: ModalFC<{
  radarTypeId: string;
  recordId: string;
  defaultKeys: string[];
}> = (props) => {
  const { radarTypeId, defaultKeys = [], recordId, ...rest } = props;
  const [selectedKeys, setSelectedKeys] = useState<(string | number)[]>(() => defaultKeys);
  const save = useRequest(saveBindList, {
    manual: true,
    onSuccess() {
      rest.onSuccess && rest.onSuccess();
      rest.onCancel();
    }
  });
  return (
    <BaseModal title='故障现象绑定' {...rest} footer={false}>
      <ProList
        cardProps={{ bodyStyle: { padding: 0 } }}
        pagination={{
          defaultPageSize: 8,
          showSizeChanger: false
        }}
        scroll={{ y: 600 }}
        rowKey='id'
        request={async (params) => {
          const { ok, data } = await getList({
            pageNum: params.current,
            pageSize: params.pageSize,
            type: PageTypeEnum.enum.page,
            radarTypeId
          });
          if (ok) {
            return {
              data: data.list,
              total: data.rowCount
            };
          }
          return {
            data: [],
            total: 0
          };
        }}
        tableAlertOptionRender={({ onCleanSelected }) => (
          <Space>
            <BaseButton
              size='small'
              type='link'
              loading={save.loading}
              disabled={defaultKeys.length === 0 && selectedKeys.length === 0}
              onClick={() => {
                if (defaultKeys.length !== 0 && selectedKeys.length === 0) {
                  BaseModal.confirm({
                    content: '确认要移除所有已绑定的故障现象信息',
                    type: 'warning',
                    onOk() {
                      save.run(recordId, selectedKeys);
                    }
                  });
                } else {
                  save.run(recordId, selectedKeys);
                }
              }}
            >
              保存
            </BaseButton>
            <BaseButton
              size='small'
              type='link'
              onClick={() => {
                onCleanSelected();
              }}
            >
              清空选择
            </BaseButton>
          </Space>
        )}
        rowSelection={{
          alwaysShowAlert: true,
          selectedRowKeys: selectedKeys,
          preserveSelectedRowKeys: true,
          onChange: (keys) => setSelectedKeys(keys)
        }}
        metas={{
          title: {
            dataIndex: 'specialId',
            render(text, entity) {
              return `${text}. ${entity.remarks}`;
            }
          }
        }}
      />
    </BaseModal>
  );
};

export default BindFaultPhenomenonModal;
