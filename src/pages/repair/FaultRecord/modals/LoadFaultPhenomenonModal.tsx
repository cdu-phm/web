import BaseButton from '@/components/base/BaseButton';
import BaseModal, { ModalFC, useModal } from '@/components/base/BaseModal';
import { useRequest } from '@/utils/request';
import { ApiOutlined, ReloadOutlined } from '@ant-design/icons';
import ProList from '@ant-design/pro-list';
import { Empty } from 'antd';
import { getBindList } from '../api';
import BindFaultPhenomenonModal from './BindFaultPhenomenonModal';

const LoadFaultPhenomenonModal: ModalFC<{
  radarTypeId: string;
  recordId: string;
  allowEdit: boolean;
}> = (props) => {
  const { radarTypeId, recordId, allowEdit, ...rest } = props;
  const list = useRequest(getBindList, { defaultParams: [recordId] });
  const bindFaul = useModal(BindFaultPhenomenonModal, {
    onSuccess() {
      list.run(recordId);
    }
  });
  return (
    <BaseModal footer={false} title='故障现象展示' {...rest}>
      <div>
        <ProList
          cardProps={{ bodyStyle: { padding: 0 } }}
          locale={{
            emptyText: (
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='暂无绑定的现象数据' />
            )
          }}
          headerTitle={
            allowEdit ? (
              <BaseButton
                code='bindFP'
                onClick={() => {
                  bindFaul.load({
                    radarTypeId,
                    recordId,
                    defaultKeys: (list.data || []).map((i) => i.id)
                  });
                }}
                icon={<ApiOutlined />}
                type='primary'
              >
                现象绑定
              </BaseButton>
            ) : (
              '已绑定故障现象列表'
            )
          }
          toolBarRender={() => [
            <BaseButton
              type='text'
              onClick={() => {
                list.run(recordId);
              }}
              icon={<ReloadOutlined />}
            />
          ]}
          loading={list.loading}
          dataSource={list.data || []}
          metas={{
            title: {
              dataIndex: 'specialId',
              render(text, entity) {
                return `${text}. ${entity.remark}`;
              }
            }
          }}
        />
      </div>
    </BaseModal>
  );
};

export default LoadFaultPhenomenonModal;
