import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RepairDeviceModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RepairDeviceModel }> = (props) => {
  const { data = {} as RepairDeviceModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RepairDeviceModel>
      title='维修设备信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='设备名称'
        name='name'
        placeholder='请输入维修设备名称'
        rules={[{ required: true, message: '请输入维修设备' }]}
      />
      <BaseFormTextArea label='设备描述' name='remarks' placeholder='请输入设备描述' />
    </BaseModalForm>
  );
};

export default InfoModal;
