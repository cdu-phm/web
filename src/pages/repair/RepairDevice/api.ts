import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RepairDeviceModel } from '../data';
/**
 * 分页获取维修设备
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/repairDevice', data);
}

/**
 * 添加维修设备
 * @param data
 * @returns
 */
export function addInfo(data: RepairDeviceModel) {
  return request.post('/pc/ensureRepair/v1/repairDevice', data, { success: '维修设备新增成功' });
}

/**
 * 编辑维修设备
 * @param data
 * @returns
 */
export function editInfo(data: RepairDeviceModel) {
  return request.put('/pc/ensureRepair/v1/repairDevice', data, { success: '维修设备编辑成功' });
}
/**
 * 删除维修设备
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/repairDevice',
    { id },
    { confirm: { content: '确认删除当前维修设备' }, success: '维修设备删除成功' }
  );
}
