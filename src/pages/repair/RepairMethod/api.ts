import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RepairMethodModel } from '../data';
/**
 * 分页获取维修方式
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc​/ensureRepair​/v1​/repairWays', data);
}

/**
 * 添加维修方式
 * @param data
 * @returns
 */
export function addInfo(data: RepairMethodModel) {
  return request.post('/pc​/ensureRepair​/v1​/repairWays', data, { success: '维修方式新增成功' });
}

/**
 * 编辑维修方式
 * @param data
 * @returns
 */
export function editInfo(data: RepairMethodModel) {
  return request.put('/pc​/ensureRepair​/v1​/repairWays', data, { success: '维修方式编辑成功' });
}
/**
 * 删除维修方式
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc​/ensureRepair​/v1​/repairWays',
    { id },
    { confirm: { content: '确认删除当前维修方式' }, success: '维修方式删除成功' }
  );
}
