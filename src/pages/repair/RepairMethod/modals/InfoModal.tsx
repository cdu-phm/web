import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RepairMethodModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RepairMethodModel }> = (props) => {
  const { data = {} as RepairMethodModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RepairMethodModel>
      title='维修方式信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='方式名称'
        name='name'
        placeholder='请输入维修方式名称'
        rules={[{ required: true, message: '请输入维修方式' }]}
      />
      <BaseFormTextArea label='描述' name='remarks' placeholder='请输入描述' />
    </BaseModalForm>
  );
};

export default InfoModal;
