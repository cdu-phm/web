import { BaseModel } from '@/models';
import { Enum, EnumValueType } from '@/utils/enum';

export interface RepairMeasuresModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface RepairEquipmentModel extends BaseModel {
  name: string;
  remarks?: string;
}
export interface RepairProgrammePutModel extends BaseModel {
  name: string;
  documentId: string;
  documentName: string;
  docUrl?: string;
  repairWayId: string;
  equipmentIds: string[];
  deviceIds: string[];
  repairMeasuresId: string;
  remarks?: string;
}
export interface RepairProgrammeModel extends BaseModel {
  name: string;
  documentId: string;
  documentName: string;
  docUrl?: string;
  repairWayId: string;
  repairWayName: string;
  repairMeasuresId: string;
  repairMeasuresName: string;
  deviceList: { id: string; name: string }[];
  equipmentList: { id: string; name: string }[];
  remarks?: string;
}

export interface ProgrammeMeasureModel extends BaseModel {
  deviceId: string;
  deviceName: string;
  equipmentId: string;
  equipmentName: string;
  measuresId: string;
  measuresName: string;
  programmeId: string;
  programmeName: string;
  name: string;
  remarks?: string;
}

export interface TechnicalDocumentationModel extends BaseModel {
  name: string;
  url?: string;
  remarks?: string;
}

export interface SafeguardMethodModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface TaskLevelModel extends BaseModel {
  name: string;
  remarks?: string;
}

export const FaultProcessStateEnum = Enum({
  not: {
    value: 0,
    label: '未维修'
  },
  done: {
    value: 1,
    label: '已维修'
  }
});

export const FaultSourceEnum = Enum({
  manual: {
    label: '手动',
    value: 0
  },
  auto: {
    label: '自动',
    value: 1
  }
});

export interface FaultRecordModel extends BaseModel {
  structureId: string;
  raEquipmentIds: string;
  raEquipmentNames: string;
  radarTypeId: string;
  radarTypeName: string;
  downTime: string;
  faultResource: EnumValueType<typeof FaultSourceEnum.enum>;
  isDeal: EnumValueType<typeof FaultProcessStateEnum.enum>;
}

export interface FaultPhenomenonModel extends BaseModel {
  desc: string;
  specialId: string;
  status: string;
  radarType: string;
  radarTypeName: string;
  remarks?: string;
}

export interface RepairRecordModel extends BaseModel {
  faultRecordId: string;
  remarks: string;
  repairTime: string;
  repairUser: string;
}

export interface EnsureBaseModel extends BaseModel {
  content: string;
  cycle: number;
  keyPartsModelId: string;
  keyPartsModelName: string;
  raExtensionId: string;
  raExtensionName: string;
  raSystemId: string;
  raSystemName: string;
  name: string;
  remarks?: string;
  taskLevelId: string;
  taskLevelName: string;
  documentName: string;
  documentId: string;
  documentUrl: string;
}

export interface RepairMethodModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface RepairDeviceModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface EnsureTypeModel extends BaseModel {
  name: string;
  cycle: number;
  parentId?: string;
  parentName?: string;
  remarks?: string;
}
