import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RepairMeasuresModel } from '../data';
/**
 * 分页获取维修措施信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/repairMeasures', data);
}

/**
 * 添加维修措施信息
 * @param data
 * @returns
 */
export function addInfo(data: RepairMeasuresModel) {
  return request.post('/pc/ensureRepair/v1/repairMeasures', data, {
    success: '维修措施信息新增成功'
  });
}

/**
 * 编辑维修措施信息
 * @param data
 * @returns
 */
export function editInfo(data: RepairMeasuresModel) {
  return request.put('/pc/ensureRepair/v1/repairMeasures', data, {
    success: '维修措施信息编辑成功'
  });
}
/**
 * 删除维修措施信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/repairMeasures',
    { id },
    { confirm: { content: '确认删除当前维修措施信息' }, success: '维修措施信息删除成功' }
  );
}
