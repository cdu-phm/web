import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { EnsureBaseModel } from '../data';
/**
 * 分页获取保障基础信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/ensure', data);
}

/**
 * 添加保障基础信息
 * @param data
 * @returns
 */
export function addInfo(data: EnsureBaseModel) {
  return request.post('/pc/ensureRepair/v1/ensure', data, { success: '保障基础信息新增成功' });
}

/**
 * 编辑保障基础信息
 * @param data
 * @returns
 */
export function editInfo(data: EnsureBaseModel) {
  return request.put('/pc/ensureRepair/v1/ensure', data, { success: '保障基础信息编辑成功' });
}
/**
 * 删除保障基础信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/ensure',
    { id },
    { confirm: { content: '确认删除当前保障基础信息' }, success: '保障基础信息删除成功' }
  );
}
