import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { download } from '@/utils';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, FilePdfOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { EnsureBaseModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        params={searchInfo}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '名称'
            },
            {
              title: '所属结构',
              render: (text, record) => {
                const { raSystemName, raExtensionName, keyPartsModelName } = record;
                return [raSystemName, raExtensionName, keyPartsModelName]
                  .filter((i) => i)
                  .join('-');
              }
            },
            {
              dataIndex: 'taskLevelName',
              title: '任务级别'
            },
            {
              dataIndex: 'documentName',
              title: '工卡',
              render: (text, record) => {
                return (
                  <BaseButton
                    type='link'
                    onClick={() => {
                      download(record.documentUrl, { filename: record.documentName });
                    }}
                  >
                    <FilePdfOutlined /> {text}
                  </BaseButton>
                );
              }
            },
            {
              dataIndex: 'content',
              title: '保障内容'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<EnsureBaseModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
