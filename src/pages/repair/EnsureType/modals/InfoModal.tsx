import {
  BaseFormDight,
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { BaseTimeEnum, formateDate } from '@/utils/time';
import { Select } from 'antd';
import { useState } from 'react';
import { EnsureTypeModel } from '../../data';
import { addInfo, editInfo, getList } from '../api';

const InfoModal: ModalFC<{ data?: EnsureTypeModel }> = (props) => {
  const { data = {} as EnsureTypeModel, onSuccess, ...anyProps } = props;
  const { id, cycle } = data;
  const date = formateDate(cycle);
  const [timeType, setTimeType] = useState(() => date.type);
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<EnsureTypeModel>
      title='保障类型信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={{ ...data, cycle: date.value }}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.cycle *= timeType;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入保障类型名称'
        rules={[{ required: true, message: '请输入保障类型名称' }]}
      />
      <BaseFormSelect
        label='父类型'
        readonly={!isadd}
        placeholder='请选择父类型'
        name='parentId'
        params={{ noRecursion: true }}
        request={getList}
        fieldProps={{
          defaultSelectedOptions: [{ name: data.parentName, id: data.parentId }],
          fieldNames: {
            label: 'name',
            value: 'id'
          }
        }}
      />
      <BaseFormDight
        label='保障周期'
        name='cycle'
        placeholder='请选择保障周期'
        fieldProps={{ min: 0 }}
        addonAfter={
          <Select
            defaultValue={timeType}
            onChange={(v) => {
              setTimeType(v);
            }}
            options={BaseTimeEnum.list}
          />
        }
      />
      <BaseFormTextArea label='描述' name='remarks' placeholder='请输入保障类型描述' />
    </BaseModalForm>
  );
};

export default InfoModal;
