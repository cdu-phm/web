import { PageParams, PageTypeEnum } from '@/models';
import { EnumValueType } from '@/utils/enum';
import { request } from '@/utils/request';
import { EnsureTypeModel } from '../data';
/**
 * 分页获取信息
 * @param data
 * @returns
 */
export function getList(
  data: PageParams<{
    name?: string;
    type?: EnumValueType<typeof PageTypeEnum.enum>;
    noRecursion?: boolean;
  }>
) {
  return request.list('/pc​/ensureRepair​/v1​/ensureType', data);
}

/**
 * 添加信息
 * @param data
 * @returns
 */
export function addInfo(data: EnsureTypeModel) {
  return request.post('/pc​/ensureRepair​/v1​/ensureType', data, { success: '信息新增成功' });
}

/**
 * 编辑信息
 * @param data
 * @returns
 */
export function editInfo(data: EnsureTypeModel) {
  return request.put('/pc​/ensureRepair​/v1​/ensureType', data, { success: '信息编辑成功' });
}
/**
 * 删除信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc​/ensureRepair​/v1​/ensureType',
    { id },
    { confirm: { content: '确认删除当前信息' }, success: '信息删除成功' }
  );
}
