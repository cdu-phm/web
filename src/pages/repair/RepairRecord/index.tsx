import BaseButton from '@/components/base/BaseButton';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { ExportOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { DatePicker } from 'antd';
import moment from 'moment';
import { useRef, useState } from 'react';
import { getList as getRepariList } from '../RepairProgramme/api';
import { exportExecl, getList } from './api';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const [searchInfo, setSearchInfo] = useState<{
    repairProgramId?: string;
    repairTimeStart?: string;
    repairTimeEnd?: string;
  }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        toolBarRender={() => [
          <BaseSelect
            request={getRepariList}
            defaultValue={searchInfo.repairProgramId}
            placeholder='请选择维修方案'
            allowClear
            style={{ width: 150 }}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, repairProgramId: v });
            }}
          />,
          <DatePicker.RangePicker
            placeholder={['请选择维修时间段', '请选择维修时间段']}
            showTime={{ format: 'HH:mm:ss' }}
            format='YYYY-MM-DD HH:mm:ss'
            ranges={{
              今天: [moment().startOf('day'), moment().endOf('day')],
              本月: [moment().startOf('month'), moment().endOf('month')]
            }}
            onChange={(values, dateStrings) => {
              const [repairTimeStart, repairTimeEnd] = dateStrings;
              setSearchInfo({
                ...searchInfo,
                repairTimeStart,
                repairTimeEnd
              });
            }}
          />,
          <BaseButton
            type='primary'
            icon={<ExportOutlined />}
            onClick={() => {
              exportExecl(searchInfo);
            }}
          >
            导出
          </BaseButton>
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'repairProgramName',
              title: '维修方案'
            },
            {
              dataIndex: 'remarks',
              title: '故障原因'
            },
            {
              dataIndex: 'repairUser',
              title: '维修人员'
            },
            {
              dataIndex: 'repairTime',
              title: '维修时间',
              valueType: 'dateTime'
            }
          ] as ProColumns[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
