import {
  BaseFormSelect,
  BaseFormTextArea,
  BaseModalForm,
  ProFormDateTimePicker
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { range } from '@/utils/array';
import moment from 'moment';
import { RepairRecordModel } from '../../data';
import { getList } from '../../RepairProgramme/api';
import { addInfo } from '../api';

const DealModal: ModalFC<{ faultRecordId: string; faultTime: string }> = (props) => {
  const { faultRecordId, faultTime, onSuccess, ...rest } = props;
  return (
    <BaseModalForm<RepairRecordModel>
      {...rest}
      title='维修信息'
      width={560}
      labelCol={{ style: { width: 80 } }}
      onFinish={async (values) => {
        values.faultRecordId = faultRecordId;
        const { ok } = await addInfo(values);
        if (ok) {
          onSuccess && onSuccess();
        }
        return ok;
      }}
    >
      <ProFormDateTimePicker
        label='维修时间'
        name='repairTime'
        rules={[{ required: true, message: '请选择设备维修时间' }]}
        fieldProps={{
          format: 'yyyy-MM-DD HH:mm:ss',
          disabledDate: (current) => {
            return current && current < moment(faultTime).startOf('day');
          },
          disabledTime: (current) => {
            const hour = moment(faultTime).format('HH');
            const min = moment(faultTime).format('mm');
            const currentHour = current?.format('HH');
            return {
              disabledHours: () => range(24).splice(0, Number(hour)),
              disabledMinutes: () =>
                currentHour === hour ? range(60).splice(0, Number(min) + 1) : []
            };
          }
        }}
      />
      <BaseFormSelect
        label='维修方案'
        name='repairProgramId'
        request={getList}
        placeholder='请选择设备维修方案'
        fieldProps={{
          defaultSelectedOptions: [],
          fieldNames: { label: 'name', value: 'id' }
        }}
        rules={[{ required: true, message: '请选择设备维修方案' }]}
      />

      <BaseFormTextArea label='故障原因' name='remarks' />
    </BaseModalForm>
  );
};

export default DealModal;
