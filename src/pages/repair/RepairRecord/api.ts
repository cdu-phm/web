import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RepairRecordModel } from '../data';
/**
 * 分页获取维修记录
 * @param data
 * @returns
 */
export function getList(
  data: PageParams<{ repairProgramId?: string; repairTimeStart?: string; repairTimeEnd?: string }>
) {
  return request.list('/pc/ensureRepair/v1/repairRecord', data);
}

/**
 * 增加维修记录
 * @param data
 * @returns
 */
export function addInfo(data: RepairRecordModel) {
  return request.post('/pc/ensureRepair/v1/repairRecord', data);
}

/**
 * 维修记录导出
 * @param data
 * @returns
 */
export function exportExecl(data: any) {
  return request.post('/pc/ensureRepair/v1/exportRepairRecord', data, { responseType: 'blob' });
}
