import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RepairEquipmentModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RepairEquipmentModel }> = (props) => {
  const { data = {} as RepairEquipmentModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RepairEquipmentModel>
      title='器材信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='器材名称'
        name='name'
        placeholder='请输入器材名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入器材名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
        placeholder='请输入器材描述'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
