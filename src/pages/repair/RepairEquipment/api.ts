import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RepairEquipmentModel } from '../data';
/**
 * 分页获取维修器材信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/repairEquipment', data);
}

/**
 * 添加维修器材信息
 * @param data
 * @returns
 */
export function addInfo(data: RepairEquipmentModel) {
  return request.post('/pc/ensureRepair/v1/repairEquipment', data, {
    success: '维修器材信息新增成功'
  });
}

/**
 * 编辑维修器材信息
 * @param data
 * @returns
 */
export function editInfo(data: RepairEquipmentModel) {
  return request.put('/pc/ensureRepair/v1/repairEquipment', data, {
    success: '维修器材信息编辑成功'
  });
}
/**
 * 删除维修器材信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/repairEquipment',
    { id },
    { confirm: { content: '确认删除当前维修器材信息' }, success: '维修器材信息删除成功' }
  );
}
