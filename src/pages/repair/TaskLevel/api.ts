import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TaskLevelModel } from '../data';
/**
 * 分页获取任务级别
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/taskLevel', data);
}

/**
 * 添加任务级别
 * @param data
 * @returns
 */
export function addInfo(data: TaskLevelModel) {
  return request.post('/pc/ensureRepair/v1/taskLevel', data, { success: '任务级别新增成功' });
}

/**
 * 编辑任务级别
 * @param data
 * @returns
 */
export function editInfo(data: TaskLevelModel) {
  return request.put('/pc/ensureRepair/v1/taskLevel', data, { success: '任务级别编辑成功' });
}
/**
 * 删除任务级别
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/taskLevel',
    { id },
    { confirm: { content: '确认删除当前任务级别' }, success: '任务级别删除成功' }
  );
}
