import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { TaskLevelModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: TaskLevelModel }> = (props) => {
  const { data = {} as TaskLevelModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<TaskLevelModel>
      title='任务级别信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='级别名称'
        name='name'
        placeholder='请输入任务级别名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入任务级别名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
        placeholder='请输入描述'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
