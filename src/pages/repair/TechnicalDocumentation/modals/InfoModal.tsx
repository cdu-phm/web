import {
  BaseFormText,
  BaseFormTextArea,
  BaseFormUpload,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { TechnicalDocumentationModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: TechnicalDocumentationModel }> = (props) => {
  const { data = {} as TechnicalDocumentationModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<TechnicalDocumentationModel>
      title='工卡信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values: TechnicalDocumentationModel) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='工卡名称'
        name='name'
        placeholder='请输入工卡名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入工卡名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormUpload
        label='工卡文件'
        name='url'
        placeholder='请上传工卡文件'
        extra='文件只支持pdf、doc、docx格式, 文件大小不可超过100M'
        rules={[{ required: true, message: '请上传工卡文件' }]}
        accept='.pdf,.doc,.docx'
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
