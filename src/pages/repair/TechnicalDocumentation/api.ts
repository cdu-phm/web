import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TechnicalDocumentationModel } from '../data';
/**
 * 分页获取工卡信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/technicalDocumentation', data);
}

/**
 * 添加工卡信息
 * @param data
 * @returns
 */
export function addInfo(data: TechnicalDocumentationModel) {
  return request.post('/pc/ensureRepair/v1/technicalDocumentation', data, {
    success: '工卡信息新增成功'
  });
}

/**
 * 编辑工卡信息
 * @param data
 * @returns
 */
export function editInfo(data: TechnicalDocumentationModel) {
  return request.put('/pc/ensureRepair/v1/technicalDocumentation', data, {
    success: '工卡信息编辑成功'
  });
}
/**
 * 删除工卡信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/technicalDocumentation',
    { id },
    { confirm: { content: '确认删除当前工卡信息' }, success: '工卡信息删除成功' }
  );
}
