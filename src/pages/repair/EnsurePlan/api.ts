import { PageParams } from '@/models';
import { request } from '@/utils/request';
/**
 * 分页获取保障计划
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/ensureRepair/v1/ensurePlan', data);
}

/**
 * 保障
 * @param data
 * @returns
 */
export function maintainPlan(data: any) {
  return request.post('/pc/ensureRepair/v1/ensurePlan', data);
}
/**
 * 停用保障计划
 * @param id
 * @returns
 */
export function stopPlan(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/ensurePlan',
    { id },
    { confirm: { content: '确认停用当前保障计划' }, success: '保障计划停用成功' }
  );
}
