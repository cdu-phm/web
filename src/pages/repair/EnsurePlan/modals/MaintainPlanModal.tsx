import {
  BaseFormSelect,
  BaseFormTextArea,
  BaseModalForm,
  ProFormDateTimePicker
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { range } from '@/utils/array';
import { Col, Row } from 'antd';
import moment from 'moment';
import { getList } from '../../EnsureMethod/api';
import { getList as getDocList } from '../../TechnicalDocumentation/api';
import { maintainPlan } from '../api';

const MaintainPlanModal: ModalFC<{
  id: string;
  ensureDate: string;
  ensureId: string;
  keyPartsId: string;
}> = (props) => {
  const { onSuccess, ensureDate, ensureId, keyPartsId, id, ...rest } = props;
  return (
    <BaseModalForm
      title='新增保障信息'
      labelCol={{ style: { width: 80 } }}
      onFinish={async (values) => {
        values.ensureId = ensureId;
        values.keyPartsId = keyPartsId;
        values.id = id;
        const { ok } = await maintainPlan(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
      {...rest}
    >
      <Row>
        <Col span={12}>
          <ProFormDateTimePicker
            label='保障时间'
            name='ensureDate'
            fieldProps={{
              format: 'yyyy-MM-DD HH:mm:ss',
              disabledDate: (current) => {
                return current && current < moment(ensureDate).startOf('day');
              },
              disabledTime: (current) => {
                const hour = moment(ensureDate).format('HH');
                const min = moment(ensureDate).format('mm');
                const currentHour = current?.format('HH');
                return {
                  disabledHours: () => range(24).splice(0, Number(hour) + 1),
                  disabledMinutes: () =>
                    currentHour === hour ? range(60).splice(0, Number(min) + 1) : []
                };
              }
            }}
            rules={[{ required: true, message: '请选择保障时间' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormSelect
            label='保障方法'
            name='wayId'
            placeholder='请选择保障方法'
            rules={[{ required: true, message: '请选择保障方法' }]}
            request={getList}
            fieldProps={{ fieldNames: { label: 'name', value: 'id' } }}
          />
        </Col>
      </Row>
      <BaseFormSelect
        label='工卡'
        name='documentationId'
        rules={[{ required: true, message: '请选择保障工卡' }]}
        placeholder='请选择保障工卡'
        request={getDocList}
        fieldProps={{ fieldNames: { label: 'name', value: 'id' } }}
      />
      <BaseFormTextArea label='保障描述' name='remarks' placeholder='请输入保障描述' />
    </BaseModalForm>
  );
};

export default MaintainPlanModal;
