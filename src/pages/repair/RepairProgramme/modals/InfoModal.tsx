import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RepairProgrammeModel, RepairProgrammePutModel } from '../../data';
import { getList as getDeviceList } from '../../RepairDevice/api';
import { getList as getEquipmentList } from '../../RepairEquipment/api';
import { getList as getMeasureList } from '../../RepairMeasures/api';
import { getList as getMethodList } from '../../RepairMethod/api';
import { getList as getDocumentationList } from '../../TechnicalDocumentation/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RepairProgrammeModel }> = (props) => {
  const { data = {} as RepairProgrammeModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RepairProgrammePutModel>
      title='维修方案信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={{
        ...data,
        equipmentIds: (data.equipmentList ?? []).map((i) => i.id),
        deviceIds: (data.deviceList ?? []).map((i) => i.id)
      }}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='工卡'
        request={getDocumentationList}
        name='documentId'
        fieldProps={{
          defaultSelectedOptions: [{ name: data.documentName, id: data.documentId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择工卡'
        rules={[{ required: true, message: '请选择工卡' }]}
      />

      <BaseFormText
        label='方案名称'
        name='name'
        placeholder='请输入方案名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入方案名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormSelect
        label='维修方式'
        request={getMethodList}
        name='repairWayId'
        fieldProps={{
          defaultSelectedOptions: [{ name: data.repairWayName, id: data.repairWayId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择维修方式'
        rules={[{ required: true, message: '请选择维修方式' }]}
      />
      <BaseFormSelect
        label='维修设备'
        multiple
        name='deviceIds'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: data.deviceList
        }}
        request={getDeviceList}
      />
      <BaseFormSelect
        label='维修器材'
        multiple
        name='equipmentIds'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: data.equipmentList
        }}
        request={getEquipmentList}
      />
      <BaseFormSelect
        label='维修措施'
        name='repairMeasuresId'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: [{ name: data.repairMeasuresName, id: data.repairMeasuresId }]
        }}
        request={getMeasureList}
        rules={[{ required: true, message: '请选择维修措施' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
        placeholder='请输入描述'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
