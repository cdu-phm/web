import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import BaseModal, { ModalFC, useModal } from '@/components/base/BaseModal';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef } from 'react';
import { ProgrammeMeasureModel } from '../../data';
import { delDetailInfo, getDetailList } from '../api';
import DetailInfoModal from './DetailInfoModal';

const DetailListModal: ModalFC<{ programmeId: string }> = (props) => {
  const { programmeId, ...rest } = props;
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(DetailInfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delDetailInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  return (
    <BaseModal title='方案明细列表' footer={false} {...rest}>
      <BaseTable
        actionRef={tableRef}
        cardProps={{ bodyStyle: { padding: 0 } }}
        request={getDetailList}
        params={{ programmeId }}
        headerTitle={
          <BaseButton
            code='createDetail'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({
                programmeId
              });
            }}
          >
            新增
          </BaseButton>
        }
        columns={
          [
            {
              dataIndex: 'name',
              title: '明细名称'
            },
            {
              dataIndex: 'deviceName',
              title: '维修设备'
            },
            {
              dataIndex: 'equipmentName',
              title: '维修器材'
            },
            {
              dataIndex: 'measuresName',
              title: '维修措施'
            },
            {
              dataIndex: 'remarks',
              title: '备注'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='editDetail'
                      onClick={() => {
                        infoModal.load({ data: record, programmeId });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='deleteDetail'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<ProgrammeMeasureModel>[]
        }
      />
    </BaseModal>
  );
};

export default DetailListModal;
