import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { ProgrammeMeasureModel } from '../../data';
import { getList as getDeviceList } from '../../RepairDevice/api';
import { getList as getEquipmentList } from '../../RepairEquipment/api';
import { getList as getMeasureList } from '../../RepairMeasures/api';
import { addDetailInfo, editDetailInfo } from '../api';

const DetailInfoModal: ModalFC<{ programmeId: string; data?: ProgrammeMeasureModel }> = (props) => {
  const { data = {} as ProgrammeMeasureModel, programmeId, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<ProgrammeMeasureModel>
      title='维修方案明细信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.programmeId = programmeId;
        const { ok } = await (isadd ? addDetailInfo : editDetailInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='明细名称'
        name='name'
        placeholder='请输入明细名称'
        rules={[{ required: true, message: '请输入明细名称' }]}
      />
      <BaseFormSelect
        label='维修设备'
        name='deviceId'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: [{ name: data.deviceName, id: data.deviceId }]
        }}
        request={getDeviceList}
        rules={[{ required: true, message: '请选择维修设备' }]}
      />
      <BaseFormSelect
        label='维修器材'
        name='equipmentId'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: [{ name: data.equipmentName, id: data.equipmentId }]
        }}
        request={getEquipmentList}
        rules={[{ required: true, message: '请选择维修器材' }]}
      />
      <BaseFormSelect
        label='维修措施'
        name='measuresId'
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: [{ name: data.measuresName, id: data.measuresId }]
        }}
        request={getMeasureList}
        rules={[{ required: true, message: '请选择维修措施' }]}
      />
      <BaseFormTextArea label='明细描述' name='remarks' placeholder='请输入明细描述' />
    </BaseModalForm>
  );
};
export default DetailInfoModal;
