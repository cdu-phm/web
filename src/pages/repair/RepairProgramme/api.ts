import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { ProgrammeMeasureModel, RepairProgrammeModel, RepairProgrammePutModel } from '../data';
/**
 * 分页获取维修方案信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list<RepairProgrammeModel>('/pc/ensureRepair/v1/repairProgramme', data);
}

/**
 * 添加维修方案信息
 * @param data
 * @returns
 */
export function addInfo(data: RepairProgrammePutModel) {
  return request.post('/pc/ensureRepair/v1/repairProgramme', data, {
    success: '维修方案信息新增成功'
  });
}

/**
 * 编辑维修方案信息
 * @param data
 * @returns
 */
export function editInfo(data: RepairProgrammePutModel) {
  return request.put('/pc/ensureRepair/v1/repairProgramme', data, {
    success: '维修方案信息编辑成功'
  });
}
/**
 * 删除维修方案信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/ensureRepair/v1/repairProgramme',
    { id },
    { confirm: { content: '确认删除当前维修方案信息' }, success: '维修方案信息删除成功' }
  );
}

/**
 * 分页获取维修方案明细信息
 * @param data
 * @returns
 */
export function getDetailList(data: PageParams<{ name?: string }>) {
  return request.list('​/pc​/ensureRepair​/v1​/programmeMeasures', data);
}

/**
 * 添加维修方案明细信息
 * @param data
 * @returns
 */
export function addDetailInfo(data: ProgrammeMeasureModel) {
  return request.post('/pc​/ensureRepair​/v1​/programmeMeasures', data, {
    success: '维修方案明细新增成功'
  });
}

/**
 * 编辑维修方案明细信息
 * @param data
 * @returns
 */
export function editDetailInfo(data: ProgrammeMeasureModel) {
  return request.put('/pc​/ensureRepair​/v1​/programmeMeasures', data, {
    success: '维修方案明细编辑成功'
  });
}
/**
 * 删除维修方案明细信息
 * @param id
 * @returns
 */
export function delDetailInfo(id: string) {
  return request.delete(
    '/pc​/ensureRepair​/v1​/programmeMeasures',
    { id },
    { confirm: { content: '确认删除当前方案明细' }, success: '方案明细删除成功' }
  );
}
