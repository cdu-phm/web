import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { download } from '@/utils';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, DownloadOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space, Tag } from 'antd';
import { useRef, useState } from 'react';
import { RepairProgrammeModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  // const detailListModal = useModal(DetailListModal);
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入方案名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '方案名称'
            },
            {
              dataIndex: 'documentName',
              title: '工卡'
            },
            {
              dataIndex: 'repairWayName',
              title: '维修方式'
            },
            {
              dataIndex: 'repairMeasuresName',
              title: '维修措施'
            },
            {
              dataIndex: 'deviceList',
              title: '维修设备',
              render: (_, record) => {
                return record.deviceList.length > 0
                  ? record.deviceList.map((i) => <Tag>{i.name}</Tag>)
                  : _;
              }
            },
            {
              dataIndex: 'equipmentList',
              title: '维修器材',
              render: (_, record) => {
                return record.equipmentList.length > 0
                  ? record.equipmentList.map((i) => <Tag>{i.name}</Tag>)
                  : _;
              }
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    {/* <BaseAction
                      color='blue'
                      title='方案明细'
                      code='loadDetail'
                      icon={<BarsOutlined />}
                      onClick={() => detailListModal.load({ programmeId: record.id })}
                    >
                      方案明细
                    </BaseAction> */}
                    <BaseAction
                      color='blue'
                      title='工卡下载'
                      code='download'
                      icon={<DownloadOutlined />}
                      onClick={() =>
                        record.docUrl && download(record.docUrl, { filename: record.documentName })
                      }
                    >
                      工卡下载
                    </BaseAction>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<RepairProgrammeModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
