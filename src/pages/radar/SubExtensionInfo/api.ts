import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RadarSubExtensionModel } from '../data';
/**
 * 分页获取分机信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ systemId?: string; name?: string }>) {
  return request.list('/pc/radar/v1/raExtensions', data);
}

/**
 * 添加分机信息
 * @param data
 * @returns
 */
export function addInfo(data: RadarSubExtensionModel) {
  return request.post('/pc/radar/v1/raExtensions', data, { success: '分机信息新增成功' });
}

/**
 * 编辑分机信息
 * @param data
 * @returns
 */
export function editInfo(data: RadarSubExtensionModel) {
  return request.put('/pc/radar/v1/raExtensions', data, { success: '分机信息编辑成功' });
}
/**
 * 删除分机信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raExtensions',
    { id },
    { confirm: { content: '确认删除当前分机信息' }, success: '分机信息删除成功' }
  );
}
