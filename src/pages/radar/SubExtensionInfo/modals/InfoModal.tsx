import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RadarSubExtensionModel } from '../../data';
import { getList } from '../../SubSystemInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RadarSubExtensionModel }> = (props) => {
  const { data = {} as RadarSubExtensionModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RadarSubExtensionModel>
      title='分机信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='分系统'
        name='systemId'
        request={getList}
        fieldProps={{
          defaultSelectedOptions: [{ name: data.systemName, id: data.systemId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择分系统'
        rules={[{ required: true, message: '请选择分系统' }]}
      />
      <BaseFormText
        label='分机名称'
        name='name'
        placeholder='请输入分机名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入分机名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
