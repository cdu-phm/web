import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import useUserStore from '@/store/user';
import { useRequest } from '@/utils/request';
import {
  BlockOutlined,
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  SettingOutlined
} from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Select, Space } from 'antd';
import { useRef, useState } from 'react';
import { InServiceEnum, RadarModel } from '../data';
import { getList as getTypeInfo } from '../RadarTypeInfo/api';
import { delInfo, getList } from './api';
import AccessMethodListModal from './modals/AccessMethodListModal';
import InfoModal from './modals/InfoModal';
import StructureModal from './modals/StructureModal';

const RadarInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const accessMethodListModal = useModal(AccessMethodListModal);
  const structureModal = useModal(StructureModal);
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const {
    userinfo: { area }
  } = useUserStore();
  const [searchInfo, setSearchInfo] = useState<{ name?: string; typeId?: string; areaId?: string }>(
    {}
  );
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <Select
            placeholder='请选择雷达所属区域'
            defaultValue={searchInfo.areaId}
            allowClear
            style={{ width: 200 }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, areaId: v });
            }}
            options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
          />,
          <BaseSelect
            request={getTypeInfo}
            allowClear
            style={{ width: 280 }}
            placeholder='请选择雷达型号'
            defaultValue={searchInfo.typeId}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, typeId: v });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入雷达名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '雷达名称'
            },
            {
              dataIndex: 'typeName',
              title: '型号'
            },
            {
              dataIndex: 'serviceName',
              title: '服务类型'
            },
            {
              dataIndex: 'siteName',
              title: '所属站点'
            },
            {
              dataIndex: 'status',
              title: '在役状态',
              renderText: (text) => InServiceEnum.getLabelByValue(text)
            },
            {
              dataIndex: 'dataTableName',
              title: '数据表'
            },
            {
              dataIndex: 'productionFactory',
              title: '生产厂家'
            },
            {
              dataIndex: 'productionDate',
              title: '生产日期',
              valueType: 'date'
            },
            {
              dataIndex: 'installTime',
              title: '安装日期',
              valueType: 'date'
            },
            {
              dataIndex: 'installationPosition',
              title: '安装地址'
            },
            {
              dataIndex: 'remarks',
              title: '功能说明'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='blue'
                      icon={<BlockOutlined />}
                      title='雷达结构管理'
                      code='loadStructure'
                      onClick={() => {
                        structureModal.load({
                          id: record.id,
                          areaId: record.areaId,
                          name: record.name
                        });
                      }}
                    />
                    <BaseAction
                      color='blue'
                      icon={<SettingOutlined />}
                      title='接入方式'
                      code='loadAccessMethod'
                      onClick={() => {
                        accessMethodListModal.load({
                          radarId: record.id,
                          radarName: record.name,
                          typeName: record.typeName,
                          onCancel: () => {
                            if (!record.dataTableName) {
                              tableRef.current?.reload();
                            }
                          }
                        });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<RadarModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default RadarInfo;
