import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RadarAccessMethodModel, RadarModel } from '../data';
/**
 * 分页获取雷达信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/raRadars', data);
}

/**
 * 添加雷达信息
 * @param data
 * @returns
 */
export function addInfo(data: RadarModel) {
  return request.post('/pc/radar/v1/raRadars', data, { success: '雷达信息新增成功' });
}

/**
 * 编辑雷达信息
 * @param data
 * @returns
 */
export function editInfo(data: RadarModel) {
  return request.put('/pc/radar/v1/raRadars', data, { success: '雷达信息编辑成功' });
}
/**
 * 删除雷达信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raRadars',
    { id },
    { confirm: { content: '确认删除当前雷达信息' }, success: '雷达信息删除成功' }
  );
}

/**
 * 获取雷达接入方式信息
 * @param data
 * @returns
 */
export function getAccessMethodList(data: PageParams<{ radarId: string }>) {
  return request.list('/pc/radar/v1/raInterventionTypes', data);
}

/**
 * 添加雷达接入方式信息
 * @param data
 * @returns
 */
export function addAccessMethodInfo(data: RadarAccessMethodModel) {
  return request.post('/pc/radar/v1/raInterventionTypes', data, {
    success: '接入方式信息新增成功'
  });
}

/**
 * 编辑雷达接入方式信息
 * @param data
 * @returns
 */
export function editAccessMethodInfo(data: RadarAccessMethodModel) {
  return request.put('/pc/radar/v1/raInterventionTypes', data, { success: '接入方式信息编辑成功' });
}
/**
 * 删除雷达接入方式信息
 * @param id
 * @returns
 */
export function delAccessMethodInfo(data: { id: string; radarId: string }) {
  return request.delete('/pc/radar/v1/raInterventionTypes', data, {
    confirm: { content: '确认删除当前接入方式信息' },
    success: '接入方式信息删除成功'
  });
}
/**
 * 获取所有的雷达基础结构树信息
 * @returns
 */
export function getRadarTreeNoDoList() {
  return request.get<any[]>('​/pc​/radar​/v1​/raTreeNoDo');
}

/**
 * 根据雷达id获取相应雷达树信息
 * @param id
 * @returns
 */
export function getRadarTreeListByRadarId(radarId: string) {
  return request.get<any[]>('/pc/radar/v1/raTree', { radarId });
}

/**
 * 编辑雷达结构树
 * @param ids
 * @returns
 */
export function saveRadarTree(data: { createIds: string[]; deleteIds: string[] }) {
  return request.post('/pc/radar/v1/raTrees', data, {
    confirm: {
      type: 'warning',
      content: '请确认修改当前雷达结构树'
    },
    success: '雷达结构树编辑成功'
  });
}
/**
 * 获取关重件记录
 * @param radarId
 * @param modelId
 * @returns
 */
export function getKeyPartsList(params: PageParams<{ radarId: string; modelId: string }>) {
  return request.list('/pc/radar/v1/selectRadarIdAndModelId', params);
}

export interface SaveKeyPartsParams {
  keyPartsModelId: string;
  radarId: string;
  raStoragesId: string;
}

/**
 * 保存关重件使用
 * @param data
 * @returns
 */
export function saveNewKeyParts(data: SaveKeyPartsParams) {
  return request.post('/pc/radar/v1/keyPartsSwitching', data, {
    confirm: { content: '确认切换关重件库存', type: 'warning' },
    success: '关重件设备切换成功'
  });
}
/**
 * 根据结构件id查询保障基础信息和保障类型
 * @param data
 * @returns
 */
export function getEnsureList(data: { structureId: string }) {
  return request.get<{
    ensureBaseList: { id: string; typeIdList: string[] }[];
    structureId: string;
  }>('/pc/ensureRepair/v1/ensureEnsureType2Structure', data);
}
/**
 *
 * @param data
 * @returns
 */
export function saveEnsure(data: {
  ensureBaseList: { id: string; createTypeIdList: string[]; deleteTypeIdList: string[] }[];
  structureId: string;
}) {
  return request.post('/pc​/ensureRepair​/v1​/ensurePlan2Structure', data, {
    success: '保障计划保存成功'
  });
}

export interface LifePredirectionItem {
  totalLife: string;
  activeLife: number;
  residualLife: number;
  failureTime: string;
  name: string;
  typeName: string;
}

/**
 * 获取 雷达寿命件寿命预测结果
 * @param data
 * @returns
 */
export function getLifePrediction(data: { radarId: string }) {
  return request.get<LifePredirectionItem[]>('/pc​/radar/v1/lifePrediction', data);
}
