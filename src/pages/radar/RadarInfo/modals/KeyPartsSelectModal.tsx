import BaseForm, {
  BaseModalForm,
  ProFormDatePicker,
  ProFormDependency,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { useRequest } from '@/utils/request';
import { Descriptions, Empty, Spin } from 'antd';
import moment from 'moment';
import { getStockList } from '../../keyParts/StorageHomeInfo/api';
import { SaveKeyPartsParams, saveNewKeyParts } from '../api';

const KeyPartsSelectModal: ModalFC<{ radarId: string; areaId: string; modelId: string }> = ({
  radarId,
  modelId,
  areaId,
  onSuccess,
  ...props
}) => {
  const stockList = useRequest(getStockList, {
    defaultParams: [{ pageNum: 1, pageSize: 99, keyPartsModelId: modelId, areaId }]
  });
  return (
    <BaseModalForm<SaveKeyPartsParams>
      title='关重件设备切换'
      width={640}
      {...props}
      initialValues={{ startTime: moment() }}
      onFinish={async (values) => {
        values.radarId = radarId;
        values.keyPartsModelId = modelId;
        const { ok } = await saveNewKeyParts(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <Spin spinning={stockList.loading}>
        {stockList.data && stockList.data.list.length > 0 ? (
          <ProFormRadio.Group
            label='库存'
            name='raStoragesId'
            fieldProps={{ style: { maxHeight: 400, overflow: 'auto' } }}
            options={(stockList.data ? stockList.data.list : []).map((i) => ({
              label: (
                <Descriptions column={2}>
                  <Descriptions.Item label='库存'>{i.storageNum}</Descriptions.Item>
                  <Descriptions.Item label='生产日期'>
                    {moment(i.manufactureDate).format('yyyy-MM-DD')}
                  </Descriptions.Item>
                  <Descriptions.Item label='入库日期'>
                    {moment(i.addDate).format('yyyy-MM-DD')}
                  </Descriptions.Item>
                  <Descriptions.Item label='有效期'>{i.validityDate} 个月</Descriptions.Item>
                </Descriptions>
              ),
              value: i.id,
              disabled: i.storageNum === 0
            }))}
            rules={[{ required: true, message: '请选择关重件设备库存' }]}
          />
        ) : (
          <BaseForm.Item
            label='库存'
            name='raStoragesId'
            rules={[{ required: true, message: '请选择关重件设备库存' }]}
          >
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
          </BaseForm.Item>
        )}
      </Spin>
      <ProFormDependency name={['raStoragesId']}>
        {({ raStoragesId }) => (
          <ProFormDatePicker
            label='启用日期'
            name='startTime'
            fieldProps={{
              disabledDate: (current) => {
                const value = (stockList.data ? stockList.data.list : []).find(
                  (i) => i.id === raStoragesId
                );
                return current && current < moment(value?.addDate).startOf('day');
              }
            }}
            rules={[{ required: true, message: '请选择设备启用日期' }]}
          />
        )}
      </ProFormDependency>
    </BaseModalForm>
  );
};
export default KeyPartsSelectModal;
