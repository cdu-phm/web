import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import BaseModal, { ModalFC, useModal } from '@/components/base/BaseModal';
import BaseTable from '@/components/base/BaseTable';
import { EnableStatusEnum } from '@/models';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef } from 'react';
import { RadarAccessMethodModel } from '../../data';
import { delAccessMethodInfo, getAccessMethodList } from '../api';
import AccessMethodInfoModal from './AccessMethodInfoModal';

const AccessMethodListModal: ModalFC<{ radarId: string; radarName: string; typeName: string }> = ({
  radarId,
  typeName,
  radarName,
  ...props
}) => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(AccessMethodInfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delAccessMethodInfo, {
    manual: true,
    fetchKey: ({ id }) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  return (
    <BaseModal footer={false} title='雷达接入方式' {...props}>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        cardProps={{ bodyStyle: { padding: 0 } }}
        params={{ radarId }}
        height={240}
        request={getAccessMethodList}
        headerTitle='接入方式列表'
        toolBarRender={() => [
          <BaseButton
            type='primary'
            code='createAccessMethod'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({ data: { radarId } as RadarAccessMethodModel, radarName, typeName });
            }}
          >
            新增
          </BaseButton>
        ]}
        columns={
          [
            {
              dataIndex: 'interventionType',
              title: '接入方式'
            },
            {
              dataIndex: 'account',
              title: '账号'
            },
            {
              dataIndex: 'password',
              title: '密码'
            },
            {
              dataIndex: 'isIntervention',
              title: '接入状态',
              renderText: EnableStatusEnum.getLabelByValue
            },
            {
              dataIndex: 'dataSite',
              title: '数据存放地址',
              valueType: 'code'
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              title: '操作',
              fixed: 'right',
              render: (text, record) => {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='editAccessMethod'
                      onClick={() => {
                        infoModal.load({ data: record, radarName, typeName });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='deleteAccessMethod'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run({ id: record.id, radarId });
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<RadarAccessMethodModel>[]
        }
      />
    </BaseModal>
  );
};

export default AccessMethodListModal;
