import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormDatePicker,
  ProFormInstance,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { Col, Row } from 'antd';
import moment from 'moment';
import { useRef } from 'react';
import { InServiceEnum, RadarModel } from '../../data';
import { getList as getRadarStationList } from '../../RadarStationInfo/api';
import { getList as getRadarTypeList } from '../../RadarTypeInfo/api';
import { getList as getServerTypeList } from '../../ServerTypeInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RadarModel }> = (props) => {
  const { data = {} as RadarModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  const formRef = useRef<ProFormInstance<RadarModel>>();
  return (
    <BaseModalForm<RadarModel>
      title='雷达信息'
      formRef={formRef}
      labelCol={{ style: { width: 80 } }}
      width={720}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.productionDate = values.productionDate ?? '';
        values.installTime = values.installTime ?? '';
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='雷达名称'
        name='name'
        placeholder='请输入雷达名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入雷达名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />

      <Row gutter={8}>
        <Col span={12}>
          <BaseFormSelect
            label='雷达型号'
            name='typeId'
            request={getRadarTypeList}
            fieldProps={{
              defaultSelectedOptions: [{ name: data.typeName, id: data.typeId }],
              fieldNames: { label: 'name', value: 'id' }
            }}
            placeholder='请选择雷达型号'
            rules={[{ required: true, message: '请选择雷达型号' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormSelect
            label='服务类型'
            name='serviceId'
            placeholder='请选择服务类型'
            request={getServerTypeList}
            fieldProps={{
              defaultSelectedOptions: [{ name: data.serviceName, id: data.serviceId }],
              fieldNames: { label: 'name', value: 'id' }
            }}
            rules={[{ required: true, message: '请选择服务类型' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormSelect
            label='所属站点'
            name='siteId'
            placeholder='请选择所属站点'
            request={getRadarStationList}
            rules={[{ required: true, message: '请选择所属站点' }]}
            fieldProps={{
              defaultSelectedOptions: [{ name: data.siteName, id: data.siteId }],
              fieldNames: { label: 'name', value: 'id' }
            }}
          />
        </Col>
        <Col span={12}>
          <BaseFormText
            label='生产厂家'
            name='productionFactory'
            placeholder='请输入生产厂家'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[{ type: 'string', max: 20, message: '长度不可超过20位' }]}
          />
        </Col>
      </Row>

      <Row gutter={8}>
        <Col span={8}>
          <ProFormDatePicker
            label='生产日期'
            name='productionDate'
            placeholder='请选择生产日期'
            fieldProps={{
              onChange() {
                formRef.current?.validateFields(['installTime']);
              },
              disabledDate: (current) => {
                return current && current > moment().endOf('day');
              }
            }}
            required
            rules={[
              (form) => {
                return {
                  validator: (_, value) => {
                    if (isEmpty(value)) {
                      return Promise.reject(new Error('请选择生产日期'));
                    }
                    const addDate = form.getFieldValue('installTime');
                    if (moment(value).format('YYYY-MM-DD') > moment(addDate).format('YYYY-MM-DD')) {
                      return Promise.reject(new Error('生产日期不能在架设日期之后'));
                    }
                    return Promise.resolve();
                  }
                };
              }
            ]}
          />
        </Col>
        <Col span={8}>
          <ProFormDatePicker
            label='架设日期'
            name='installTime'
            placeholder='请选择架设日期'
            fieldProps={{
              onChange() {
                formRef.current?.validateFields(['productionDate']);
              }
            }}
            required
            rules={[
              (form) => {
                return {
                  validator: (_, value) => {
                    if (isEmpty(value)) {
                      return Promise.reject(new Error('请选择架设日期'));
                    }
                    const addDate = form.getFieldValue('productionDate');
                    if (moment(value).format('YYYY-MM-DD') < moment(addDate).format('YYYY-MM-DD')) {
                      return Promise.reject(new Error('架设日期不能在生产日期之前'));
                    }
                    return Promise.resolve();
                  }
                };
              }
            ]}
          />
        </Col>
        <Col span={8}>
          <ProFormRadio.Group
            label='在役状态'
            name='status'
            radioType='button'
            options={InServiceEnum.list}
          />
        </Col>
      </Row>
      <BaseFormTextArea
        label='安装地址'
        name='installationPosition'
        placeholder='请输入安装地址'
        fieldProps={{ rows: 2 }}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='功能说明'
        name='remarks'
        placeholder='请输入雷达功能说明'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
