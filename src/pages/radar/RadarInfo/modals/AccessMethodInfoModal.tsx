import {
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { EnableStatusEnum } from '@/models';
import { isEmpty } from '@/utils/is';
import { RadarAccessMethodModel } from '../../data';
import { addAccessMethodInfo, editAccessMethodInfo } from '../api';

const AccessMethodInfoModal: ModalFC<{
  data: RadarAccessMethodModel;
  radarName: string;
  typeName: string;
}> = (props) => {
  const { data, onSuccess, radarName, typeName, ...anyProps } = props;
  const { id, radarId, isIntervention = EnableStatusEnum.enum.enable } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RadarAccessMethodModel>
      title='接入方式信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={{ ...data, isIntervention }}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.radarId = radarId;
        (values as any).radarName = radarName;
        (values as any).typeName = typeName;
        const { ok } = await (isadd ? addAccessMethodInfo : editAccessMethodInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <ProFormRadio.Group
        label='接入状态'
        name='isIntervention'
        radioType='button'
        options={EnableStatusEnum.list}
      />
      <BaseFormText
        label='接入方式'
        name='interventionType'
        placeholder='请输入雷达接入方式'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入雷达接入方式' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='账号'
        name='account'
        placeholder='请输入接入账号'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入接入账号' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='密码'
        name='password'
        placeholder='请输入接入密码'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[{ required: true, message: '请输入接入密码' }]}
      />
      <BaseFormText
        label='数据地址'
        name='dataSite'
        placeholder='请输入数据存放地址'
        rules={[{ required: true, message: '请输入数据存放地址' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default AccessMethodInfoModal;
