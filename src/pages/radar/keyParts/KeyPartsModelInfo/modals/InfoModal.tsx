import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormDependency,
  ProFormInstance,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { TrueFalseEnum } from '@/models';
import { getList as getExtensionList } from '@/pages/radar/SubExtensionInfo/api';
import { getList as getSystemList } from '@/pages/radar/SubSystemInfo/api';
import { isEmpty } from '@/utils/is';
import { Alert } from 'antd';
import { useRef } from 'react';
import { KeyPartsModelModel } from '../../../data';
import { getList as getTypeList } from '../../KeyPartsTypeInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: KeyPartsModelModel }> = (props) => {
  const { data = {} as KeyPartsModelModel, onSuccess, ...anyProps } = props;
  const { id, isLife = TrueFalseEnum.enum.true } = data;
  const isadd = isEmpty(id);
  const formRef = useRef<ProFormInstance<KeyPartsModelModel>>();
  return (
    <BaseModalForm<KeyPartsModelModel>
      title='关重件型号信息'
      formRef={formRef}
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={{ ...data, isLife }}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='分系统'
        name='systemId'
        request={getSystemList}
        fieldProps={{
          defaultSelectedOptions: [{ name: data.systemName, id: data.systemId }],
          fieldNames: { label: 'name', value: 'id' },
          onChange: () => {
            formRef.current?.setFieldsValue({ extensionId: undefined });
          }
        }}
        placeholder='请选择分系统'
        rules={[{ required: true, message: '请选择分系统' }]}
      />
      <ProFormDependency name={['systemId']}>
        {({ systemId }) => (
          <BaseFormSelect
            label='分机'
            name='extensionId'
            request={getExtensionList}
            hiddenItem={isEmpty(systemId)}
            hiddenRender={() => <Alert message='请先选择分系统' type='warning' />}
            fieldProps={{
              params: { systemId },
              defaultSelectedOptions: [{ name: data.extensionName, id: data.extensionId }],
              fieldNames: { label: 'name', value: 'id' }
            }}
            placeholder='请选择分机'
            rules={[{ required: true, message: '请选择分机' }]}
          />
        )}
      </ProFormDependency>
      <BaseFormSelect
        label='类型'
        name='typeId'
        request={getTypeList}
        fieldProps={{
          defaultSelectedOptions: [{ name: data.typeName, id: data.typeId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择关重件类型'
        rules={[{ required: true, message: '请选择关重件类型' }]}
      />
      <BaseFormText
        label='型号'
        name='name'
        placeholder='请输入关重件型号'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入关重件型号' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <ProFormRadio.Group
        label='寿命件'
        name='isLife'
        radioType='button'
        options={TrueFalseEnum.list}
      />
      <BaseFormTextArea
        label='用途'
        name='use'
        rules={[{ required: true, message: '请输入关重件用途' }]}
        placeholder='请输入关重件用途'
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
