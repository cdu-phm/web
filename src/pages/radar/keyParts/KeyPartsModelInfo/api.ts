import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { KeyPartsModelModel } from '../../data';
/**
 * 分页获取关重件型号信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ extensionId?: string; name?: string }>) {
  return request.list('/pc/radar/v1/raKeyPartsModels', data);
}

/**
 * 添加关重件型号信息
 * @param data
 * @returns
 */
export function addInfo(data: KeyPartsModelModel) {
  return request.post('/pc/radar/v1/raKeyPartsModels', data, { success: '关重件型号信息新增成功' });
}

/**
 * 编辑关重件型号信息
 * @param data
 * @returns
 */
export function editInfo(data: KeyPartsModelModel) {
  return request.put('/pc/radar/v1/raKeyPartsModels', data, { success: '关重件型号信息编辑成功' });
}
/**
 * 删除关重件型号信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raKeyPartsModels',
    { id },
    { confirm: { content: '确认删除当前关重件型号信息' }, success: '关重件型号信息删除成功' }
  );
}
