import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { TrueFalseEnum } from '@/models';
import { useRequest } from '@/utils/request';
import { BankOutlined, DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space, Tag } from 'antd';
import { useRef, useState } from 'react';
import { KeyPartsModelModel } from '../../data';
import { getList as getExtensionList } from '../../SubExtensionInfo/api';
import StockListModal from '../StorageHomeInfo/modals/StockListModal';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const KeyPartsModelInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const stockListModal = useModal(StockListModal, { isStorageHome: false });

  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string; extensionId?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSelect
            request={getExtensionList}
            placeholder='请选择分机'
            defaultValue={searchInfo.extensionId}
            allowClear
            style={{ width: 160 }}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, extensionId: v });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入型号名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '关重件型号'
            },
            {
              dataIndex: 'systemName',
              title: '所属分系统'
            },
            {
              dataIndex: 'extensionName',
              title: '所属分机'
            },
            {
              dataIndex: 'typeName',
              title: '关重件名称'
            },
            {
              dataIndex: 'isLife',
              title: '寿命件',
              render: (text, record) => {
                const t = TrueFalseEnum.getLabelByValue(record.isLife);
                const color = record.isLife ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: 'use',
              title: '用途'
            },
            {
              dataIndex: 'remarks',
              title: '备注'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                    />
                    <BaseAction
                      color='blue'
                      title='库存信息'
                      code='loadStorage'
                      icon={<BankOutlined />}
                      onClick={() => {
                        stockListModal.load({ id: record.id });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<KeyPartsModelModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default KeyPartsModelInfo;
