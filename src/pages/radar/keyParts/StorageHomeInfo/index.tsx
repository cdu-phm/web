import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import useUserStore from '@/store/user';
import { useRequest } from '@/utils/request';
import { BankOutlined, DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Select, Space } from 'antd';
import { useRef, useState } from 'react';
import { StorageHomeModel } from '../../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';
import StockListModal from './modals/StockListModal';

const StorageHomeInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const stockListModal = useModal(StockListModal);
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const {
    userinfo: { area }
  } = useUserStore();
  const [searchInfo, setSearchInfo] = useState<{ name?: string; areaId?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            type='primary'
            icon={<PlusOutlined />}
            code='create'
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        request={getList}
        toolBarRender={() => [
          <Select
            placeholder='请选择库房所属区域'
            defaultValue={searchInfo.areaId}
            allowClear
            style={{ width: 180 }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, areaId: v });
            }}
            options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入库房名称查询'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        columns={
          [
            {
              dataIndex: 'name',
              title: '库房名称'
            },
            {
              dataIndex: 'areaName',
              title: '所属区域'
            },
            {
              dataIndex: 'address',
              title: '地址'
            },
            {
              dataIndex: 'remarks',
              title: '备注'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                      icon={<EditOutlined />}
                      title='编辑'
                    />
                    <BaseAction
                      color='blue'
                      icon={<BankOutlined />}
                      title='查看库存'
                      code='loadStorage'
                      onClick={() => {
                        stockListModal.load({ id: record.id, areaId: record.areaId });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<StorageHomeModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default StorageHomeInfo;
