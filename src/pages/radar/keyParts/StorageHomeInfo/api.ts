import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { StockModel, StorageHomeModel } from '../../data';
/**
 * 分页获取库房信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/raStorageHomes', data);
}

/**
 * 添加库房信息
 * @param data
 * @returns
 */
export function addInfo(data: StorageHomeModel) {
  return request.post('/pc/radar/v1/raStorageHomes', data, { success: '库房信息新增成功' });
}

/**
 * 编辑库房信息
 * @param data
 * @returns
 */
export function editInfo(data: StorageHomeModel) {
  return request.put('/pc/radar/v1/raStorageHomes', data, { success: '库房信息编辑成功' });
}
/**
 * 删除库存信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raStorageHomes',
    { id },
    { confirm: { content: '确认删除当前库房信息' }, success: '库房信息删除成功' }
  );
}

/**
 * 分页获取库存信息
 * @param data
 * @returns
 */
export function getStockList(
  data: PageParams<{
    keyPartsModelId?: string;
    areaId?: string;
    storageHomeId?: string;
    startTime?: string;
    endTime?: string;
  }>
) {
  return request.list<StockModel>('/pc/radar/v1/raStorages', data);
}

/**
 * 添加库存信息
 * @param data
 * @returns
 */
export function addStockInfo(data: StockModel) {
  return request.post('/pc/radar/v1/raStorages', data, { success: '库存信息新增成功' });
}

/**
 * 编辑库存信息
 * @param data
 * @returns
 */
export function editStockInfo(data: StockModel) {
  return request.put('/pc/radar/v1/raStorages', data, { success: '库存信息编辑成功' });
}
/**
 * 删除库存信息
 * @param id
 * @returns
 */
export function delStockInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raStorages',
    { id },
    { confirm: { content: '确认删除当前库存信息' }, success: '库存信息删除成功' }
  );
}
