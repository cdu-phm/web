import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import BaseModal, { ModalFC, useModal } from '@/components/base/BaseModal';
import BaseTable from '@/components/base/BaseTable';
import { StockModel } from '@/pages/radar/data';
import useUserStore from '@/store/user';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { DatePicker, Select, Space } from 'antd';
import moment from 'moment';
import { useRef, useState } from 'react';
import { delStockInfo, getStockList } from '../api';
import StockInfoModal from './StockInfoModal';

const StockListModal: ModalFC<{ id: string; areaId?: string; isStorageHome?: boolean }> = ({
  id,
  areaId,
  isStorageHome = true,
  ...props
}) => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(StockInfoModal, {
    isStorageHome,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delStockInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const idName = isStorageHome ? 'storageHomeId' : 'keyPartsModelId';
  const {
    userinfo: { area }
  } = useUserStore();
  const [searchInfo, setSearchInfo] = useState<{
    areaId?: string;
    startTime?: string;
    endTime?: string;
  }>({ areaId });
  return (
    <BaseModal footer={false} title='库存列表' {...props}>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        cardProps={{ bodyStyle: { padding: 0 } }}
        params={{ [idName]: id, ...searchInfo }}
        height={240}
        request={getStockList}
        headerTitle={
          <BaseButton
            type='primary'
            code='createStorage'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({ areaId, data: { [idName]: id } as unknown as StockModel });
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          isStorageHome ? null : (
            <Select
              placeholder='请选择库房所属区域'
              defaultValue={searchInfo.areaId}
              allowClear
              style={{ width: 200 }}
              onChange={(v) => {
                setSearchInfo({ ...searchInfo, areaId: v });
              }}
              options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
            />
          ),
          <>
            入库时间
            <DatePicker.RangePicker
              defaultValue={
                searchInfo.startTime
                  ? [moment(searchInfo.startTime), moment(searchInfo.endTime)]
                  : undefined
              }
              onChange={(v, [startTime, endTime]) => {
                setSearchInfo({ ...searchInfo, startTime, endTime });
              }}
            />
          </>
        ]}
        columns={
          [
            {
              dataIndex: 'areaName',
              title: '区域'
            },
            isStorageHome
              ? {
                  dataIndex: 'keyPartsModelName',
                  title: '关重件型号'
                }
              : {
                  dataIndex: 'storageHomeName',
                  title: '库房'
                },
            {
              dataIndex: 'storageNum',
              title: '库存',
              width: 120
            },
            {
              dataIndex: 'addDate',
              title: '入库日期',
              valueType: 'date'
            },
            {
              dataIndex: 'manufactureDate',
              title: '生产日期',
              valueType: 'date'
            },
            {
              dataIndex: 'validityDate',
              title: '有效期',
              width: 120,
              renderText: (text) => `${text} 月`
            },
            {
              title: '操作',
              fixed: 'right',
              render: (text, record) => {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      code='editStorage'
                      icon={<EditOutlined />}
                      title='编辑'
                      onClick={() => {
                        infoModal.load({ areaId, data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      code='deleteStorage'
                      icon={<DeleteOutlined />}
                      title='删除'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<StockModel>[]
        }
      />
    </BaseModal>
  );
};

export default StockListModal;
