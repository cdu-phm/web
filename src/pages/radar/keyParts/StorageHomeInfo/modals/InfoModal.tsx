import {
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormSelect
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import useUserStore from '@/store/user';
import { isEmpty } from '@/utils/is';
import { StorageHomeModel } from '../../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: StorageHomeModel }> = (props) => {
  const { data = {} as StorageHomeModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  const {
    userinfo: { area }
  } = useUserStore();
  return (
    <BaseModalForm<StorageHomeModel>
      title='库房信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入库房名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入库房名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <ProFormSelect
        label='所属区域'
        name='areaId'
        placeholder='请选择库房所属区域'
        rules={[{ required: true, message: '请选择库房所属区域' }]}
        options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
      />
      <BaseFormTextArea
        label='地址'
        name='address'
        placeholder='请输入库房地址'
        rules={[{ required: true, message: '请输入库房地址' }]}
      />

      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
