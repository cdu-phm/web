import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { KeyPartsTypeModel } from '../../data';
/**
 * 分页获取关重件名称信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/raKeyPartsTypes', data);
}

/**
 * 添加关重件名称信息
 * @param data
 * @returns
 */
export function addInfo(data: KeyPartsTypeModel) {
  return request.post('/pc/radar/v1/raKeyPartsTypes', data, { success: '关重件名称信息新增成功' });
}

/**
 * 编辑关重件名称信息
 * @param data
 * @returns
 */
export function editInfo(data: KeyPartsTypeModel) {
  return request.put('/pc/radar/v1/raKeyPartsTypes', data, { success: '关重件名称信息编辑成功' });
}
/**
 * 删除关重件名称信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raKeyPartsTypes',
    { id },
    { confirm: { content: '确认删除当前关重件名称信息' }, success: '关重件名称信息删除成功' }
  );
}
