import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RadarStationModel } from '../data';
/**
 * 分页获取雷达站信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/raStations', data);
}

/**
 * 添加雷达站信息
 * @param data
 * @returns
 */
export function addInfo(data: RadarStationModel) {
  return request.post('/pc/radar/v1/raStations', data, { success: '雷达站信息新增成功' });
}

/**
 * 编辑雷达站信息
 * @param data
 * @returns
 */
export function editInfo(data: RadarStationModel) {
  return request.put('/pc/radar/v1/raStations', data, { success: '雷达站信息编辑成功' });
}
/**
 * 删除雷达站信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raStations',
    { id },
    { confirm: { content: '确认删除当前雷达站信息' }, success: '雷达站信息删除成功' }
  );
}
