import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { RadarStationModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const RadarStationInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [name, setName] = useState('');
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSearch
            placeholder='请输入站点名称搜索'
            defaultValue={name}
            onChange={(v) => setName(v)}
          />
        ]}
        params={{ name }}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '站点名称'
            },
            {
              dataIndex: 'longitude',
              title: '经度'
            },
            {
              dataIndex: 'latitude',
              title: '纬度'
            },
            {
              dataIndex: 'stationAreaName',
              title: '服务区域'
            },
            {
              dataIndex: 'unit',
              title: '单位'
            },
            {
              dataIndex: 'unitNumber',
              title: '部队番号'
            },
            {
              dataIndex: 'phone',
              title: '联系方式'
            },
            {
              dataIndex: 'phoneStation',
              title: '通讯地址'
            },
            {
              dataIndex: 'remarks',
              title: '备注'
            },
            {
              title: '操作',
              fixed: 'right',
              width: 100,
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<RadarStationModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default RadarStationInfo;
