import {
  BaseFormDight,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormSelect
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import useUserStore from '@/store/user';
import { isEmpty } from '@/utils/is';
import { Col, Row } from 'antd';
import { RadarStationModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RadarStationModel }> = (props) => {
  const { data = {} as RadarStationModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isedit = !isEmpty(id);
  const { userinfo } = useUserStore();
  const { area } = userinfo;
  return (
    <BaseModalForm<RadarStationModel>
      title='站点信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editInfo : addInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入站点名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入站点名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <ProFormSelect
        label='服务区域'
        name='areaId'
        placeholder='请选择服务区域'
        rules={[{ required: true, message: '请选择服务区域' }]}
        options={area.map((i) => ({ label: i.areaName, value: i.areaId }))}
      />
      <Row>
        <Col span={12}>
          <BaseFormDight
            label='经度'
            name='longitude'
            placeholder='请输入经度'
            rules={[{ required: true, message: '请输入经度' }]}
            fieldProps={{ step: 0.001, precision: 6 }}
          />
        </Col>
        <Col span={12}>
          <BaseFormDight
            label='纬度'
            name='latitude'
            placeholder='请输入纬度'
            rules={[{ required: true, message: '请输入纬度' }]}
            fieldProps={{ step: 0.001, precision: 6 }}
          />
        </Col>
      </Row>
      <BaseFormText
        label='联系方式'
        name='phone'
        placeholder='请输入联系方式'
        rules={[
          { required: true, message: '请输入联系方式' },
          {
            pattern:
              /(^(?:(?:\d{3}-)?\d{8}|^(?:\d{4}-)?\d{7,8})(?:-\d+)?$)|(^(?:(?:\+|00)86)?1[3-9]\d{9}$)/,
            message: '请输入正确的电话号码,可输入座机或手机号'
          }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='通信地址'
        name='phoneStation'
        placeholder='请输入通信地址'
      />
      <BaseFormText
        label='单位'
        name='unit'
        placeholder='请输入单位'
        fieldProps={{ maxLength: 50, showCount: true }}
        rules={[{ type: 'string', max: 50, message: '长度不可超过50位' }]}
      />
      <BaseFormText
        label='部队番号'
        name='unitNumber'
        placeholder='请输入部队番号'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[{ type: 'string', max: 20, message: '长度不可超过20位' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
