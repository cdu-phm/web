import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { ServerTypeModel } from '../data';
/**
 * 分页获取服务类型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/raServerTypes', data);
}

/**
 * 添加服务类型信息
 * @param data
 * @returns
 */
export function addInfo(data: ServerTypeModel) {
  return request.post('/pc/radar/v1/raServerTypes', data, { success: '服务类型信息新增成功' });
}

/**
 * 编辑服务类型信息
 * @param data
 * @returns
 */
export function editInfo(data: ServerTypeModel) {
  return request.put('/pc/radar/v1/raServerTypes', data, { success: '服务类型信息编辑成功' });
}
/**
 * 删除服务类型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raServerTypes',
    { id },
    { confirm: { content: '确认删除当前服务类型信息' }, success: '服务类型信息删除成功' }
  );
}
