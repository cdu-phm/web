import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RadarSubSystemModel } from '../data';
/**
 * 分页获取分系统信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ radarId?: string; name?: string }>) {
  return request.list<RadarSubSystemModel>('/pc/radar/v1/raSystems', data);
}

/**
 * 添加分系统信息
 * @param data
 * @returns
 */
export function addInfo(data: RadarSubSystemModel) {
  return request.post('/pc/radar/v1/raSystems', data, { success: '分系统信息新增成功' });
}

/**
 * 编辑分系统信息
 * @param data
 * @returns
 */
export function editInfo(data: RadarSubSystemModel) {
  return request.put('/pc/radar/v1/raSystems', data, { success: '分系统信息编辑成功' });
}
/**
 * 删除分系统信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/raSystems',
    { id },
    { confirm: { content: '确认删除当前分系统信息' }, success: '分系统信息删除成功' }
  );
}
