import {
  BaseFormDight,
  BaseFormText,
  BaseFormTextArea,
  BaseFormUpload,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { Col, Image, Row } from 'antd';
import { RadarSubSystemModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RadarSubSystemModel }> = (props) => {
  const { data = {} as RadarSubSystemModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RadarSubSystemModel>
      title='分系统信息'
      labelCol={{ style: { width: 90 } }}
      width={720}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='分系统名称'
        name='name'
        placeholder='请输入分系统名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入分系统名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />

      <Row>
        <Col span={12}>
          <BaseFormDight
            label='权重'
            name='weight'
            placeholder='请输入分系统权重'
            fieldProps={{ min: 0, step: 1 }}
            rules={[{ required: true, message: '请输入分系统权重' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormDight
            label='显示序号'
            name='displaySerialNum'
            placeholder='请输入显示序号'
            fieldProps={{ min: 0, step: 1, precision: 0 }}
            rules={[{ required: true, message: '请输入分系统显示序号' }]}
          />
        </Col>
        <Col span={8}>
          <BaseFormUpload
            label='健康图片'
            name='perfectIcon'
            accept='.jpg,.png,.gif'
            extra=''
            fieldProps={{
              showUrl: false,
              preview: (url) => (
                <div style={{ marginTop: 10 }}>
                  {url && <Image src={url} width={100} height={100} />}
                </div>
              )
            }}
          />
        </Col>
        <Col span={8}>
          <BaseFormUpload
            label='弱化图片'
            name='lowIcon'
            extra=''
            accept='.jpg,.png,.gif'
            fieldProps={{
              showUrl: false,
              preview: (url) => (
                <div style={{ marginTop: 10 }}>
                  {url && <Image src={url} width={100} height={100} />}
                </div>
              )
            }}
          />
        </Col>
        <Col span={8}>
          <BaseFormUpload
            label='故障图片'
            extra=''
            accept='.jpg,.png,.gif'
            name='faultIcon'
            fieldProps={{
              showUrl: false,
              preview: (url) => (
                <div style={{ marginTop: 10 }}>
                  {url && <Image src={url} width={100} height={100} />}
                </div>
              )
            }}
          />
        </Col>
        <Col span={24} style={{ textAlign: 'center', marginBottom: 20 }}>
          只能上传jpg、png、gif，且不可超过100M
        </Col>
      </Row>
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
