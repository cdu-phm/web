import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { ServerAreaModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: ServerAreaModel }> = (props) => {
  const { data = {} as ServerAreaModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isedit = !isEmpty(id);
  return (
    <BaseModalForm<ServerAreaModel>
      title='服务区域信息'
      labelCol={{ style: { width: 60 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isedit ? editInfo : addInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入服务区域名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入服务区域名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        placeholder='请输入备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
