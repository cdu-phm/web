import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { ServerAreaModel } from '../data';
/**
 * 分页获取服务区域信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/serverAreas', data);
}

/**
 * 添加服务区域信息
 * @param data
 * @returns
 */
export function addInfo(data: ServerAreaModel) {
  return request.post('/pc/radar/v1/serverAreas', data, { success: '服务区域信息新增成功' });
}

/**
 * 编辑服务区域信息
 * @param data
 * @returns
 */
export function editInfo(data: ServerAreaModel) {
  return request.put('/pc/radar/v1/serverAreas', data, { success: '服务区域信息编辑成功' });
}
/**
 * 删除服务区域信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/serverAreas',
    { id },
    { confirm: { content: '确认删除当前服务区域信息' }, success: '服务区域信息删除成功' }
  );
}
