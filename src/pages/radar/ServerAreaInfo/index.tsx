import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { ServerAreaModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const ServerAreaInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [name, setName] = useState<string>();
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSearch
            placeholder='请输入区域名称搜索'
            defaultValue={name}
            onChange={(v) => setName(v)}
          />
        ]}
        params={{ name }}
        request={getList}
        columns={
          [
            {
              dataIndex: 'name',
              title: '服务区域名称'
            },
            {
              dataIndex: 'remarks',
              title: '备注'
            },
            {
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<ServerAreaModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default ServerAreaInfo;
