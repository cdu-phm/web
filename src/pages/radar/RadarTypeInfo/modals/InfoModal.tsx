import {
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormSelect
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RadarTypeModel, RadarTypeShapeEnum } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: RadarTypeModel }> = (props) => {
  const { data = {} as RadarTypeModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<RadarTypeModel>
      title='雷达型号信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入雷达型号名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入雷达型号名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='型号标识'
        name='sign'
        placeholder='请输入雷达型号标识'
        rules={[
          { required: true, message: '请输入雷达型号标识' },
          {
            pattern: /^[a-zA-Z][0-9a-zA-Z_-]*$/g,
            message: '型号标识只能包括大小写字母、数字、下划线、短横线,只能以字母作为开头'
          }
        ]}
      />
      <ProFormSelect
        label='形状'
        name='tabShap'
        placeholder='请选择型号形状'
        options={RadarTypeShapeEnum.list}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
        placeholder='请输入备注'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
