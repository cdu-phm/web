import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RadarTypeModel } from '../data';
/**
 * 分页获取雷达型号信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/fiRadarTypes', data);
}

/**
 * 添加雷达型号信息
 * @param data
 * @returns
 */
export function addInfo(data: RadarTypeModel) {
  return request.post('/pc/radar/v1/fiRadarTypes', data, { success: '雷达型号信息新增成功' });
}

/**
 * 编辑雷达型号信息
 * @param data
 * @returns
 */
export function editInfo(data: RadarTypeModel) {
  return request.put('/pc/radar/v1/fiRadarTypes', data, { success: '雷达型号信息编辑成功' });
}
/**
 * 删除雷达型号信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/fiRadarTypes',
    { id },
    { confirm: { content: '确认删除当前雷达型号信息' }, success: '雷达型号信息删除成功' }
  );
}
