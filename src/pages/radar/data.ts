import { BaseModel, EnableStatusEnumValueType, TrueFalseEnumValueType } from '@/models';
import { Enum, EnumValueType } from '@/utils/enum';

export interface ServerAreaModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface RadarStationModel extends BaseModel {
  name: string;
  areaId: string;
  stationAreaName: string;
  latitude: number;
  longitude: number;
  phone: string;
  phoneStation: string;
  unit: string;
  unitNumber: string;
  remarks?: string;
}

export interface ServerTypeModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface RadarTypeModel extends BaseModel {
  name: string;
  tabShap: string;
  remarks?: string;
}

export const InServiceEnum = Enum({
  在役: 1,
  不在役: 0
} as const);

export const RadarTypeShapeEnum = Enum({
  rect: {
    label: '矩形',
    value: 'rect'
  },
  triangle: {
    label: '三角形',
    value: 'triangle'
  },
  diamond: {
    label: '钻石形',
    value: 'diamond'
  },
  pentagram: {
    label: '五角星形',
    value: 'pentagram'
  },
  trapezium: {
    label: '梯形',
    value: 'trapezium'
  },
  plum: {
    label: '梅花形',
    value: 'plum'
  }
});

export interface RadarModel extends BaseModel {
  name: string;
  siteId: string;
  siteName: string;
  areaId: string;
  areaName: string;
  serviceId: string;
  serviceName: string;
  typeId: string;
  typeName: string;
  status: EnableStatusEnumValueType;
  dataTableName: string;
  installTime: string;
  installationPosition: string;
  productionDate: string;
  productionFactory: string;
  remarks?: string;
}

export interface RadarAccessMethodModel extends BaseModel {
  radarId: string;
  account: string;
  password: string;
  isIntervention: EnableStatusEnumValueType;
  interventionType: string;
  dataSite: string;
  remarks: string;
}

export interface RadarSubSystemModel extends BaseModel {
  name: string;
  weight: number;
  faultIcon: string;
  lowIcon: string;
  perfectIcon: string;
  displaySerialNum: number;
  remarks?: string;
}
export interface RadarSubExtensionModel extends BaseModel {
  name: string;
  systemId: string;
  systemName: string;
  remarks?: string;
}

export interface KeyPartsTypeModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface KeyPartsModelModel extends BaseModel {
  name: string;
  typeId: string;
  typeName: string;
  extensionId: string;
  extensionName: string;
  systemId: string;
  systemName: string;
  isLife: TrueFalseEnumValueType;
  use: string;
  remarks?: string;
}

export interface StorageHomeModel extends BaseModel {
  name: string;
  address: string;
  areaId: string;
  areaName: string;
  remarks?: string;
}

export interface StockModel extends BaseModel {
  keyPartsModelId: string;
  keyPartsModelName: string;
  storageHomeId: string;
  storageHomeName: string;
  storageNum: number;
  validityDate: number;
  addDate: string;
  areaId: string;
  areaName: string;
  manufactureDate: string;
}

export const RadarTreeTypeEnum = Enum({
  radar: {
    label: '雷达',
    value: 0
  },
  system: {
    label: '分系统',
    value: 1
  },
  extension: {
    label: '分机',
    value: 2
  },
  keyParts: {
    label: '关重件',
    value: 3
  }
});

export interface RadarTreeItem {
  id: string;
  name: string;
  treeId: string;
  type: EnumValueType<typeof RadarTreeTypeEnum.enum>;
  children?: RadarTreeItem[];
}

export interface KeyPartsModel extends BaseModel {
  radarId: string;
  keyPartsModelId: string;
  status: EnableStatusEnumValueType;
  date: string;
}
