import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { ConfigProvider, Empty, Space, Table } from 'antd';
import { FC, useRef, useState } from 'react';
import { DisplayLocationModel } from '../data';
import { delInfo, getChildList, getList } from './api';
import InfoModal from './modals/InfoModal';

const DisplayLocationTable: FC<{
  pid?: string;
  radarFileType?: string;
  radarFileTypeName?: string;
}> = (props) => {
  const { pid, radarFileType, radarFileTypeName } = props;
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string; pid?: string }>({ pid });
  const title = pid ? '排序' : '显示';
  return (
    <ConfigProvider
      renderEmpty={() => (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='暂无数据'>
          {pid && (
            <BaseButton
              type='primary'
              code='create'
              icon={<PlusOutlined />}
              onClick={() => {
                infoModal.load({ pid, radarFileType, radarFileTypeName });
              }}
            >
              新增排序位置
            </BaseButton>
          )}
        </Empty>
      )}
    >
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          pid ? (
            '排序位置'
          ) : (
            <BaseButton
              type='primary'
              code='create'
              icon={<PlusOutlined />}
              onClick={() => {
                infoModal.load({ pid });
              }}
            >
              新增显示位置
            </BaseButton>
          )
        }
        toolBarRender={() => [
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder={`请输入${title}位置搜索`}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={pid ? getChildList : getList}
        summary={(data) =>
          data.length > 0 &&
          pid && (
            <Table.Summary.Row style={{ position: 'relative', height: 32 }}>
              <td style={{ position: 'absolute', left: 0, width: '100%', padding: 0 }}>
                <BaseButton
                  type='dashed'
                  icon={<PlusOutlined />}
                  onClick={() => {
                    infoModal.load({ pid, radarFileType, radarFileTypeName });
                  }}
                  block
                >
                  新增排序位置
                </BaseButton>
              </td>
            </Table.Summary.Row>
          )
        }
        expandable={
          pid
            ? undefined
            : {
                expandedRowRender(record) {
                  return (
                    <DisplayLocationTable
                      pid={record.id}
                      radarFileType={record.radarFileType}
                      radarFileTypeName={record.radarFileTypeName}
                    />
                  );
                }
              }
        }
        columns={
          [
            {
              dataIndex: 'name',
              title: `${title}位置`
            },
            {
              dataIndex: 'radarFileTypeName',
              hideInTable: pid,
              title: '雷达文件类型'
            },
            {
              dataIndex: 'displaySerialNum',
              title: `${title}序号`
            },
            {
              dataIndex: 'description',
              title: '位置说明'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record, pid });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<DisplayLocationModel>[]
        }
      />
    </ConfigProvider>
  );
};
const Info = () => {
  return (
    <BasePageFrame>
      <DisplayLocationTable />
    </BasePageFrame>
  );
};

export default Info;
