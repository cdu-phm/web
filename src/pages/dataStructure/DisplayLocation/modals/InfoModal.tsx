import {
  BaseFormDight,
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { DisplayLocationModel } from '../../data';
import { getList } from '../../FileTypeInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{
  pid?: string;
  radarFileType?: string;
  radarFileTypeName?: string;
  data?: DisplayLocationModel;
}> = (props) => {
  const {
    pid,
    radarFileType,
    radarFileTypeName,
    data = { radarFileType, radarFileTypeName } as DisplayLocationModel,
    onSuccess,
    ...anyProps
  } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  const title = pid ? '排序' : '显示';
  return (
    <BaseModalForm<DisplayLocationModel>
      title={`${title}位置信息`}
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.pid = pid;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='文件类型'
        name='radarFileType'
        readonly={Boolean(pid)}
        placeholder={`请选择雷达文件类型`}
        request={getList}
        fieldProps={{
          fieldNames: { label: 'name', value: 'id' },
          defaultSelectedOptions: [{ name: data.radarFileTypeName, id: data.radarFileType }]
        }}
        rules={[{ required: true, message: `请选择雷达文件类型` }]}
      />
      <BaseFormText
        label={`${title}位置`}
        name='name'
        placeholder={`请输入${title}位置`}
        rules={[{ required: true, message: `请输入${title}位置` }]}
      />
      <BaseFormDight
        label={`${title}序号`}
        placeholder={`请输入${title}序号`}
        name='displaySerialNum'
        fieldProps={{ min: 0, step: 1, precision: 0 }}
        rules={[{ required: true, message: `请输入${title}序号` }]}
      />
      <BaseFormTextArea label='位置说明' name='description' placeholder='请输入位置说明' />
    </BaseModalForm>
  );
};

export default InfoModal;
