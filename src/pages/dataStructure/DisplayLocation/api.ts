import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { DisplayLocationModel } from '../data';
/**
 * 分页获取显示位置
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc​/fi​/v1​/displayLocationInfos', data);
}

/**
 * 分页获取显示位置
 * @param data
 * @returns
 */
export function getChildList(data: PageParams<{ pid?: string; name?: string }>) {
  return request.list('/pc​/fi​/v1​/displayLocationInfosChild', data);
}

/**
 * 添加显示位置
 * @param data
 * @returns
 */
export function addInfo(data: DisplayLocationModel) {
  return request.post('/pc​/fi​/v1​/displayLocationInfos', data, { success: '位置新增成功' });
}

/**
 * 编辑显示位置
 * @param data
 * @returns
 */
export function editInfo(data: DisplayLocationModel) {
  return request.put('/pc​/fi​/v1​/displayLocationInfos', data, { success: '位置编辑成功' });
}
/**
 * 删除显示位置
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc​/fi​/v1​/displayLocationInfos',
    { id },
    { confirm: { content: '确认删除当前位置' }, success: '位置删除成功' }
  );
}
