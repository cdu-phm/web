import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { FileTypeModel } from '../data';
/**
 * 分页获取文件类型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/fi/v1/fiFileTypes', data);
}

/**
 * 添加文件类型信息
 * @param data
 * @returns
 */
export function addInfo(data: FileTypeModel) {
  return request.post('/pc/fi/v1/fiFileTypes', data, { success: '文件类型信息新增成功' });
}

/**
 * 编辑文件类型信息
 * @param data
 * @returns
 */
export function editInfo(data: FileTypeModel) {
  return request.put('/pc/fi/v1/fiFileTypes', data, { success: '文件类型信息编辑成功' });
}
/**
 * 删除文件类型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/fi/v1/fiFileTypes',
    { id },
    { confirm: { content: '确认删除当前文件类型信息' }, success: '文件类型信息删除成功' }
  );
}
