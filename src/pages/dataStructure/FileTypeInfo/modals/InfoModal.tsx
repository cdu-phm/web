import {
  BaseFormDight,
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { getList } from '@/pages/radar/RadarTypeInfo/api';
import { isEmpty } from '@/utils/is';
import { FileTypeModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: FileTypeModel }> = (props) => {
  const { data = {} as FileTypeModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<FileTypeModel>
      title='文件类型信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        label='雷达型号'
        name='radarTypeId'
        request={getList}
        fieldProps={{
          defaultSelectedOptions: [{ name: data.radarTypeName, id: data.radarTypeId }],
          fieldNames: { label: 'name', value: 'id' }
        }}
        placeholder='请选择雷达型号'
        rules={[{ required: true, message: '请选择雷达型号' }]}
      />
      <BaseFormText
        label='类型名称'
        name='name'
        placeholder='请输入文件类型名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入文件类型名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormDight
        label='文件长度'
        name='length'
        fieldProps={{ step: 1, min: 1 }}
        rules={[{ required: true, message: '请输入文件长度' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
        placeholder='请输入文件类型描述'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
