import { EnableStatusEnumValueType, PageParams, PageTypeEnum } from '@/models';
import { request } from '@/utils/request';
import { RealFileStrutureModel, SubSystemStatusModel } from '../data';
/**
 * 分页获取文件结构信息
 * @param data
 * @returns
 */
export function getList(
  data: PageParams<{
    name?: string;
    isStatus?: EnableStatusEnumValueType;
    dataTypeId?: string;
    fileTypeId?: string;
  }>
) {
  return request.list('/pc/fi/v1/fiRealTimeFileStructures', data);
}
/**
 * 查询所有文件结构信息
 * @param data
 * @returns
 */
export function getAllList(data: {
  name?: string;
  systemId?: string;
  isStatus?: EnableStatusEnumValueType;
  dataTypeId?: string;
  fileTypeId?: string;
}) {
  return request.get<RealFileStrutureModel[]>('/pc/fi/v1/fiRealTimeFileStructures', {
    ...data,
    type: PageTypeEnum.enum.all
  });
}

/**
 * 添加文件结构信息
 * @param data
 * @returns
 */
export function addInfo(data: RealFileStrutureModel) {
  return request.post('/pc/fi/v1/fiRealTimeFileStructures', data, {
    success: '文件结构信息新增成功'
  });
}

/**
 * 编辑文件结构信息
 * @param data
 * @returns
 */
export function editInfo(data: RealFileStrutureModel) {
  return request.put('/pc/fi/v1/fiRealTimeFileStructures', data, {
    success: '文件结构信息编辑成功'
  });
}
/**
 * 删除文件结构信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/fi/v1/fiRealTimeFileStructures',
    { id },
    { confirm: { content: '确认删除当前文件结构信息' }, success: '文件结构信息删除成功' }
  );
}

/**
 * 获取子系统状态文件信息
 * @param data
 * @returns
 */
export function getSystemStatusList(data: PageParams<{ id: string }>) {
  return request.list('/pc/fi/v1/fiSubsystemStatusDatas', data);
}

/**
 * 添加子系统状态文件信息
 * @param data
 * @returns
 */
export function addSystemStatusInfo(data: SubSystemStatusModel) {
  return request.post('/pc/fi/v1/fiSubsystemStatusDatas', data, {
    success: '子系统状态信息新增成功'
  });
}

/**
 * 编辑子系统状态文件信息
 * @param data
 * @returns
 */
export function editSystemStatusInfo(data: SubSystemStatusModel) {
  return request.put('/pc/fi/v1/fiSubsystemStatusDatas', data, {
    success: '子系统状态信息编辑成功'
  });
}
/**
 * 删除子系统状态文件信息
 * @param id
 * @returns
 */
export function delSystemStatusInfo(id: string) {
  return request.delete(
    '/pc/fi/v1/fiSubsystemStatusDatas',
    { id },
    { confirm: { content: '确认删除当前子系统状态信息' }, success: '子系统状态信息删除成功' }
  );
}
