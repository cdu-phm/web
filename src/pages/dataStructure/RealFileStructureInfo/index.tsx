import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { EnableStatusEnumValueType, TrueFalseEnum } from '@/models';
import { useRequest } from '@/utils/request';
import { AlignLeftOutlined, DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Radio, Space, Tag } from 'antd';
import { useRef, useState } from 'react';
import { RealFileStrutureModel } from '../data';
import { getList as getFileTypeList } from '../FileTypeInfo/api';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';
import SubSystemStatusListModal from './modals/SubSystemStatusListModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const subSystemStatusListModal = useModal(SubSystemStatusListModal, {});
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{
    name?: string;
    isStatus?: EnableStatusEnumValueType;
    dataTypeId?: string;
    fileTypeId?: string;
  }>({ isStatus: undefined });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        params={searchInfo}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          // <BaseSelect
          //   request={getDataTypeList}
          //   fieldNames={{ label: 'name', value: 'id' }}
          //   defaultValue={searchInfo.dataTypeId}
          //   style={{ width: 160 }}
          //   placeholder='请选择数据类型'
          //   onChange={(dataTypeId) => {
          //     setSearchInfo({ ...searchInfo, dataTypeId });
          //   }}
          // />,
          <BaseSelect
            request={getFileTypeList}
            fieldNames={{ label: 'name', value: 'id' }}
            style={{ width: 160 }}
            placeholder='请选择文件类型'
            allowClear
            defaultValue={searchInfo.fileTypeId}
            onChange={(fileTypeId) => {
              setSearchInfo({ ...searchInfo, fileTypeId });
            }}
          />,
          <>
            状态位
            <Radio.Group
              defaultValue={searchInfo.isStatus}
              optionType='button'
              options={[{ label: '全部', value: undefined as any }, ...TrueFalseEnum.list]}
              onChange={(e) => {
                setSearchInfo({
                  ...searchInfo,
                  isStatus: e.target.value
                });
              }}
            />
          </>,
          <BaseSearch
            defaultValue={searchInfo.name}
            placeholder='请输入文件结构中/英文名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        request={getList}
        columns={
          [
            {
              dataIndex: 'cname',
              title: '中文名称'
            },
            {
              dataIndex: 'ename',
              title: '英文名称'
            },
            {
              dataIndex: 'dataSerial',
              title: '数据序号'
            },

            {
              title: '所属结构',
              render: (text, record) => {
                const { systemName, extensionName, keyPartsModelName } = record;
                return [systemName, extensionName, keyPartsModelName].filter((i) => i).join('-');
              }
            },
            {
              dataIndex: 'dataTypeName',
              title: '数据类型'
            },
            {
              dataIndex: 'fileTypeName',
              title: '文件类型'
            },
            {
              dataIndex: 'min',
              title: '最小值'
            },
            {
              dataIndex: 'max',
              title: '最大值'
            },
            {
              dataIndex: 'scale',
              title: '倍数'
            },
            {
              dataIndex: 'unit',
              title: '单位'
            },
            {
              dataIndex: 'isLargeScreen',
              title: '大屏展示',
              render: (text, record) => {
                const t = TrueFalseEnum.getLabelByValue(record.isLargeScreen);
                const color = record.isLargeScreen ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: 'displayLocationName',
              title: '显示位置',
              render: (text, record) => {
                const { displayLocationPName, displayLocationName } = record;
                return displayLocationName
                  ? `${displayLocationPName}-${displayLocationName}`
                  : text;
              }
            },
            {
              dataIndex: 'importantPara',
              title: '重要参数',
              render: (text, record) => {
                const t = TrueFalseEnum.getLabelByValue(record.importantPara);
                const color = record.importantPara ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: 'isComplete',
              title: '是否整机',
              render: (text, record) => {
                const t = TrueFalseEnum.getLabelByValue(record.isComplete);
                const color = record.isComplete ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: 'isStatus',
              title: '状态字段',
              render: (text, record) => {
                const t = TrueFalseEnum.getLabelByValue(record.isStatus);
                const color = record.isStatus ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='blue'
                      icon={<AlignLeftOutlined />}
                      title='详情'
                      onClick={() => {
                        infoModal.load({ data: record, readonly: true });
                      }}
                    />
                    {record.isStatus ? (
                      <BaseAction
                        color='blue'
                        icon={<AlignLeftOutlined />}
                        title='子系统状态数据'
                        onClick={() => {
                          subSystemStatusListModal.load({
                            id: `${record.dataSerial}`,
                            systemId: record.systemId
                          });
                        }}
                      />
                    ) : null}
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<RealFileStrutureModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
