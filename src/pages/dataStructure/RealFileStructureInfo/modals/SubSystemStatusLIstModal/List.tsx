import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { FC, useRef } from 'react';
import { SubSystemStatusModel } from '../../../data';
import { delSystemStatusInfo, getSystemStatusList } from '../../api';
import InfoModal from './InfoModal';

const StatuList: FC<{ systemId?: string; id: string; height: number }> = ({
  systemId,
  id,
  height
}) => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delSystemStatusInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  return (
    <BaseTable
      rowKey='id'
      actionRef={tableRef}
      cardProps={{ bodyStyle: { padding: 0 } }}
      params={{ realTimeFileId: id }}
      height={height}
      request={getSystemStatusList}
      headerTitle='子系统数据列表'
      toolBarRender={() => [
        <BaseButton
          type='primary'
          code='statusCreate'
          icon={<PlusOutlined />}
          onClick={() => {
            infoModal.load({
              statusSource: id,
              systemId,
              data: {} as SubSystemStatusModel
            });
          }}
        >
          新增
        </BaseButton>
      ]}
      columns={
        [
          {
            dataIndex: 'cname',
            title: '中文名称'
          },
          {
            dataIndex: 'ename',
            title: '英文名称'
          },
          {
            title: '所属结构',
            renderText: (text, record) => {
              const { raExtensionName, keyPartModelName } = record;
              const names = [raExtensionName, keyPartModelName].filter((i) => i);
              return names.length > 0 ? names.join('-') : undefined;
            },
            width: 100
          },
          {
            dataIndex: 'statusOrder',
            title: '状态位序号',
            width: 100
          },
          {
            dataIndex: 'bitNumber',
            title: 'bit数',
            width: 60
          },
          // {
          //   dataIndex: 'statusSource',
          //   title: '状态字来源',
          //   width: 100
          // },
          {
            dataIndex: 'zeroStatusMean',
            title: '0状态含义',
            width: 100
          },
          {
            title: '操作',
            fixed: 'right',
            render: (text, record) => {
              return (
                <Space>
                  <BaseAction
                    color='green'
                    icon={<EditOutlined />}
                    title='编辑'
                    code='statusEdit'
                    onClick={() => {
                      infoModal.load({ statusSource: id, systemId, data: record });
                    }}
                  />
                  <BaseAction
                    color='red'
                    icon={<DeleteOutlined />}
                    title='删除'
                    code='statusDelete'
                    loading={del.fetches[record.id]?.loading}
                    onClick={() => {
                      del.run(record.id);
                    }}
                  />
                </Space>
              );
            }
          }
        ] as ProColumns<SubSystemStatusModel>[]
      }
    />
  );
};

export default StatuList;
