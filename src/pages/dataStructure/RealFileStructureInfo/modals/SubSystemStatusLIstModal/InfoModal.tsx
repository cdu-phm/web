import {
  BaseFormDight,
  BaseFormSelect,
  BaseFormText,
  BaseModalForm,
  ProFormDependency
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { getList as getKeyPartsModelList } from '@/pages/radar/keyParts/KeyPartsModelInfo/api';
import { getList as getExtensionList } from '@/pages/radar/SubExtensionInfo/api';
import { isEmpty } from '@/utils/is';
import { Alert, Col, Row } from 'antd';
import { SubSystemStatusModel } from '../../../data';
import { addSystemStatusInfo, editSystemStatusInfo } from '../../api';

const InfoModal: ModalFC<{
  statusSource: string;
  systemId?: string;
  data?: SubSystemStatusModel;
}> = (props) => {
  const {
    statusSource,
    systemId,
    data = {} as SubSystemStatusModel,
    onSuccess,
    ...anyProps
  } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<SubSystemStatusModel>
      title='子系统状态数据信息'
      labelCol={{ style: { width: 100 } }}
      width={640}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        values.statusSource = statusSource;
        const { ok } = await (isadd ? addSystemStatusInfo : editSystemStatusInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <Row>
        <Col span={12}>
          <BaseFormText
            label='中文名称'
            name='cname'
            placeholder='请输入中文名称'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入中文名称' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
        </Col>
        <Col span={12}>
          <BaseFormText
            label='英文名称'
            name='ename'
            placeholder='请输入英文名称'
            fieldProps={{ maxLength: 50, showCount: true }}
            rules={[
              { required: true, message: '请输入英文名称' },
              { type: 'string', max: 50, message: '长度不可超过50位' },
              {
                pattern: /^[a-zA-Z][0-9a-zA-Z_-]*$/g,
                message: '英文名称只能包括大小写字母、数字、下划线、短横线,只能以字母作为开头'
              }
            ]}
          />
        </Col>
        {systemId && (
          <>
            <Col span={24}>
              <BaseFormSelect
                label='分机'
                name='raExtensionId'
                request={getExtensionList}
                fieldProps={{
                  params: { systemId },
                  defaultSelectedOptions: [{ name: data.raExtensionName, id: data.raExtensionId }],
                  fieldNames: { label: 'name', value: 'id' }
                }}
                placeholder='请选择分机'
              />
            </Col>
            <Col span={24}>
              <ProFormDependency name={['raExtensionId']}>
                {({ raExtensionId }) => (
                  <BaseFormSelect
                    label='关重件型号'
                    name='keyPartModelId'
                    request={getKeyPartsModelList}
                    hiddenItem={isEmpty(raExtensionId)}
                    hiddenRender={() => <Alert message='请先选择分机' type='warning' />}
                    fieldProps={{
                      params: { systemId },
                      defaultSelectedOptions: [
                        { name: data.keyPartModelId, typeName: '', id: data.keyPartModelId }
                      ],
                      fieldNames: { label: (data: any) => data.typeName + data.name, value: 'id' }
                    }}
                    placeholder='请选择关重件型号'
                  />
                )}
              </ProFormDependency>
            </Col>
          </>
        )}
        <Col span={12}>
          <BaseFormDight
            label='状态位序号'
            name='statusOrder'
            fieldProps={{ step: 1, min: 1 }}
            rules={[{ required: true, message: '请输入状态位序号' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormDight
            label='bit数'
            name='bitNumber'
            fieldProps={{ step: 1, min: 0 }}
            rules={[{ required: true, message: '请输入状态bit数目' }]}
          />
        </Col>

        <Col span={24}>
          <BaseFormText
            label='0状态含义'
            name='zeroStatusMean'
            placeholder='请输入0状态含义'
            rules={[{ required: true, message: '请输入0状态含义' }]}
          />
        </Col>
      </Row>
    </BaseModalForm>
  );
};

export default InfoModal;
