import BaseModal, { ModalFC } from '@/components/base/BaseModal';
import StatuList from './List';

const SubSystemStatusListModal: ModalFC<{ systemId?: string; id: string }> = ({
  systemId,
  id,
  ...props
}) => {
  return (
    <BaseModal width={1080} footer={false} title='子系统数据' {...props}>
      <StatuList systemId={systemId} id={id} height={240} />
    </BaseModal>
  );
};

export default SubSystemStatusListModal;
