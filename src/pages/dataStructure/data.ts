import { BaseModel, EnableStatusEnumValueType, TrueFalseEnumValueType } from '@/models';

export interface DataTypeModel extends BaseModel {
  name: string;
  customName: string;
  basicByteNum: number;
  basicTypeNum: number;
}

export interface FileTypeModel extends BaseModel {
  radarTypeId: string;
  radarTypeName: string;
  name: string;
  length: number;
  remarks?: string;
}

export interface RealFileStrutureModel extends BaseModel {
  cname: string;
  ename: string;
  dataTypeId: string;
  dataTypeName: string;
  fileTypeId: string;
  fileTypeName: string;
  displayLocationId: string;
  displayLocationName: string;
  displayLocationPId: string;
  displayLocationPName: string;
  systemId?: string;
  systemName?: string;
  extensionId?: string;
  extensionName?: string;
  keyPartsModelId?: string;
  keyPartsModelName?: string;
  length: number;
  min: string;
  max: string;
  unit: string;
  dataSerial: number;
  isStatus: EnableStatusEnumValueType;
  isComplete: TrueFalseEnumValueType;
  isLargeScreen: TrueFalseEnumValueType;
  importantPara: TrueFalseEnumValueType;
  icon: string;
  scale: number;
  remarks?: string;
}

export interface SubSystemStatusModel extends BaseModel {
  cname: string;
  ename: string;
  raExtensionId: string;
  raExtensionName: string;
  keyPartModelId: string;
  keyPartModelName: string;
  statusOrder: number;
  statusSource: string;
  zeroStatusMean: string;
}

export interface DisplayLocationModel extends BaseModel {
  pid?: string;
  name: string;
  displaySerialNum: number;
  description?: string;
  radarFileType: string;
  radarFileTypeName: string;
}
