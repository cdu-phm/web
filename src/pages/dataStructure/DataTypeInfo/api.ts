import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { DataTypeModel } from '../data';
/**
 * 分页获取数据类型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/fi/v1/fiDataTypes', data);
}

/**
 * 添加数据类型信息
 * @param data
 * @returns
 */
export function addInfo(data: DataTypeModel) {
  return request.post('/pc/fi/v1/fiDataTypes', data, { success: '数据类型信息新增成功' });
}

/**
 * 编辑数据类型信息
 * @param data
 * @returns
 */
export function editInfo(data: DataTypeModel) {
  return request.put('/pc/fi/v1/fiDataTypes', data, { success: '数据类型信息编辑成功' });
}
/**
 * 删除数据类型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/fi/v1/fiDataTypes',
    { id },
    { confirm: { content: '确认删除当前数据类型信息' }, success: '数据类型信息删除成功' }
  );
}
