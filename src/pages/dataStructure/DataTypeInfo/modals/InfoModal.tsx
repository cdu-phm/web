import { BaseFormDight, BaseFormText, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { Col, Row } from 'antd';
import { DataTypeModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: DataTypeModel }> = (props) => {
  const { data = {} as DataTypeModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<DataTypeModel>
      title='数据类型信息'
      labelCol={{ style: { width: 110 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='类型名称'
        name='name'
        placeholder='请输入类型名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入类型名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='自定义类型名称'
        name='customName'
        fieldProps={{ maxLength: 20, showCount: true }}
        placeholder='请输入自定义类型名称'
        rules={[
          { required: true, message: '请输入自定义类型名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <Row>
        <Col span={12}>
          <BaseFormDight
            label='基本类型字节数'
            name='basicByteNum'
            fieldProps={{ step: 1, min: 1 }}
            rules={[{ required: true, message: '请输入基本类型字节数' }]}
          />
        </Col>
        <Col span={12}>
          <BaseFormDight
            label='基本类型数目'
            name='basicTypeNum'
            fieldProps={{ step: 1, min: 1 }}
            rules={[{ required: true, message: '请输入基本类型数目' }]}
          />
        </Col>
      </Row>
    </BaseModalForm>
  );
};

export default InfoModal;
