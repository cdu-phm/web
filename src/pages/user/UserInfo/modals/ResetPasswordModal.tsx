import { BaseFormText, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import useUserStore from '@/store/user';
import { resetPassword, verificationUser } from '../api';

const ResetPasswordModal: ModalFC<{ userId: string }> = (props) => {
  const { userinfo } = useUserStore.getState();
  const { userId, ...anyProps } = props;
  return (
    <BaseModalForm
      title='用户密码重置'
      width={560}
      {...anyProps}
      onFinish={async ({ password }) => {
        const { ok } = await verificationUser(userinfo.username, password);
        if (ok) {
          const res = await resetPassword(userId);
          return res.ok;
        }
        return false;
      }}
    >
      <BaseFormText.Password
        label='登录用户密码'
        name='password'
        placeholder='请输入登录用户密码'
        fieldProps={{
          readOnly: true,
          onFocus: (e) => {
            e.target.removeAttribute('readonly');
          }
        }}
        rules={[
          { required: true, message: '请输入登录用户密码' }
          // () => ({
          //   validator(_, value) {
          //     if (!/^[0-9a-zA-Z~!@#$%^&*]{6,20}$/g.test(value)) {
          //       return Promise.reject(
          //         new Error('密码应为6-20位，只能输入大写字母/小写字母/数字/特殊字符(~!@#$%^&*)')
          //       );
          //     }
          //     return Promise.resolve();
          //   }
          // })
        ]}
      />
    </BaseModalForm>
  );
};

export default ResetPasswordModal;
