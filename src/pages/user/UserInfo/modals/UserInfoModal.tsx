import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormSelect
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { Col, Row } from 'antd';
import { getList } from '../../../radar/ServerAreaInfo/api';
import { UserModel, UserType } from '../../data';
import { addUser, editUser } from '../api';

const UserInfoModal: ModalFC<{ data?: UserModel }> = (props) => {
  const { data = {} as UserModel, onSuccess, ...anyProps } = props;

  const isAdd = isEmpty(data.id);
  return (
    <BaseModalForm<UserModel>
      title='用户信息'
      initialValues={{ ...data, areaId: (data.area || []).map((i) => i.areaId) }}
      labelCol={{ style: { width: 80 } }}
      width={960}
      {...anyProps}
      onFinish={async (values) => {
        const { ok } = await (isAdd ? addUser : editUser)({ ...values, id: data.id });
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <Row>
        <Col span={12}>
          <BaseFormText
            label='用户名'
            name='username'
            placeholder='请输入用户名'
            proFieldProps={{ mode: isAdd ? 'edit' : 'read' }}
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入用户名' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
          <BaseFormText
            label='真实姓名'
            name='realName'
            placeholder='请输入真实姓名'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入真实姓名' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
          <ProFormSelect
            label='用户类型'
            name='userType'
            options={UserType.list}
            rules={[{ required: true, message: '请选择用户类型' }]}
          />
          <BaseFormSelect
            label='服务区域'
            name='areaId'
            placeholder='请选择服务区域'
            rules={[{ required: true, message: '请选择服务区域' }]}
            request={getList}
            multiple
            fieldProps={{
              fieldNames: {
                label: 'name',
                value: 'id'
              },
              defaultSelectedOptions: (data.area || []).map((i) => ({
                name: i.areaName,
                id: i.areaId
              }))
            }}
          />
          <BaseFormText
            label='电话'
            placeholder='请输入用户电话'
            name='mobile'
            rules={[
              { required: true, message: '请输入用户电话' },
              { pattern: /^(?:(?:\+|00)86)?1[3-9]\d{9}$/, message: '请输入正确格式的电话信息' }
            ]}
          />
        </Col>
        <Col span={12}>
          <BaseFormText
            label='邮箱'
            placeholder='请输入用户邮箱'
            name='mailbox'
            fieldProps={{ maxLength: 50 }}
            rules={[
              { required: true, message: '请输入用户邮箱' },
              {
                pattern:
                  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                message: '请输入正确格式的邮箱信息'
              }
            ]}
          />

          <BaseFormText
            label='证件'
            name='certificatesId'
            placeholder='请输入用户身份证号'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              {
                pattern:
                  /^[1-9]\d{5}(?:18|19|20)\d{2}(?:0[1-9]|10|11|12)(?:0[1-9]|[1-2]\d|30|31)\d{3}[\dXx]$/,
                message: '请输入正确格式的身份证信息'
              }
            ]}
          />
          <BaseFormText
            label='职称'
            name='jobName'
            placeholder='请输入用户职称'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[{ type: 'string', max: 20, message: '长度不可超过20位' }]}
          />
          <BaseFormTextArea
            fieldProps={{ maxLength: 240, showCount: true }}
            label='备注'
            name='remarks'
          />
        </Col>
      </Row>
    </BaseModalForm>
  );
};

export default UserInfoModal;
