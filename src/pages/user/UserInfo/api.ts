import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { RoleModel, UserModel } from '../data';
/**
 * 分页获取用户列表
 * @export
 * @param {PageParams<any>} params
 * @returns
 */
export function getUserList(params: PageParams<{ username?: string }>) {
  return request.list('/pc/user/v1/users', params);
}

/**
 * 新增用户
 * @param data
 * @returns
 */
export function addUser(data: UserModel) {
  return request.post('​/pc​/user​/v1​/users', data, { success: '用户信息新增成功' });
}

/**
 * 修改用户信息
 * @param data
 * @returns
 */
export function editUser(data: UserModel) {
  return request.put('​/pc​/user​/v1​/users', data, { success: '用户信息编辑成功' });
}

/**
 * 根据用户id删除用户信息
 * @param id
 * @returns
 */
export function delUser(id: string) {
  return request.delete(
    '​/pc​/user​/v1​/users',
    { id },
    { confirm: { content: '确定删除当前用户信息' }, success: '用户信息删除成功' }
  );
}
/**
 * 批量删除用户信息
 * @param id
 * @returns
 */
export function delUsers(userIds: string[]) {
  return request.post(
    '​/pc/user/v1/deleteUsers',
    { userIds },
    { confirm: { content: '确认批量删除当前选中的用户信息' }, success: '用户信息批量删除成功' }
  );
}
/**
 * 验证用户信息
 * @param id
 * @returns
 */
export function verificationUser(username: string, password: string) {
  return request.post('/pc/user/v1/verificationUser', {}, { params: { username, password } });
}

/**
 * 根据用户id重置用户密码
 * @param id
 * @returns
 */
export function resetPassword(id: string) {
  return request.get(
    '/pc​/user​/v1​/users/reset',
    { id },
    { confirm: { content: '确认重置该用户密码' }, success: '用户密码重置成功' }
  );
}
/**
 * 绑定用户角色
 * @param userId
 * @param roleIds
 * @returns
 */
export function saveUserRoles(userId: string, roleIds: string[]) {
  return request.post(
    '/pc/user/v1/users/roles',
    {
      userId,
      roleIds
    },
    {
      success: '用户角色操作成功'
    }
  );
}
/**
 * 根据用户id获取绑定角色
 * @param userId
 * @returns
 */
export function getUserRoles(userId: string) {
  return request.get<RoleModel[]>('/pc/user/v1/usersRoles', { userId });
}
