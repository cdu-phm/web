import { BaseModel } from '@/models';
import { Enum, EnumValueType } from '@/utils/enum';

export const UserType = Enum({
  学生: '00',
  老师: '01',
  后台: '02'
} as const);

export const PageType = Enum({
  菜单: '00',
  页面: '01'
} as const);

export interface RoleModel extends BaseModel {
  roleName: string;
  remarks: string;
}
export interface UserModel extends BaseModel {
  username: string;
  realName: string;
  // areaId: string;
  // areaName: string;
  area: {
    areaId: string;
    areaName: string;
  }[];
  areaId: string[];
  mobile: string;
  mailbox: string;
  certificatesId: string;
  password: string;
  remarks?: string;
  jobName: string;
  userType: EnumValueType<typeof UserType['enum']>;
}

export interface PageModel extends BaseModel {
  remarks?: string;
  label: string;
  permissionPid: string | null;
  idPath?: string;
  actionUrl: string;
  router: string;
  iconType?: string;
  sort: number;
  type: EnumValueType<typeof PageType['enum']>;
}

export interface FunctionBlockModel extends BaseModel {
  name: string;
  router: string;
  pageId: string;
  description?: string;
}
