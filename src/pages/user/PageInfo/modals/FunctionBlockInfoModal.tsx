import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { FunctionBlockModel } from '../../data';
import { addFunctionBlockInfo, editFunctionBlockInfo } from '../api';

const FunctionBlockInfoModal: ModalFC<{ data?: FunctionBlockModel; pageId: string }> = (props) => {
  const { data = {} as FunctionBlockModel, onSuccess, pageId, ...anyProps } = props;
  const { id } = data;
  const isedit = Boolean(id);
  return (
    <BaseModalForm<FunctionBlockModel>
      initialValues={data}
      width={800}
      title='功能块信息'
      labelCol={{ style: { width: 50 } }}
      {...anyProps}
      onFinish={async (values) => {
        values.pageId = pageId;
        values.id = id;
        const { ok } = await (isedit ? editFunctionBlockInfo : addFunctionBlockInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='name'
        placeholder='请输入功能模块名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入功能模块名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='标识'
        name='router'
        placeholder='请输入功能模块唯一标识'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入功能模块唯一标识' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='description'
      />
    </BaseModalForm>
  );
};

export default FunctionBlockInfoModal;
