import {
  BaseFormDight,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { Col, Row } from 'antd';
import { PageModel, PageType } from '../../data';
import { addPageInfo, editPageInfo } from '../api';

const PageInfoModal: ModalFC<{ data?: any; parentID?: string }> = (props) => {
  const {
    onSuccess,
    data = { sort: 0, type: PageType.enum.菜单 },
    parentID = null,
    ...anyProps
  } = props;
  const { id } = data;
  const isedit = Boolean(id);
  return (
    <BaseModalForm<PageModel>
      title='页面信息'
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.permissionPid = parentID;
        values.id = id;
        const { data, ok } = await (isedit ? editPageInfo : addPageInfo)(values);
        ok && onSuccess && onSuccess(data);
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='label'
        placeholder='请输入页面名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入页面名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='地址'
        name='actionUrl'
        placeholder='请输入页面地址'
        fieldProps={{ maxLength: 50, showCount: true }}
        rules={[
          { required: true, message: '请输入页面地址' },
          { type: 'string', max: 50, message: '长度不可超过50位' }
        ]}
      />
      <Row gutter={16}>
        <Col span={8}>
          <BaseFormText
            label='标识'
            name='router'
            placeholder='请输入页面标识'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入页面标识' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
        </Col>
        <Col span={8}>
          <BaseFormDight label='排序' name='sort' />
        </Col>
        <Col span={8}>
          <ProFormRadio.Group label='类型' name='type' options={PageType.list} />
        </Col>
      </Row>

      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default PageInfoModal;
