import BaseModal, { ModalFC } from '@/components/base/BaseModal';

const FunctionBlockListModal: ModalFC = (props) => {
  return <BaseModal width={900} title='功能模块列表' {...props} />;
};

export default FunctionBlockListModal;
