import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { FunctionBlockModel, PageModel } from '../data';
/**
 *
 *
 * @export
 * @param {PageParams<any>} params
 * @returns
 */
export function getPageInfoList(params: PageParams<{ id: string }>) {
  return request.list('/pc/permission/v1/permissions', params);
}

/**
 *
 * @param data
 * @returns
 */
export function addPageInfo(data: PageModel) {
  return request.post<PageModel>('/pc/permission/v1/permissions', data, {
    success: '页面信息新增成功'
  });
}

/**
 *
 * @param data
 * @returns
 */
export function editPageInfo(data: PageModel) {
  return request.put('/pc/permission/v1/permissions', data, { success: '页面信息编辑成功' });
}

/**
 *
 * @param id
 * @returns
 */
export function deletePageInfo(id: string) {
  return request.delete(
    '/pc/permission/v1/permissions',
    { id },
    { confirm: { content: '确定删除当前页面信息' } }
  );
}

interface PageInfoTreeItem extends PageModel {
  permissionTreeVOList?: PageInfoTreeItem[];
}

/**
 *
 *
 * @export
 * @param {PageParams<any>} params
 * @returns
 */
export function getPageTree() {
  return request.get<PageInfoTreeItem[]>('/pc/permission/v1/treePermission');
}

/**
 *
 *
 * @export
 * @param {PageParams<any>} params
 * @returns
 */
export function getFunctionBlockInfoList(params: PageParams<{ id: string }>) {
  return request.list('/pc/operation/v1/operation', params);
}

/**
 *
 * @param data
 * @returns
 */
export function addFunctionBlockInfo(data: FunctionBlockModel) {
  return request.post('/pc/operation/v1/operation', data, { success: '功能模块信息新增成功' });
}

/**
 *
 * @param data
 * @returns
 */
export function editFunctionBlockInfo(data: FunctionBlockModel) {
  return request.put('/pc/operation/v1/operation', data, { success: '功能模块信息编辑成功' });
}

/**
 *
 * @param id
 * @returns
 */
export function deleteFunctionBlockInfo(id: string) {
  return request.delete(
    '/pc/operation/v1/operation',
    { id },
    { confirm: { content: '确定删除当前功能模块信息' } }
  );
}
