import { PageParams, PageTypeEnum } from '@/models';
import { request } from '@/utils/request';
import { RoleModel } from '../data';
/**
 * 分页查询角色列表
 * @export
 * @param {PageParams<any>} params
 * @returns
 */
export function getRoleInfoList(params: PageParams<{ roleName?: string }>) {
  return request.list('/pc/role/v1/roles', { type: PageTypeEnum.enum.page, ...params });
}
/**
 *
 * @returns
 */
export function getRoleInfoAllList() {
  return request.get<RoleModel[]>('/pc/role/v1/roles', { type: PageTypeEnum.enum.all });
}
/**
 * 新增角色信息
 * @param data
 * @returns
 */
export function addRoleInfo(data: RoleModel) {
  return request.post('/pc/role/v1/roles', data, { success: '角色信息新增成功' });
}

/**
 * 修改角色信息
 * @param data
 * @returns
 */
export function editRoleInfo(data: RoleModel) {
  return request.put('/pc/role/v1/roles', data, { success: '角色信息编辑成功' });
}

/**
 * 删除角色
 * @param id
 * @returns
 */
export function delRoleInfo(id: string) {
  return request.delete(
    '/pc/role/v1/roles',
    { id },
    { confirm: { content: '确定删除当前角色信息' }, success: '角色信息删除成功' }
  );
}

/**
 * 获取所有权限
 * @returns
 */
export function getTreeRolePermissions() {
  return request.get<any[]>('/pc/role/v1/treeRolePermission');
}

/**
 * 获取角色绑定权限
 * @returns
 */
export function getRolesBindPermissions(roleIds: string[]) {
  return request.get<any[]>('/pc/role/v1/treeRolesPermission', { roleIds });
}
/**
 * 获取角色绑定权限
 * @param id
 * @returns
 */
export function getRolePermissions(roleId: string) {
  return request.get<{ operationIds: string[]; permissionIds: string[] }>(
    '/pc/role/v1/roles/permissions',
    { roleId }
  );
}
/**
 * 分配角色权限
 * @param data
 * @returns
 */
export function saveRolePermissions(data: {
  operationIds: string[];
  permissionIds: string[];
  roleId: string;
}) {
  return request.post('/pc/role/v1/roles/permissions', data, { success: '角色权限编辑成功' });
}
