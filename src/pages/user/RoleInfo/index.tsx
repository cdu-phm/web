import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { RoleModel as IRoleInfo } from '../data';
import { delRoleInfo, getRoleInfoList } from './api';
import BindPermissionModal from './modals/BindPermissionModal';
import RoleInfoInfoModal from './modals/RoleInfoModal';

const RoleInfo = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(RoleInfoInfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const bindPressionModal = useModal(BindPermissionModal);
  const del = useRequest(delRoleInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ roleName?: string }>({});
  return (
    <BasePageFrame>
      <BaseTable
        actionRef={tableRef}
        rowKey='id'
        params={searchInfo}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        request={getRoleInfoList}
        toolBarRender={() => [
          <BaseSearch
            defaultValue={searchInfo.roleName}
            placeholder='请输入角色名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, roleName: v });
            }}
          />
        ]}
        columns={
          [
            { dataIndex: 'roleName', title: '名称' },
            { dataIndex: 'remarks', title: '备注' },
            {
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      title='编辑'
                      code='edit'
                      icon={<EditOutlined />}
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='blue'
                      code='pageBind'
                      title='权限绑定'
                      icon={<SettingOutlined />}
                      onClick={() => {
                        bindPressionModal.load({ roleId: record.id });
                      }}
                    />
                    <BaseAction
                      color='red'
                      title='删除'
                      code='delete'
                      icon={<DeleteOutlined />}
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<IRoleInfo>[]
        }
      />
    </BasePageFrame>
  );
};

export default RoleInfo;
