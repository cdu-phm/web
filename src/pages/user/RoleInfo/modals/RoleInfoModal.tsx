import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { RoleModel } from '../../data';
import { addRoleInfo, editRoleInfo } from '../api';

const RoleInfoInfoModal: ModalFC<{ data?: RoleModel }> = (props) => {
  const { data = {} as RoleModel, onSuccess, ...anyProps } = props;
  const isAdd = isEmpty(data.id);
  return (
    <BaseModalForm<RoleModel>
      title='角色信息'
      initialValues={data}
      labelCol={{ style: { width: 80 } }}
      width={560}
      {...anyProps}
      onFinish={async (values) => {
        const { roleName, remarks } = values;
        const { ok } = await (isAdd ? addRoleInfo : editRoleInfo)({
          id: data.id,
          roleName,
          remarks
        });
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='名称'
        name='roleName'
        placeholder='请输入角色名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入角色名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='备注'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default RoleInfoInfoModal;
