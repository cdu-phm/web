import BaseTree, { BaseTreeProps } from '@/components/base/BaseTree';
import { CSSProperties, FC } from 'react';

const PermissionTree: FC<{
  treeData: any[];
  checkable?: boolean;
  targetKeys?: string[];
  onCheck?: BaseTreeProps['onCheck'];
  onSelect?: BaseTreeProps['onSelect'];
  style?: CSSProperties;
  height?: number;
}> = (props) => {
  const { treeData, style, height, targetKeys = [], checkable = true, onCheck, onSelect } = props;
  return (
    <BaseTree
      blockNode
      style={style}
      height={height}
      checkable={checkable}
      defaultExpandAll
      bindProps={{ key: 'id', value: 'id', title: 'label' }}
      treeData={treeData}
      checkedKeys={targetKeys}
      selectedKeys={targetKeys}
      onCheck={onCheck}
      onSelect={onSelect}
    />
  );
};

export default PermissionTree;
