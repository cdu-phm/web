import china from '@/assets/map/china.json';
import BaseEchart from '@/components/base/BaseEchart';
import BaseModal from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import useUserStore from '@/store/user';
import { useRequest } from '@/utils/request';
import * as echarts from 'echarts';
import { useState } from 'react';
import { getRadarMap } from './api';

echarts.registerMap('china', china as any);
const radarClick = (radarItem: any) => {
  const { area } = useUserStore.getState().userinfo;
  if (area.some((i) => i.areaId === radarItem.areaId)) {
    if (radarItem.isTable !== 0) {
      if (radarItem.isTableData !== 0) {
        window.open(`/monitor/radar?radarId=${radarItem.id}`, '__blank');
      } else {
        BaseModal.warning({
          title: '雷达接入警告',
          content: `雷达 ${radarItem.name} 暂无数据，无法查看详情`
        });
      }
    } else {
      BaseModal.warning({
        title: '雷达接入警告',
        content: `雷达 ${radarItem.name} 暂未接入，无法查看详情`
      });
    }
  } else {
    BaseModal.warning({
      title: '权限警告',
      content: `您的区域与 ${radarItem.name} 雷达不相同，无法访问`
    });
  }
};
const IndexPage = () => {
  const [data, setData] = useState<any[]>([]);
  const radarMap = useRequest(getRadarMap, {
    onSuccess(data:any) {
      const resData: any[] = [];
      const itudes: string[] = [];
      var res = data.raRadarMapVOS;
      res.forEach((i) => {
        let { longitude, latitude } = i;
        const str = `${longitude}${latitude}`;
        if (itudes.includes(str)) {
          longitude *= 1 + Math.random() * 0.000005;
          latitude *= 1 + Math.random() * 0.000005;
        } else {
          itudes.push(str);
        }
        resData.push([longitude, latitude, i.areaName, i.stationName, i.name, i.id]);
      });
      setData(resData);
    }
  });
  return (
    <BasePageFrame>
      <BaseEchart
        style={{ height: '100%', width: '100%' }}
        option={{
          backgroundColor: '#9bcffa',
          geo: {
            map: 'china',
            roam: true,
            zoom: 1.7,
            label: {
              show: true,
              color: '#000'
            },
            center: [108.502262, 35.209484],
            itemStyle: {
              borderColor: '#c8c1b4',
              shadowColor: '#000',
              areaColor: '#fff',
              shadowBlur: 1
            },
            emphasis: {
              label: {
                color: '#000'
              },
              itemStyle: {
                color: '#000',
                shadowColor: '#000',
                areaColor: '#eeeeee'
              }
            }
          },
          legend: {
            show: false
          },
          tooltip: {
            show: true,
            formatter: (value: any) => {
              const [, , areaName = '', stationName = '', radarName = ''] = value.data;
              return `${areaName}${stationName} ${radarName}`;
            }
          },
          series: [
            {
              name: '雷达',
              type: 'scatter',
              coordinateSystem: 'geo',
              data,
              symbolSize: 30,
              symbol: `image://${window.origin}/images/marker.png`,
              datasetIndex: 1
            }
          ]
        }}
        onEvents={{
          click: (event: any) => {
            if (event.componentType === 'series') {
              const value = [...event.value].pop();
              const radar = radarMap.data?.find((i) => i.id === value);
              radar && radarClick(radar);
            }
          }
        }}
      />
    </BasePageFrame>
  );
};

export default IndexPage;

// const IndexPage = () => {
//   useEffect(() => {
//     const map = new BMap.Map('container');

//     // 创建地图实例
//     const point = new BMap.Point(116.404, 39.915);
//     // 创建点坐标
//     map.centerAndZoom(point, 5);
//     // 初始化地图，设置中心点坐标和地图级别
//     map.enableScrollWheelZoom(true); // 开启鼠标滚轮缩放
//     const top_left_control = new BMap.ScaleControl({ anchor: BMAP_ANCHOR_TOP_LEFT }); // 左上角，添加比例尺
//     const top_left_navigation = new BMap.NavigationControl(); // 左上角，添加默认缩放平移控件
//     map.addControl(top_left_control);
//     map.addControl(top_left_navigation);
//     const mapType1 = new BMap.MapTypeControl({
//       mapTypes: [BMAP_NORMAL_MAP, BMAP_SATELLITE_MAP, BMAP_HYBRID_MAP]
//     });
//     const overView = new BMap.OverviewMapControl();

//     const overViewOpen = new BMap.OverviewMapControl({
//       isOpen: true,
//       anchor: BMAP_ANCHOR_BOTTOM_RIGHT
//     });
//     map.addControl(mapType1); // 2D图，卫星图
//     // 左上角，默认地图控件

//     map.setCurrentCity('北京'); // 由于有3D图，需要设置城市哦

//     map.addControl(overView); // 添加默认缩略地图控件

//     map.addControl(overViewOpen); // 右下角，打开
//     const markers: any[] = [];

//     // 初始化点聚合

//     const markerClusterer = new BMapLib.MarkerClusterer(map, { markers });
//     getRadarMap().then((result) => {
//       if (result.ok) {
//         const itudes: string[] = [];
//         markerClusterer.clearMarkers();

//         result.data.forEach((i) => {
//           let { longitude, latitude } = i;
//           const str = `${longitude}${latitude}`;
//           if (itudes.includes(str)) {
//             longitude *= 1 + Math.random() * 0.000005;
//             latitude *= 1 + Math.random() * 0.000005;
//           } else {
//             itudes.push(str);
//           }
//           const marker = new BMap.Marker(new BMap.Point(longitude, latitude), {
//             title: `${i.areaName}${i.stationName} ${i.name}`,
//             icon: new BMap.Icon(
//               `${window.origin}/images/marker.png`,
//               { width: 30, height: 30 },
//               { imageSize: { width: 30, height: 30 } }
//             )
//           });
//           markers.push(marker);
//           const markerPoint = new BMap.Point(longitude, latitude);
//           const opts = {
//             width: 200,
//             height: 180
//           };

//           const infoWindow = new BMap.InfoWindow(
//             `<div>
//               <div><span style="width: 70px;display:inline-block">区域:</span>${i.areaName}</div>
//               <div><span style="width: 70px;display:inline-block">站名:</span>${i.stationName}</div>
//               <div><span style="width: 70px;display:inline-block">雷达名称:</span>${i.name}</div>
//               <div><span style="width: 70px;display:inline-block">经度:</span>${i.latitude}</div>
//               <div><span style="width: 70px;display:inline-block">纬度:</span>${i.longitude}</div>
//               <div><span style="width: 70px;display:inline-block">安装位置:</span>${
//                 i.installationPosition ?? '无'
//               }</div>
//               <div><span style="width: 70px;display:inline-block">架设时间:</span>${
//                 i.installTime ? moment(i.installTime).format('YYYY-MM-DD') : '无'
//               }</div>
//               <div style="text-align:right">
//                 <button class="ant-btn ant-btn-link" id="${i.id}"  data-item="${i.id}">详情</button>
//               </div>
//             </div>
//             `,
//             opts
//           );
//           infoWindow.addEventListener('open', () => {
//             const item = document.getElementById(i.id);
//             if (item) {
//               item.onclick = () => {
//                 const { areaId } = useUserStore.getState().userinfo;
//                 if (areaId === i.areaId) {
//                   if (i.isTable) {
//                     window.open(`/monitor/radar?radarId=${i.id}`, '_blank');
//                   } else {
//                     BaseModal.warning({
//                       title: '雷达接入警告',
//                       content: `雷达 ${i.name} 暂未接入，无法查看详情`
//                     });
//                   }
//                 } else {
//                   BaseModal.warning({
//                     title: '权限警告',
//                     content: `您的区域与 ${i.name} 雷达不相同，无法访问`
//                   });
//                 }
//               };
//             }
//           });
//           // 点标记添加点击事件
//           marker.addEventListener('click', () => {
//             map.openInfoWindow(infoWindow, markerPoint); // 开启信息窗口
//           });
//         });
//         markerClusterer.addMarkers(markers);
//       }
//     });
//   }, []);
//   return (
//     <BasePageFrame>
//       <div id='container' style={{ height: '100%', width: '100%' }} />
//     </BasePageFrame>
//   );
// };

// export default IndexPage;
