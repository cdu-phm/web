import { request } from '@/utils/request';

export interface RadarMapStatus {
  RaRadarMapVOS: any[];
}

/**
 * 获取雷达在地图上显示的基本信息
 * @returns
 */
export function getRadarMap() {
  return request.get<any[]>('/pc/radar/v1/raStationMap');
}

/**
 * 获取雷达在地图上显示的状态信息
 * @returns
 */
export function getRadarMapStatus() {
  return request.get<RadarMapStatus>('/pc/radar/v1/raStationMapStatus');
}
