import { BaseFormText, BaseFormTextArea, BaseModalForm } from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { ExpertKnowledgeLibraryModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: ExpertKnowledgeLibraryModel }> = (props) => {
  const { data = {} as ExpertKnowledgeLibraryModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<ExpertKnowledgeLibraryModel>
      title='专家知识库信息'
      labelCol={{ style: { width: 95 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='知识库名称'
        name='name'
        placeholder='请输入知识库名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入知识库名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='key'
        name='key'
        placeholder='请输入知识库 key'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入知识库 key' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='value'
        name='value'
        placeholder='请输入知识库 value'
        fieldProps={{ maxLength: 50, showCount: true }}
        rules={[
          { required: true, message: '请输入知识库 value' },
          { type: 'string', max: 50, message: '长度不可超过50位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        placeholder='请输入知识库描述'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
