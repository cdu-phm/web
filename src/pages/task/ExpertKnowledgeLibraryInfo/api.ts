import { PageParams, PageTypeEnum } from '@/models';
import { request } from '@/utils/request';
import { ExpertKnowledgeLibraryModel } from '../data';
/**
 * 分页获取专家知识库信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/taExpertKnowledgeLibrarys', data);
}

/**
 * 获取专家知识信息
 * @returns
 */
export function getAllList() {
  return request.get<ExpertKnowledgeLibraryModel[]>('/pc/radar/v1/taExpertKnowledgeLibrarys', {
    type: PageTypeEnum.enum.all
  });
}

/**
 * 添加专家知识库信息
 * @param data
 * @returns
 */
export function addInfo(data: ExpertKnowledgeLibraryModel) {
  return request.post('/pc/radar/v1/taExpertKnowledgeLibrarys', data, {
    success: '专家知识库信息新增成功'
  });
}

/**
 * 编辑专家知识库信息
 * @param data
 * @returns
 */
export function editInfo(data: ExpertKnowledgeLibraryModel) {
  return request.put('/pc/radar/v1/taExpertKnowledgeLibrarys', data, {
    success: '专家知识库信息编辑成功'
  });
}
/**
 * 删除专家知识库信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taExpertKnowledgeLibrarys',
    { id },
    { confirm: { content: '确认删除当前专家知识库信息' }, success: '专家知识库信息删除成功' }
  );
}
