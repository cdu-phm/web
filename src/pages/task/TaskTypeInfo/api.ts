import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TaskTypeModel } from '../data';
/**
 * 分页获取任务类型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/taTaskTypes', data);
}

/**
 * 添加任务类型信息
 * @param data
 * @returns
 */
export function addInfo(data: TaskTypeModel) {
  return request.post('/pc/radar/v1/taTaskTypes', data, { success: '任务类型信息新增成功' });
}

/**
 * 编辑任务类型信息
 * @param data
 * @returns
 */
export function editInfo(data: TaskTypeModel) {
  return request.put('/pc/radar/v1/taTaskTypes', data, { success: '任务类型信息编辑成功' });
}
/**
 * 删除任务类型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taTaskTypes',
    { id },
    { confirm: { content: '确认删除当前任务类型信息' }, success: '任务类型信息删除成功' }
  );
}
