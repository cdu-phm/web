import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TaskModel } from '../data';
/**
 * 分页获取任务信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ taskTypeId?: string; status?: number }>) {
  return request.list('/pc/radar/v1/taTasks', data);
}

/**
 * 添加任务信息
 * @param data
 * @returns
 */
export function addInfo(data: TaskModel) {
  return request.post('/pc/radar/v1/taTasks', data, { success: '任务信息新增成功' });
}

/**
 * 编辑任务信息
 * @param data
 * @returns
 */
export function editInfo(data: TaskModel) {
  return request.put('/pc/radar/v1/taTasks', data, { success: '任务信息编辑成功' });
}
/**
 * 删除任务信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taTasks',
    { id },
    { confirm: { content: '确认删除当前任务信息' }, success: '任务信息删除成功' }
  );
}
