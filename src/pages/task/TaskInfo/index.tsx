import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { EnableStatusEnum } from '@/models';
import { useRequest } from '@/utils/request';
import { formateDate } from '@/utils/time';
import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Radio, Space, Tag } from 'antd';
import { useRef, useState } from 'react';
import { TaskModel } from '../data';
import { getList as getTypeList } from '../TaskTypeInfo/api';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    fetchKey: (id) => id,
    manual: true,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ taskTypeId: string | null; status?: number }>({
    taskTypeId: null,
    status: EnableStatusEnum.enum.enable
  });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        params={searchInfo}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSelect
            value={searchInfo.taskTypeId}
            request={getTypeList}
            placeholder='请选择任务类型'
            allowClear
            style={{ width: 200 }}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v = null) => {
              setSearchInfo({ ...searchInfo, taskTypeId: v });
            }}
          />,
          <Radio.Group
            optionType='button'
            defaultValue={searchInfo.status}
            options={[{ label: '全部', value: '' }, ...EnableStatusEnum.list]}
            onChange={(e) => {
              setSearchInfo({ ...searchInfo, status: e.target.value });
            }}
          />
        ]}
        request={getList}
        columns={
          [
            {
              dataIndex: 'treeName',
              title: '任务设备'
            },
            {
              dataIndex: 'remarks',
              title: '任务描述'
            },
            {
              dataIndex: 'typeName',
              title: '任务类型'
            },
            {
              dataIndex: 'algorithmModelCName',
              title: '算法模型'
            },
            {
              dataIndex: 'cycle',
              title: '任务周期',
              renderText: (text) => {
                const date = formateDate(text);
                return `${date.value} ${date.unit}`;
              }
            },
            {
              dataIndex: 'startTime',
              title: '开始时间',
              valueType: 'dateTime'
            },
            {
              dataIndex: 'endTime',
              title: '截止时间',
              valueType: 'dateTime'
            },
            {
              dataIndex: 'status',
              title: '状态',
              render(text, record) {
                const t = EnableStatusEnum.getLabelByValue(record.status);
                const color = record.status ? 'success' : 'default';
                return <Tag color={color}>{t}</Tag>;
              }
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<TaskModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
