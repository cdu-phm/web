import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { AlgorithmModelLibraryModel } from '../data';
/**
 * 分页获取算法模型信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string; taskTypeId?: string }>) {
  return request.list('/pc/radar/v1/taAlgorithmModelLibrarys', data);
}

/**
 * 添加算法模型信息
 * @param data
 * @returns
 */
export function addInfo(data: AlgorithmModelLibraryModel) {
  return request.post('/pc/radar/v1/taAlgorithmModelLibrarys', data, {
    success: '算法模型信息新增成功'
  });
}

/**
 * 编辑算法模型信息
 * @param data
 * @returns
 */
export function editInfo(data: AlgorithmModelLibraryModel) {
  return request.put('/pc/radar/v1/taAlgorithmModelLibrarys', data, {
    success: '算法模型信息编辑成功'
  });
}
/**
 * 删除算法模型信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taAlgorithmModelLibrarys',
    { id },
    { confirm: { content: '确认删除当前算法模型信息' }, success: '算法模型信息删除成功' }
  );
}

/**
 * 获取模型已绑定的数据结构信息
 * @returns
 */
export function getSelectRealTimeFileStructuresList(modelId: string) {
  return request.get<string[]>('/pc/fi/v1/selectRealTimeFileStructures', { modelId });
}

/**
 * 添加算法模型 数据结构信息
 * @param data
 * @returns
 */
export function bindSelectRealTimeFileStructures(modelId: string, realFileIds: string[]) {
  return request.post(
    '/pc/fi/v1/fiTaRelations',
    {
      modelId,
      realFileIds
    },
    {
      confirm: { content: '请确认将当前的实时文件结构绑定到该算法模型上' },
      success: '算法模型-实时文件结构绑定成功'
    }
  );
}
