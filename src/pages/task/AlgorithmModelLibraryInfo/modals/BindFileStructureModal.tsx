import BaseModal, { ModalFC } from '@/components/base/BaseModal';
import BaseTableTransfer from '@/components/base/BaseTransfer/BaseTableTransfer';
import { EnableStatusEnum, TrueFalseEnum } from '@/models';
import { RealFileStrutureModel } from '@/pages/dataStructure/data';
import { getAllList } from '@/pages/dataStructure/RealFileStructureInfo/api';
import { useRequest } from '@/utils/request';
import { Tag } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { useState } from 'react';
import { bindSelectRealTimeFileStructures, getSelectRealTimeFileStructuresList } from '../api';

const BindFileStructureModal: ModalFC<{ systemId: string; id: string }> = ({
  id,
  systemId,
  ...props
}) => {
  const columns: ColumnsType<RealFileStrutureModel> = [
    {
      dataIndex: 'cname',
      title: '中文名称'
    },
    {
      dataIndex: 'ename',
      title: '英文名称'
    },
    {
      dataIndex: 'isComplete',
      title: '是否整机',
      width: 75,
      render: (text, record) => {
        const t = TrueFalseEnum.getLabelByValue(record.isComplete);
        const color = record.isStatus ? 'success' : 'default';
        return <Tag color={color}>{t}</Tag>;
      }
    },
    {
      title: '所属结构',
      render: (text, record) => {
        const { systemName, extensionName, keyPartsModelName } = record;
        return [systemName, extensionName, keyPartsModelName].filter((i) => i).join('-');
      }
    }
  ];
  const fileList = useRequest(getAllList, {
    initialData: [],
    defaultParams: [{ systemId, isStatus: EnableStatusEnum.enum.disable }]
  });
  const [targetKeys, setTargetKeys] = useState<string[]>([]);
  useRequest(getSelectRealTimeFileStructuresList, {
    defaultParams: [id],
    onSuccess(data) {
      setTargetKeys(data);
    }
  });
  const save = useRequest(bindSelectRealTimeFileStructures, {
    manual: true,
    onSuccess() {
      props.onCancel();
    }
  });
  return (
    <BaseModal
      title='算法模型-文件结构数据绑定'
      {...props}
      onOk={() => {
        save.run(id, targetKeys);
      }}
      width={1200}
      okButtonProps={{ disabled: fileList.loading, loading: save.loading }}
    >
      <BaseTableTransfer
        titles={['未绑定文件结构', '已绑定文件结构']}
        dataSource={fileList.data}
        targetKeys={targetKeys}
        onChange={(t) => {
          setTargetKeys(t);
        }}
        rowKey={(record) => record.id}
        bodyHeight={300}
        leftColumns={columns}
        rightColumns={columns}
      />
    </BaseModal>
  );
};

export default BindFileStructureModal;
