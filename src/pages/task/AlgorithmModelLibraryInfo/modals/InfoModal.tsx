import BaseForm, {
  BaseFormDight,
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseFormUpload,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import BaseTree from '@/components/base/BaseTree';
import { getRadarTreeNoDoList } from '@/pages/radar/RadarInfo/api';
import { isEmpty } from '@/utils/is';
import { useRequest } from '@/utils/request';
import { ProFormInstance } from '@ant-design/pro-form';
import { Col, Row, Spin } from 'antd';
import { useRef, useState } from 'react';
import { getList } from '../../AlgorithmLibraryInfo/api';
import { AlgorithmModelLibraryModel } from '../../data';
import { getList as getTaskTypeList } from '../../TaskTypeInfo/api';
import { addInfo, editInfo } from '../api';

const flatten = (list: Record<string, any>[] = [], parentIds: string[]) => {
  list.forEach((item: any) => {
    item.allId = parentIds.length > 0 ? `${parentIds.join('-')}-${item.id}` : item.id;
    if (item.children) {
      flatten(item.children, [...parentIds, item.id]);
    }
  });
  return list;
};
const InfoModal: ModalFC<{ data?: AlgorithmModelLibraryModel }> = (props) => {
  const { data = {} as AlgorithmModelLibraryModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  const formRef = useRef<ProFormInstance>();
  const [key, setKey] = useState<string | undefined>(() => data.algorithmModelObject);
  const radarTree = useRequest(getRadarTreeNoDoList);
  const setTreeId = (id = '') => {
    setKey(id);
    const value = formRef.current?.getFieldsValue(true);
    formRef.current?.setFieldsValue({ ...value, algorithmModelObject: id });
  };
  return (
    <BaseModalForm<AlgorithmModelLibraryModel>
      title='算法模型信息'
      formRef={formRef}
      labelCol={{ style: { width: 80 } }}
      width={960}
      initialValues={data}
      {...anyProps}
      onFinish={async (values: AlgorithmModelLibraryModel) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <Row>
        <Col span={12}>
          <BaseFormSelect
            request={getTaskTypeList}
            label='任务类型'
            name='taskTypeId'
            placeholder='请选择任务类型'
            fieldProps={{
              defaultSelectedOptions: [{ name: data.taskTypeName, id: data.taskTypeId }],
              fieldNames: { label: 'name', value: 'id' }
            }}
            rules={[{ required: true, message: '请选择任务类型' }]}
          />
          <BaseForm.Item
            label='雷达结构'
            name='algorithmModelObject'
            rules={[{ required: true, message: '请选择雷达结构' }]}
          >
            {/* <div style={{ textAlign: 'right' }}>
              <Button
                type='primary'
                size='small'
                disabled={!key}
                onClick={() => {
                  setTreeId();
                }}
              >
                取消选择
              </Button>
            </div> */}
            <Spin spinning={radarTree.loading}>
              <div style={{ height: 380 }}>
                <BaseTree
                  blockNode
                  defaultExpandAll
                  selectedKeys={key ? [key] : []}
                  height={380}
                  bindProps={{ key: 'allId', value: 'allId', title: 'name' }}
                  treeData={flatten(radarTree.data, [])}
                  onSelect={(keys, { node }) => {
                    const { allId } = node as unknown as Record<string, string>;
                    setTreeId(allId);
                  }}
                />
              </div>
            </Spin>
          </BaseForm.Item>
        </Col>
        <Col span={12}>
          <BaseFormSelect
            request={getList}
            label='算法库'
            name='algorithmId'
            placeholder='请选择算法库'
            fieldProps={{
              defaultSelectedOptions: [{ cname: data.algorithmCName, id: data.algorithmId }],
              fieldNames: { label: 'cname', value: 'id' }
            }}
            rules={[{ required: true, message: '请选择算法库' }]}
          />
          <BaseFormText
            label='英文名称'
            name='ename'
            fieldProps={{ maxLength: 50, showCount: true }}
            placeholder='请输入算法模型库英文名称'
            rules={[
              { required: true, message: '请输入算法模型库英文名称' },
              { type: 'string', max: 50, message: '长度不可超过50位' },
              {
                pattern: /^[a-zA-Z][0-9a-zA-Z_-]*$/g,
                message: '英文名称只能包括大小写字母、数字、下划线、短横线,只能以字母作为开头'
              }
            ]}
          />
          <BaseFormText
            label='中文名称'
            name='cname'
            placeholder='请输入算法模型库中文名称'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入算法模型库中文名称' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
          <BaseFormDight
            label='时序长度'
            name='sequentialLength'
            fieldProps={{ step: 1, min: 0 }}
          />
          <BaseFormUpload
            label='模型文件'
            name='modelUrl'
            rules={[{ required: true, message: '请上传模型文件' }]}
          />
          <BaseFormText
            label='版本'
            name='version'
            placeholder='请输入算法模型库版本'
            fieldProps={{ maxLength: 20, showCount: true }}
            rules={[
              { required: true, message: '请输入算法模型库版本' },
              { type: 'string', max: 20, message: '长度不可超过20位' }
            ]}
          />
          <BaseFormTextArea
            fieldProps={{ maxLength: 240, showCount: true }}
            label='模型说明'
            name='remarks'
          />
        </Col>
      </Row>
    </BaseModalForm>
  );
};

export default InfoModal;
