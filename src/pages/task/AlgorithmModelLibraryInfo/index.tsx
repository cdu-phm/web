import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { AlgorithmModelLibraryModel } from '../data';
import { getList as getTaskTypeList } from '../TaskTypeInfo/api';
import { delInfo, getList } from './api';
import BindFileStructureModal from './modals/BindFileStructureModal';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const bindModal = useModal(BindFileStructureModal);
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ taskTypeId?: string; name?: string }>({
    name: undefined
  });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSelect
            request={getTaskTypeList}
            defaultValue={searchInfo.taskTypeId}
            fieldNames={{ label: 'name', value: 'id' }}
            placeholder='请选择任务类型'
            allowClear
            style={{ width: 150 }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, taskTypeId: v });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            style={{ width: 240 }}
            placeholder='请输入中文/英文名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'cname',
              title: '中文名称'
            },
            {
              dataIndex: 'ename',
              title: '英文名称'
            },
            {
              dataIndex: 'algorithmCName',
              title: '所属算法库'
            },
            {
              dataIndex: 'taskTypeName',
              title: '任务类型'
            },
            {
              dataIndex: 'algorithmModelObjectName',
              title: '所属设备',
              renderText: (text) => text || '通用'
            },
            {
              dataIndex: 'version',
              title: '版本'
            },
            {
              dataIndex: 'createDateTime',
              title: '生成日期',
              valueType: 'date'
            },
            {
              dataIndex: 'remarks',
              title: '模型说明'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='blue'
                      icon={<SettingOutlined />}
                      title='绑定文件结构'
                      code='bindDataStructure'
                      onClick={() => {
                        bindModal.load({
                          id: record.id,
                          systemId: record.algorithmModelObject.split('-')[0]
                        });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<AlgorithmModelLibraryModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
