import BaseModal, { ModalFC } from '@/components/base/BaseModal';
import BaseTableTransfer from '@/components/base/BaseTransfer/BaseTableTransfer';
import { useRequest } from '@/utils/request';
import { Spin } from 'antd';
import { useState } from 'react';
import { getAllList } from '../../ExpertKnowledgeLibraryInfo/api';
import { bindAlgorithmExpertKnowledges, getAlgorithmExpertKnowledgesList } from '../api';

const BindExpertKnowledgeLibraryModal: ModalFC<{ id: string }> = ({ id, ...props }) => {
  const columns = [
    {
      dataIndex: 'name',
      title: '名称'
    },
    {
      dataIndex: 'originKey',
      title: 'key'
    },
    {
      dataIndex: 'remarks',
      title: '备注',
      showSorterTooltip: true
    }
  ];
  const knowledegeList = useRequest(getAllList, { initialData: [] });
  const [targetKeys, setTargetKeys] = useState<string[]>([]);
  useRequest(getAlgorithmExpertKnowledgesList, {
    defaultParams: [id],
    onSuccess(data) {
      setTargetKeys(data);
    }
  });
  const save = useRequest(bindAlgorithmExpertKnowledges, {
    manual: true,
    onSuccess() {
      props.onCancel();
    }
  });
  return (
    <BaseModal
      title='算法专家知识绑定'
      {...props}
      onOk={() => {
        save.run(id, targetKeys);
      }}
      okButtonProps={{ disabled: knowledegeList.loading, loading: save.loading }}
    >
      <Spin spinning={knowledegeList.loading}>
        <BaseTableTransfer
          titles={['未绑定专家知识', '已绑定专家知识']}
          dataSource={knowledegeList.data.map((i) => ({
            ...i,
            originKey: i.key
          }))}
          targetKeys={targetKeys}
          onChange={(t) => {
            setTargetKeys(t);
          }}
          rowKey={(record) => record.id}
          bodyHeight={300}
          leftColumns={columns}
          rightColumns={columns}
        />
      </Spin>
    </BaseModal>
  );
};

export default BindExpertKnowledgeLibraryModal;
