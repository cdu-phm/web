import {
  BaseFormSelect,
  BaseFormText,
  BaseFormTextArea,
  BaseModalForm
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { isEmpty } from '@/utils/is';
import { AlgorithmLibraryModel } from '../../data';
import { getList } from '../../IntelligentLibraryInfo/api';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: AlgorithmLibraryModel }> = (props) => {
  const { data = {} as AlgorithmLibraryModel, onSuccess, ...anyProps } = props;
  const { id } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<AlgorithmLibraryModel>
      title='算法库信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={data}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormSelect
        request={getList}
        label='智能库'
        name='intelligencelId'
        placeholder='请选择智能库'
        fieldProps={{
          defaultSelectedOptions: [{ cname: data.intelligencelCName, id: data.intelligencelId }],
          fieldNames: { label: 'cname', value: 'id' }
        }}
        rules={[{ required: true, message: '请选择智能库' }]}
      />
      <BaseFormText
        label='英文名称'
        name='ename'
        placeholder='请输入算法库英文名称'
        fieldProps={{ maxLength: 50, showCount: true }}
        rules={[
          { required: true, message: '请输入算法库英文名称' },
          { type: 'string', max: 50, message: '长度不可超过50位' },
          {
            pattern: /^[a-zA-Z][0-9a-zA-Z_-]*$/g,
            message: '英文名称只能包括大小写字母、数字、下划线、短横线,只能以字母作为开头'
          }
        ]}
      />
      <BaseFormText
        label='中文名称'
        name='cname'
        placeholder='请输入算法库中文名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入算法库中文名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormText
        label='版本'
        name='version'
        placeholder='请输入算法库版本'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入算法库版本' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
