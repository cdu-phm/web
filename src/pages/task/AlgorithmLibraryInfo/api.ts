import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { AlgorithmLibraryModel } from '../data';
/**
 * 分页获取算法库信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/taAlgorithmlLibrarys', data);
}

/**
 * 添加算法库信息
 * @param data
 * @returns
 */
export function addInfo(data: AlgorithmLibraryModel) {
  return request.post('/pc/radar/v1/taAlgorithmlLibrarys', data, {
    success: '算法库信息新增成功'
  });
}

/**
 * 编辑算法库信息
 * @param data
 * @returns
 */
export function editInfo(data: AlgorithmLibraryModel) {
  return request.put('/pc/radar/v1/taAlgorithmlLibrarys', data, {
    success: '算法库信息编辑成功'
  });
}
/**
 * 删除算法库信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taAlgorithmlLibrarys',
    { id },
    { confirm: { content: '确认删除当前算法库信息' }, success: '算法库信息删除成功' }
  );
}
/**
 * 根据算法id查询 专家知识id
 * @returns
 */
export function getAlgorithmExpertKnowledgesList(algorithmId: string) {
  return request.get<string[]>('/pc/radar/v1/taAlgorithmExpertKnowledges', { algorithmId });
}

/**
 * 绑定算法库的专家知识信息
 * @param algorithmId 算法库id
 * @param expertKnowledgeIds 专家知识id列表
 * @returns
 */
export function bindAlgorithmExpertKnowledges(algorithmId: string, expertKnowledgeIds: string[]) {
  return request.post(
    '/pc/radar/v1/taAlgorithmExpertKnowledges',
    {
      algorithmId,
      expertKnowledgeIds
    },
    {
      success: '算法专家知识绑定编辑成功'
    }
  );
}
