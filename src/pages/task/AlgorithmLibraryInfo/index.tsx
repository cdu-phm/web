import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, EditOutlined, PlusOutlined, SettingOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Space } from 'antd';
import { useRef, useState } from 'react';
import { AlgorithmLibraryModel } from '../data';
import { delInfo, getList } from './api';
import BindExpertKnowledgeLibraryModal from './modals/BindExpertKnowledgeLibraryModal';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const bindModal = useModal(BindExpertKnowledgeLibraryModal);
  const [searchInfo, setSearchInfo] = useState<{ name?: string }>({
    name: undefined
  });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            type='primary'
            code='create'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <BaseSearch
            defaultValue={searchInfo.name}
            style={{ width: 240 }}
            placeholder='请输入中文/英文名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'cname',
              title: '中文名称'
            },
            {
              dataIndex: 'ename',
              title: '英文名称'
            },
            {
              dataIndex: 'intelligencelCName',
              title: '所属智能库'
            },
            {
              dataIndex: 'version',
              title: '版本'
            },
            {
              dataIndex: 'createDateTime',
              title: '生成日期',
              valueType: 'date'
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='blue'
                      icon={<SettingOutlined />}
                      title='专家知识'
                      code='bindEKL'
                      onClick={() => {
                        bindModal.load({ id: record.id });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<AlgorithmLibraryModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
