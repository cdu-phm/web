import BasePageFrame from '@/components/base/BasePageFrame';
import List from './List';

const Info = () => {
  return (
    <BasePageFrame>
      <List />
    </BasePageFrame>
  );
};

export default Info;
