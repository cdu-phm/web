import BaseSearch from '@/components/base/BaseSearch';
import BaseSelect from '@/components/base/BaseSelect/index';
import BaseTable from '@/components/base/BaseTable';
import { ProColumns } from '@ant-design/pro-table';
import { DatePicker, Space } from 'antd';
import moment from 'moment';
import { FC, useState } from 'react';
import { TaskResultModel } from '../data';
import { getList as getTaskTypeList } from '../TaskTypeInfo/api';
import { getList } from './api';

const List: FC<{ taskId?: string }> = (props) => {
  const { taskId } = props;
  const [searchInfo, setSearchInfo] = useState<{
    startTime?: string;
    endTime?: string;
    radarName?: string;
    taskTypeId?: string;
    taskId?: string;
  }>({
    startTime: moment().add(-7, 'd').format('yyyy-MM-DD 00:00:00'),
    endTime: moment().add(1, 'd').format('yyyy-MM-DD 00:00:00'),
    taskId
  });
  return (
    <BaseTable
      rowKey='id'
      headerTitle={
        <Space>
          <BaseSearch
            defaultValue={searchInfo.radarName}
            placeholder='请输入雷达名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, radarName: v });
            }}
          />
          <BaseSelect
            request={getTaskTypeList}
            placeholder='请选择任务类型'
            allowClear
            style={{ width: 180 }}
            defaultValue={searchInfo.taskTypeId}
            fieldNames={{ label: 'name', value: 'id' }}
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, taskTypeId: v });
            }}
          />
          <DatePicker.RangePicker
            showTime={{ format: 'HH:mm:ss' }}
            format='YYYY-MM-DD HH:mm:ss'
            placeholder={['任务结果生成开始时间', '任务结果生成结束时间']}
            defaultValue={[moment(searchInfo.startTime), moment(searchInfo.endTime)]}
            onChange={(values, dateStrings) => {
              const [startTime, endTime] = dateStrings;
              setSearchInfo({
                ...searchInfo,
                startTime: startTime !== '' ? startTime : undefined,
                endTime: endTime !== '' ? endTime : undefined
              });
            }}
          />
        </Space>
      }
      params={searchInfo}
      request={getList}
      columns={
        [
          {
            dataIndex: 'radarName',
            title: '雷达'
          },
          {
            dataIndex: 'taskTypeName',
            title: '任务类型'
          },
          {
            dataIndex: 'resultData',
            title: '预测结果'
          },
          {
            dataIndex: 'trustDegree',
            title: '置信度'
          },
          {
            dataIndex: 'createDateTime',
            title: '创建时间',
            valueType: 'dateTime'
          }
        ] as ProColumns<TaskResultModel>[]
      }
    />
  );
};

export default List;
