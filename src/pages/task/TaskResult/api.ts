import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { TaskResultModel } from '../data';
/**
 * 分页获取任务结果信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ taskId?: string }>) {
  return request.list<TaskResultModel>('/pc/radar/v1/taTasksResult', data);
}
