import BaseAction from '@/components/base/BaseAction';
import BaseButton from '@/components/base/BaseButton';
import { useModal } from '@/components/base/BaseModal';
import BasePageFrame from '@/components/base/BasePageFrame';
import BaseSearch from '@/components/base/BaseSearch';
import BaseTable from '@/components/base/BaseTable';
import { EnableStatusEnum } from '@/models';
import { download } from '@/utils';
import { useRequest } from '@/utils/request';
import { DeleteOutlined, DownloadOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { ActionType, ProColumns } from '@ant-design/pro-table';
import { Radio, Space } from 'antd';
import { useRef, useState } from 'react';
import { IntelligentLibraryModel } from '../data';
import { delInfo, getList } from './api';
import InfoModal from './modals/InfoModal';

const Info = () => {
  const tableRef = useRef<ActionType>();
  const infoModal = useModal(InfoModal, {
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const del = useRequest(delInfo, {
    manual: true,
    fetchKey: (id) => id,
    onSuccess() {
      tableRef.current?.reload();
    }
  });
  const [searchInfo, setSearchInfo] = useState<{ name?: string; isUse?: number }>({
    name: undefined,
    isUse: undefined
  });
  return (
    <BasePageFrame>
      <BaseTable
        rowKey='id'
        actionRef={tableRef}
        headerTitle={
          <BaseButton
            code='create'
            type='primary'
            icon={<PlusOutlined />}
            onClick={() => {
              infoModal.load({});
            }}
          >
            新增
          </BaseButton>
        }
        toolBarRender={() => [
          <Radio.Group
            defaultValue={searchInfo.isUse}
            options={[{ label: '全部', value: undefined as any }, ...EnableStatusEnum.list]}
            optionType='button'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, isUse: v.target.value });
            }}
          />,
          <BaseSearch
            defaultValue={searchInfo.name}
            style={{ width: 240 }}
            placeholder='请输入中文/英文名称搜索'
            onChange={(v) => {
              setSearchInfo({ ...searchInfo, name: v });
            }}
          />
        ]}
        params={searchInfo}
        request={getList}
        columns={
          [
            {
              dataIndex: 'cname',
              title: '中文名称'
            },
            {
              dataIndex: 'ename',
              title: '英文名称'
            },
            {
              dataIndex: 'isUse',
              title: '启用状态',
              renderText: EnableStatusEnum.getLabelByValue
            },
            {
              dataIndex: 'version',
              title: '版本'
            },
            {
              dataIndex: 'createDateTime',
              title: '生成日期',
              valueType: 'date'
            },
            {
              dataIndex: 'remarks',
              title: '描述'
            },
            {
              dataIndex: '',
              title: '操作',
              fixed: 'right',
              render(text, record) {
                return (
                  <Space>
                    <BaseAction
                      color='blue'
                      title='下载'
                      code='download'
                      icon={<DownloadOutlined />}
                      onClick={() =>
                        record.urlFileName &&
                        download(record.urlFileName, { filename: record.cname })
                      }
                    >
                      工卡下载
                    </BaseAction>
                    <BaseAction
                      color='green'
                      icon={<EditOutlined />}
                      title='编辑'
                      code='edit'
                      onClick={() => {
                        infoModal.load({ data: record });
                      }}
                    />
                    <BaseAction
                      color='red'
                      icon={<DeleteOutlined />}
                      title='删除'
                      code='delete'
                      loading={del.fetches[record.id]?.loading}
                      onClick={() => {
                        del.run(record.id);
                      }}
                    />
                  </Space>
                );
              }
            }
          ] as ProColumns<IntelligentLibraryModel>[]
        }
      />
    </BasePageFrame>
  );
};

export default Info;
