import { PageParams } from '@/models';
import { request } from '@/utils/request';
import { IntelligentLibraryModel } from '../data';
/**
 * 分页获取智能库信息
 * @param data
 * @returns
 */
export function getList(data: PageParams<{ name?: string }>) {
  return request.list('/pc/radar/v1/taIntelligenceLibrarys', data);
}

/**
 * 添加智能库信息
 * @param data
 * @returns
 */
export function addInfo(data: IntelligentLibraryModel) {
  return request.post('/pc/radar/v1/taIntelligenceLibrarys', data, {
    success: '智能库信息新增成功'
  });
}

/**
 * 编辑智能库信息
 * @param data
 * @returns
 */
export function editInfo(data: IntelligentLibraryModel) {
  return request.put('/pc/radar/v1/taIntelligenceLibrarys', data, {
    success: '智能库信息编辑成功'
  });
}
/**
 * 删除智能库信息
 * @param id
 * @returns
 */
export function delInfo(id: string) {
  return request.delete(
    '/pc/radar/v1/taIntelligenceLibrarys',
    { id },
    { confirm: { content: '确认删除当前智能库信息' }, success: '智能库信息删除成功' }
  );
}
