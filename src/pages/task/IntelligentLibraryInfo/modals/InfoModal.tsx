import {
  BaseFormText,
  BaseFormTextArea,
  BaseFormUpload,
  BaseModalForm,
  ProFormRadio
} from '@/components/base/BaseForm';
import { ModalFC } from '@/components/base/BaseModal';
import { EnableStatusEnum } from '@/models';
import { isEmpty } from '@/utils/is';
import { IntelligentLibraryModel } from '../../data';
import { addInfo, editInfo } from '../api';

const InfoModal: ModalFC<{ data?: IntelligentLibraryModel }> = (props) => {
  const { data = {} as IntelligentLibraryModel, onSuccess, ...anyProps } = props;
  const { id, isUse = EnableStatusEnum.enum.enable } = data;
  const isadd = isEmpty(id);
  return (
    <BaseModalForm<IntelligentLibraryModel>
      title='智能库信息'
      labelCol={{ style: { width: 80 } }}
      width={560}
      initialValues={{ ...data, isUse }}
      {...anyProps}
      onFinish={async (values) => {
        values.id = id;
        const { ok } = await (isadd ? addInfo : editInfo)(values);
        ok && onSuccess && onSuccess();
        return ok;
      }}
    >
      <BaseFormText
        label='英文名称'
        name='ename'
        fieldProps={{ maxLength: 50, showCount: true }}
        placeholder='请输入智能库英文名称'
        rules={[
          { required: true, message: '请输入智能库英文名称' },
          { type: 'string', max: 50, message: '长度不可超过50位' },
          {
            pattern: /^[a-zA-Z][0-9a-zA-Z_-]*$/g,
            message: '英文名称只能包括大小写字母、数字、下划线、短横线,只能以字母作为开头'
          }
        ]}
      />
      <BaseFormText
        label='中文名称'
        name='cname'
        placeholder='请输入智能库中文名称'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入智能库中文名称' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <ProFormRadio.Group
        label='启用状态'
        name='isUse'
        options={EnableStatusEnum.list}
        radioType='button'
      />
      <BaseFormText
        label='版本'
        name='version'
        placeholder='请输入智能库版本'
        fieldProps={{ maxLength: 20, showCount: true }}
        rules={[
          { required: true, message: '请输入智能库版本' },
          { type: 'string', max: 20, message: '长度不可超过20位' }
        ]}
      />
      <BaseFormUpload
        label='智能库文件'
        name='urlFileName'
        extra='文件不可超过100M'
        rules={[{ required: true, message: '请上传智能库文件' }]}
      />
      <BaseFormTextArea
        fieldProps={{ maxLength: 240, showCount: true }}
        label='描述'
        name='remarks'
      />
    </BaseModalForm>
  );
};

export default InfoModal;
