import { BaseModel, EnableStatusEnumValueType } from '@/models';

export interface ExpertKnowledgeLibraryModel extends BaseModel {
  name: string;
  date: string;
  key: string;
  value: string;
  remarks?: string;
}

export interface IntelligentLibraryModel extends BaseModel {
  ename: string;
  cname: string;
  isUse: EnableStatusEnumValueType;
  urlFileName: string;
  version: string;
  remarks?: string;
}

export interface AlgorithmLibraryModel extends BaseModel {
  ename: string;
  cname: string;
  intelligencelId: string;
  intelligencelCName: string;
  intelligencelEName: string;
  date: string;
  version: string;
  remarks?: string;
}

export interface AlgorithmModelLibraryModel extends BaseModel {
  ename: string;
  cname: string;
  algorithmId: string;
  algorithmCName: string;
  algorithmEName: string;
  taskTypeId: string;
  taskTypeName: string;
  algorithmModelObject: string;
  version: string;
  sequentialLength: number;
  modelUrl: string;
  remarks?: string;
}

export interface TaskTypeModel extends BaseModel {
  name: string;
  remarks?: string;
}

export interface TaskModel extends BaseModel {
  treeId: string;
  treeName: string;
  typeId: string;
  typeName: string;
  algorithmModelId: string;
  algorithmModelCName: string;
  algorithmModelEName: string;
  startTime: string;
  endTime: string;
  status: EnableStatusEnumValueType;
  cycle: number;
  remarks: string;
  radarId?: string;
  systemId?: string;
  extensionId?: string;
  keyPartsId?: string;
}

export interface TaskResultModel extends BaseModel {
  createDateTime: string;
  radarName: string;
  resultData: string;
  taskTypeName: string;
  trustDegree: string;
}
