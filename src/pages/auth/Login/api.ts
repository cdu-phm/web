import { UserInfo } from '@/store/user';
import { request } from '@/utils/request';

/**
 *
 * @param data
 * @returns
 */
export function login(data: { username: string; password: string }) {
  return request.post<UserInfo>(
    '/pc/v1/login',
    {},
    {
      params: data,
      data
    }
  );
}
