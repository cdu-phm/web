import loginBanner from '@/assets/images/login-banner.png';
import loginBg from '@/assets/images/login-bg.png';
import Logo from '@/assets/images/logo.png';
import Logo1 from '@/assets/images/logo1@2x.png';
import BaseForm, { BaseFormText, ProFormText } from '@/components/base/BaseForm';
import { login } from '@/pages/auth/Login/api';
import { history } from '@/router';
import useMenuStore from '@/store/menu';
import useUserStore from '@/store/user';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import type { FC } from 'react';
import styled from 'styled-components';

const LoginLayout = styled.div`
  position: relative;
  background-image: url('${loginBg}');
  background-size: 100% 100%;
  min-height: 720px;
  min-width: 1200px;
  height: 100vh;
  width: 100vw;
  color: ${(props) => props.theme.primaryColor};
  @keyframes enter-x-animation {
    to {
      opacity: 1;
      transform: translate(0);
    }
  }
  .copyright {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    bottom: 34px;
    margin: auto;
    color: #8aa2af;
    text-align: center;
    .compony {
      width: 258px;
      height: 34px;
    }
  }
`;
const LoginWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  width: 1160px;
  height: 600px;
  border-radius: 30px;
  box-shadow: 0px 0px 25px 0px #005aff;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background: #fff;
  padding: 0 95px;

  .login-prefix {
    background-image: url('${loginBanner}');
    background-size: 100% 100%;
    z-index: 1;
    width: 411px;
    height: 392px;
  }
`;

const LoginForm = styled.div`
  .login-title {
    font-size: 26px;
    font-weight: 500;
    color: #000000;
    margin-bottom: 120px;
  }
  .ant-input-affix-wrapper {
    background: transparent;
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom-style: dashed;
    padding: 10px 12px;
  }
  .ant-input-prefix,
  .ant-input-suffix,
  .ant-input-password-icon {
    color: #2994ff;
    font-size: 24px;
  }
  .enter-x {
    margin-top: 50px;
    background: #2994ff !important;
    color: #fff !important;
    height: 60px !important;
    border: none;
  }
  /* .enter-x,
  h1 {
    opacity: 0;
    transform: translate(50px);
  }
  h1 {
    animation: enter-x-animation 0.4s ease-in-out 0.3s;
    animation-fill-mode: forwards;
  }
  .enter-x {
    animation: enter-x-animation 0.4s ease-in-out 0.7s;
    animation-fill-mode: forwards;
  }
  .ant-row:nth-child(2) {
    animation: enter-x-animation 0.4s ease-in-out 0.3s;
    animation-fill-mode: forwards;
  }
  .ant-row:nth-child(3) {
    animation: enter-x-animation 0.4s ease-in-out 0.5s;
    animation-fill-mode: forwards;
  } */
`;

const Login: FC = () => {
  const { setToken, setUser } = useUserStore.getState();
  const { setPermissions } = useMenuStore.getState();
  const submit = async (values: { username: string; password: string }) => {
    const { ok, data } = await login(values);
    if (ok) {
      const { token, userPermissions } = data;
      setPermissions(userPermissions);
      setToken(token);
      setUser(data);
      // const { search } = history.location;
      requestAnimationFrame(() => {
        history.replace(`/`);
      });
    }
  };

  return (
    <LoginLayout>
      <LoginWrap>
        <div className='login-prefix' />
        <div style={{ width: '50%' }}>
          <LoginForm>
            <div className='login-title'>
              <img src={Logo} width={96} height={96} /> 欢迎登录气象雷达预测评估平台
            </div>
            <BaseForm
              className='animation-form'
              isKeyPressSubmit
              submitter={{
                render(formprops) {
                  return (
                    <Button
                      {...formprops.submitButtonProps}
                      block
                      className='enter-x'
                      size='large'
                      shape='round'
                      onClick={formprops.submit}
                    >
                      立即登录
                    </Button>
                  );
                }
              }}
              size='large'
              onFinish={submit}
            >
              <ProFormText
                name='username'
                placeholder='请输入用户名'
                allowClear={false}
                fieldProps={{ prefix: <UserOutlined /> }}
                rules={[{ message: '请输入用户名', required: true }]}
              />
              <BaseFormText.Password
                name='password'
                placeholder='请输入密码'
                fieldProps={{ prefix: <LockOutlined /> }}
                rules={[{ message: '请输入密码', required: true }]}
              />
            </BaseForm>
          </LoginForm>
        </div>
      </LoginWrap>
      <div className='copyright'>
        <img className='compony' src={Logo1} alt='成都中电锦江信息产业有限公司' />
        <div>构建时间： {APP_INFO.lastBuildTime}</div>
        <div>{APP_INFO.pkg.version}</div>
      </div>
    </LoginLayout>
  );
};

export default Login;
