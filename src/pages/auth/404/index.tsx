import { ReactComponent as ErrorIcon } from '@/assets/svgs/404.svg';
import { history } from '@/router';
import { Button, Result } from 'antd';

const Error404 = () => {
  return (
    <Result
      status='error'
      title='权限不足'
      icon={<ErrorIcon style={{ width: '44vw', height: '40vh' }} />}
      subTitle='抱歉，权限不足，无法访问当前页面，如需修改权限，请联系管理员'
      extra={
        <Button type='primary' onClick={() => history.replace('/')}>
          返回首页
        </Button>
      }
    />
  );
};

export default Error404;
