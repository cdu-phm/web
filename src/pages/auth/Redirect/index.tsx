import Loading from '@/components/base/BaseLoading';
import { history } from '@/router';
import { getParameters } from '@/utils';
import { useMount } from 'ahooks';
import type { FC } from 'react';

const Redirect: FC = () => {
  useMount(() => {
    const redirect = getParameters().get('redirect');
    console.log(redirect);
    history.replace(redirect ?? '/');
  });
  return <Loading />;
};

export default Redirect;
