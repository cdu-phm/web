import Loading from '@/components/base/BaseLoading';
import BaseModal from '@/components/base/BaseModal';
import useUserStore from '@/store/user';
import { getParameters } from '@/utils';
import { useMount } from 'ahooks';
import type { FC } from 'react';

const Logout: FC = () => {
  const { logout } = useUserStore.getState();
  useMount(() => {
    const redirect = getParameters().get('redirect');
    BaseModal.destroyAll();
    logout(redirect!);
  });
  return <Loading />;
};

export default Logout;
