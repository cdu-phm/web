import { request, request_1 } from '@/utils/request';
import { AlgorithmDataModel, DataSimulationModel, IndicatorSimulationModel } from './data';

export const saveData = (data: DataSimulationModel) => {
  return request.post('​/pc​/ta​/v1​/simulationData', data);
};
export const getData = () => {
  return request.get<DataSimulationModel[]>('​/pc​/ta​/v1​/simulationData');
};

export const saveAlgorithmData = (data: AlgorithmDataModel) => {
  return request.post('​/pc/ta/v1/simulationAlgorithmData', data);
};
export const getAlgorithmData = () => {
  return request.get<AlgorithmDataModel[]>('​/pc/ta/v1/simulationAlgorithmData');
};

export const getIndicatorSimulationData = (data: { radarId: string }) => {
  return request_1.get<IndicatorSimulationModel[]>('/pc/ta/v1/simulationOfIndexResult', data);
};

export const getLifeModel = (data: { keyPartsModelId: string }) => {
  return request.get<
    {
      cname: string;
      id: string;
      keyPartsModelId: string;
    }[]
  >('/pc/radar/v1/LifeModel', data);
};

export const getLifePieceModel = (data: {
  radarId: string;
  keyPartsModelId: string;
  ids: string;
}) => {
  return request.get<
    {
      totalLife: string;
      activeLife: number;
      residualLife: number;
      failureTime: string;
      name: string;
      typeName: string;
      taAlgorithmModelName?: string;
      confidence: number;
    }[]
  >('/pc/radar/v1/LifePieceModel', data);
};
