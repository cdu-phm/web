import BaseButton from '@/components/base/BaseButton';
import BaseEchart from '@/components/base/BaseEchart';
import BaseForm, {
  BaseFormSelect,
  ProFormDependency,
  ProFormInstance
} from '@/components/base/BaseForm';
import BasePageFrame from '@/components/base/BasePageFrame';
import { getList as getRadarList } from '@/pages/radar/RadarInfo/api';
import { getList as getRadarTypeList } from '@/pages/radar/RadarTypeInfo/api';
import { useRequest } from '@/utils/request';
import { Alert, Card, ConfigProvider, Empty, Spin } from 'antd';
import { useRef, useState } from 'react';
import { getIndicatorSimulationData } from '../api';

const IndicatorSimulation = () => {
  const form = useRef<ProFormInstance>();
  const [realData, setRealData] = useState<{
    xData: number[];
    diagnosticRateData: number[];
    healthPredictionRateData: number[];
    realFalultNumsData: string[];
    totalFaultNumsData: string[];
  }>({
    xData: [],
    diagnosticRateData: [],
    healthPredictionRateData: [],
    realFalultNumsData: [],
    totalFaultNumsData: []
  });
  const data = useRequest(getIndicatorSimulationData, {
    manual: true,
    onSuccess(data = []) {
      const xData: number[] = [];
      const realFalultNumsData: string[] = [];
      const totalFaultNumsData: string[] = [];
      const diagnosticRateData: number[] = [];
      const healthPredictionRateData: number[] = [];
      data.forEach((i) => {
        xData.push(i.sampleNum);
        realFalultNumsData.push(i.realFalultNums);
        totalFaultNumsData.push(i.totalFaultNums);
        diagnosticRateData.push(i.diagnosticRate);
        healthPredictionRateData.push(i.healthPredictionRate);
      });
      setRealData({
        xData,
        realFalultNumsData,
        totalFaultNumsData,
        diagnosticRateData,
        healthPredictionRateData
      });
    }
  });

  return (
    <BasePageFrame>
      <div
        style={{
          height: '100%',
          width: 720,
          margin: 'auto',
          display: 'flex',
          flexDirection: 'column'
        }}
      >
        <Spin spinning={data.loading}>
          <Card>
            <BaseForm
              formRef={form}
              initialValues={{ simulationDataType: 0 }}
              labelCol={{ style: { width: 80 } }}
              layout='horizontal'
              submitter={{
                render: (props) => {
                  return (
                    <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                      <BaseButton
                        type='primary'
                        block
                        onClick={() => {
                          props.submit();
                        }}
                      >
                        计算结果
                      </BaseButton>
                    </div>
                  );
                }
              }}
              onFinish={async (values) => {
                data.run(values);
              }}
            >
              <h2>请选择仿真参数</h2>
              <BaseFormSelect
                label='雷达型号'
                name='radarTypeId'
                request={getRadarTypeList}
                placeholder='请选择雷达型号'
                fieldProps={{
                  fieldNames: { label: 'name', value: 'id' },
                  onChange() {
                    form.current?.setFieldsValue({ radarId: undefined });
                  }
                }}
                rules={[{ required: true, message: '请选择雷达型号' }]}
              />
              <ProFormDependency name={['radarTypeId']}>
                {({ radarTypeId }) => {
                  return (
                    <ConfigProvider
                      renderEmpty={() => (
                        <Empty
                          image={Empty.PRESENTED_IMAGE_SIMPLE}
                          description='暂无可进行仿真的雷达，请检查雷达类型下有绑定雷达，且雷达已有数据表'
                        />
                      )}
                    >
                      <BaseFormSelect
                        label='雷达'
                        name='radarId'
                        placeholder='请选择雷达'
                        hiddenItem={!radarTypeId}
                        hiddenRender={() => <Alert type='warning' message='请先选择雷达型号' />}
                        params={{ typeId: radarTypeId, isRUL: 1 }}
                        fieldProps={{
                          fieldNames: { label: 'name', value: 'id' }
                        }}
                        request={getRadarList}
                        rules={[
                          {
                            required: true,
                            message: radarTypeId ? '请选择雷达' : '请先选择雷达型号'
                          }
                        ]}
                      />
                    </ConfigProvider>
                  );
                }}
              </ProFormDependency>
            </BaseForm>
          </Card>
        </Spin>
        <Card style={{ flex: 1, marginTop: 8 }} bodyStyle={{ height: '100%' }}>
          {data.data ? (
            <BaseEchart
              style={{ height: '100%' }}
              option={{
                title: {
                  text: '仿真结果'
                },
                tooltip: {
                  trigger: 'axis'
                },
                legend: {},
                grid: {
                  left: '3%',
                  right: '60',
                  bottom: '20',
                  containLabel: true
                },
                xAxis: {
                  name: '样本数',
                  type: 'category',
                  boundaryGap: false,
                  nameTextStyle: {
                    padding: 20
                  },
                  data: realData?.xData
                },
                yAxis: [
                  {
                    type: 'value',
                    name: '诊断率 (%)',
                    interval: 10,
                    min: 60,
                    max: 100
                  },
                  {
                    type: 'value',
                    name: '健康预测准确度 (%)',
                    interval: 10,
                    min: 60,
                    max: 100
                  }
                ],
                series: [
                  {
                    name: '诊断率',
                    type: 'line',
                    yAxisIndex: 0,
                    data: realData.diagnosticRateData,
                    smooth: true
                  },
                  {
                    name: '健康预测准确度',
                    yAxisIndex: 1,
                    type: 'line',
                    data: realData.healthPredictionRateData,
                    smooth: true
                  }
                ]
              }}
            />
          ) : (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='暂无结果' />
          )}
        </Card>
      </div>
    </BasePageFrame>
  );
};

export default IndicatorSimulation;
