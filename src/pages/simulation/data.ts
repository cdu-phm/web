import { BaseModel } from '@/models';

export interface DataSimulationModel extends BaseModel {
  simulationDataType: number;
  radarTypeId: string;
  radarId: string;
  radarTypeName: string;
  radarName: string;
  dataSourceName: string;
  startDataTime: string;
  endDataTime: string;
  state: number;
}

export interface AlgorithmDataModel extends BaseModel {
  dataSourceName: string;
  endDataTime: Date;
  id: string;
  raKeyPartsModelId: string;
  raKeyPartsModelName: string;
  radarId: string;
  radarTypeId: string;
  radarTypeName: string;
  radarName: string;
  simulationDataType: number;
  startDataTime: Date;
  state: number;
}

export interface IndicatorSimulationModel extends BaseModel {
  createTime: string;
  diagnosticRate: number;
  healthPredictionRate: number;
  realFalultNums: string;
  sampleNum: number;
  siteCode: string;
  totalFaultNums: string;
}
