import BaseButton from '@/components/base/BaseButton';
import BaseEchart from '@/components/base/BaseEchart';
import BasePageFrame from '@/components/base/BasePageFrame';
import { useRequest } from '@/utils/request';
import { ExportOutlined } from '@ant-design/icons';
import { DatePicker, Empty, Space, Spin } from 'antd';
import { useState } from 'react';
import styled from 'styled-components';
import { exportSubsystemFaultNum, getSubsystemFaultNum } from '../api';

const Wrap = styled.div`
  height: 100%;
  .ant-spin-nested-loading,
  .ant-spin-container {
    height: 100%;
  }
`;
const FaultSubsystem = () => {
  const [data, setData] = useState<{ xData: string[]; sData: number[] }>({ xData: [], sData: [] });
  const [date, setDate] = useState<{ startTime?: string; endTime?: string }>({});
  const getdata = useRequest(getSubsystemFaultNum, {
    onSuccess(resdata) {
      const dataList = Object.entries(resdata);
      const xData: string[] = [];
      const sData: number[] = [];
      dataList.forEach(([key, value]) => {
        xData.push(key);
        sData.push(value);
      });
      setData({ xData, sData });
    }
  });
  return (
    <BasePageFrame>
      <Wrap>
        <Spin style={{ height: '100%' }} spinning={getdata.loading}>
          <div
            style={{ height: '100%', display: 'flex', flexDirection: 'column', overflow: 'hidden' }}
          >
            <Space>
              <DatePicker.RangePicker
                allowClear
                onChange={(date, dateString) => {
                  const [startTime, endTime] = dateString;
                  setDate({ startTime, endTime });
                }}
              />
              <BaseButton
                onClick={() => {
                  getdata.run(date);
                }}
              >
                查询
              </BaseButton>
              <BaseButton
                type='primary'
                icon={<ExportOutlined />}
                onClick={() => {
                  exportSubsystemFaultNum(date);
                }}
              >
                导出
              </BaseButton>
            </Space>
            <div style={{ flex: 1, marginTop: 16 }}>
              {data.xData.length > 0 ? (
                <BaseEchart
                  style={{ height: '100%' }}
                  option={{
                    tooltip: {
                      trigger: 'axis',
                      axisPointer: {
                        type: 'shadow'
                      },
                      formatter: '分系统：{b}<br/>故障次数：{c}'
                    },
                    xAxis: {
                      type: 'category',
                      data: data.xData,
                      name: '分系统',
                      axisLabel: {
                        interval: 0
                      }
                    },
                    yAxis: {
                      type: 'value',
                      name: '故障次数',
                      max: Math.max(...data.sData)
                    },
                    series: [
                      {
                        data: data.sData,
                        type: 'bar',
                        barWidth: 40,
                        itemStyle: {
                          color: '#4bf54d'
                        }
                      }
                    ]
                  }}
                />
              ) : (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='暂无统计数据' />
              )}
            </div>
          </div>
        </Spin>
      </Wrap>
    </BasePageFrame>
  );
};

export default FaultSubsystem;
