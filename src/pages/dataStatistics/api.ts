import { request } from '@/utils/request';

export const getRadarTypeFaultNum = (data?: { startTime?: string; endTime?: string }) => {
  return request.get<Record<string, number>>('/pc/screen/v1/radarTypeFaultsNum', data);
};

export const getSubsystemFaultNum = (data?: { startTime?: string; endTime?: string }) => {
  return request.get<Record<string, number>>('/pc/screen/v1/systemFaultsNum', data);
};

export const exportRadarTypeFaultNum = (data?: { startTime?: string; endTime?: string }) => {
  return request.post<Record<string, number>>('/pc/screen/v1/exportRadarTypeFaultsNum', data, {
    responseType: 'blob'
  });
};

export const exportSubsystemFaultNum = (data?: { startTime?: string; endTime?: string }) => {
  return request.post<Record<string, number>>('/pc/screen/v1/exportSystemFaultsNum', data, {
    responseType: 'blob'
  });
};
