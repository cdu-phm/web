import moment from 'moment';
import { useEffect, useState } from 'react';

const useNowDate = () => {
  const [date, setDate] = useState(() => moment());
  useEffect(() => {
    const timer = setTimeout(() => {
      setDate(moment());
    }, 1000);
    return () => {
      clearTimeout(timer);
    };
  }, [date]);
  return date;
};
export default useNowDate;
