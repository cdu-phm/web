// import MultiplePageControl from './MultiplePageControl';
import bg from '@/assets/images/bg.png';
import { withRouter } from '@/router';
import { Layout } from 'antd';
import { memo } from 'react';
import { CSSTransition, SwitchTransition } from 'react-transition-group';
import styled from 'styled-components';
import HeaderTop from './TheHeader';
import TheMenu from './TheMenu';

const { Header, Content } = Layout;
const OverLayout = styled(Layout)`
  && {
    height: 100vh;
    /* background-image: url('${bg}'); */
    background-size: 100% 100%;
    min-height: 768px;
    min-width: 1024px;
  }
`;
const LayoutHeader = styled(Header)`
  &&& {
    height: 60px;
    padding: 0;
    background: #001529;
    background-repeat: no-repeat;
    background-size: cover;
  }
`;
const LayoutContent = styled(Content)`
  overflow: auto;
  .fade-enter {
    /* transform: scale(0.9); */
    opacity: 0;
  }
  .fade-enter-active {
    transform: translateX(0);
    opacity: 1;
    transition: opacity 300ms, transform 300ms;
  }
  .fade-exit {
    opacity: 1;
  }
  .fade-exit-active {
    opacity: 0;
    transition: opacity 300ms, transform 300ms;
  }
  .content-wrap {
    position: relative;
    height: 100%;
  }
`;
const Version = styled.div`
  font-size: 12px;
  text-align: right;
  line-height: 18px;
  padding-right: 16px;
  span:hover {
    color: ${(props) => props.theme.primaryColor};
  }
  span + span {
    margin-left: 8px;
  }
`;
const MainLayout = styled(Layout)`
  position: relative;
  background: ${(props) => props.theme.lightBackground};
`;

const SecurityLayout = withRouter(({ children, location }) => {
  // useSocket();
  return (
    <OverLayout>
      <TheMenu />
      <MainLayout>
        <LayoutHeader>
          <HeaderTop />
        </LayoutHeader>
        <LayoutContent id='scrollBox'>
          <SwitchTransition mode='out-in'>
            <CSSTransition<undefined>
              key={location.pathname}
              classNames='fade'
              timeout={300}
              addEndListener={(node: HTMLElement, done: () => void) => {
                node.addEventListener('transitionend', done, false);
              }}
            >
              <div className='content-wrap'>{children}</div>
            </CSSTransition>
          </SwitchTransition>
        </LayoutContent>
        <Version>
          <span>{APP_INFO.pkg.version}</span>
          <span>发布时间：{APP_INFO.lastBuildTime}</span>
        </Version>
      </MainLayout>
    </OverLayout>
  );
});

export default memo(SecurityLayout);
