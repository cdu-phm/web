import logo from '@/assets/images/logo.png';
import logo2 from '@/assets/images/logo2.png';
import { getDevNavs, history } from '@/router';
import { iconMap } from '@/router/routes';
import useMenuStore, { MenuInfo } from '@/store/menu';
import useTabStore from '@/store/tab';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { useToggle } from 'ahooks';
import { Layout, Menu } from 'antd';
import classNames from 'classnames';
import { memo } from 'react';
import styled from 'styled-components';

const { Sider } = Layout;
const MenuContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  .menu-wrap {
    flex: 1;
    overflow-x: hidden;
    overflow-y: auto;
    .ant-menu {
      font-size: 15px;
    }
    ::-webkit-scrollbar {
      width: 0px;
      height: 0px;
      border-radius: 3px;
    }
    ::-webkit-scrollbar-track {
      background: #001529;
    }

    ::-webkit-scrollbar-thumb {
      background: rgba(102, 102, 102, 0.5);
    }

    ::-webkit-scrollbar-thumb:hover {
      background: #999;
    }
  }

  .logo {
    width: 100%;
    display: flex;
    justify-content: center;
    padding: 8px 0 8px;
    img {
      width: 25%;
      transition: width ease 0.5s;
    }
    img + img {
      margin-left: 10%;
    }
    &.collapse {
      img {
        width: 45%;
      }
    }
  }
  .ant-menu-submenu-title {
    height: 69px !important;
    background-size: 100% 100%;
  }
`;
const MenuAction = styled.div`
  flex-shrink: 0;
  height: 40px;
  font-size: 20px;
  line-height: 40px;
  text-align: center;
  border-top: 1px solid #333;
  color: #fff;
`;
const getSubMenu = (nav: MenuInfo) => {
  const Icon = iconMap[nav.key];
  return (
    <Menu.SubMenu
      key={nav.target}
      icon={Icon && <Icon />}
      title={<div title={nav.name}>{nav.name}</div>}
    >
      {(nav.children ?? []).map((i) => {
        const CIcon = iconMap[i.key];
        return i.children ? (
          getSubMenu(i)
        ) : (
          <Menu.Item icon={CIcon && <CIcon />} title={i.name} key={i.target}>
            {i.name}
          </Menu.Item>
        );
      })}
    </Menu.SubMenu>
  );
};
const TheMenu = () => {
  const [collapse, { toggle }] = useToggle(false);
  const { navigations } = useMenuStore.getState();
  const { activeTabs } = useTabStore();
  const navs = [...getDevNavs(), ...(navigations ?? [])];
  const Icon = collapse ? MenuUnfoldOutlined : MenuFoldOutlined;
  return (
    <Sider width={220} collapsed={collapse} theme='dark'>
      <MenuContainer>
        <div className={classNames('logo', { collapse })}>
          <img src={logo} />
          <img src={logo2} />
        </div>
        <div className='menu-wrap'>
          <Menu
            mode='inline'
            theme='dark'
            onClick={(info) => {
              if (info.key.includes('monitor')) {
                window.open(info.key, '_blank');
              } else {
                history.push(info.key);
              }
            }}
            defaultOpenKeys={activeTabs}
            selectedKeys={activeTabs}
          >
            {navs.map((i) => {
              const Icon = iconMap[i.key];

              return i.children ? (
                getSubMenu(i)
              ) : (
                <Menu.Item icon={Icon && <Icon />} title={i.name} key={i.target}>
                  {i.name}
                </Menu.Item>
              );
            })}
          </Menu>
        </div>
        <MenuAction>
          <Icon onClick={() => toggle()} />
        </MenuAction>
      </MenuContainer>
    </Sider>
  );
};

export default memo(TheMenu);
