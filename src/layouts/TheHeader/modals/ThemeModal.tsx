import BaseModal, { ModalFC } from '@/components/base/BaseModal';
import colors from '@/design/color';
import useThemeStore from '@/store/theme';
import { CheckOutlined } from '@ant-design/icons';
import styled from 'styled-components';

const ThemeItem = styled.span<{ color: string }>`
  && {
    padding: 0;
    height: 24px;
    width: 24px;
    transition: border ease-in 0.3s;
    background-color: ${(props) => props.color};
    color: #fff;
    font-size: 16px;
    text-align: center;
    line-height: 24px;
    cursor: pointer;
  }
  & + & {
    margin-left: 8px;
  }
`;

const ThemeModal: ModalFC = (props) => {
  const { activeTheme, setTheme } = useThemeStore();
  const setThemeChange = (value: string) => {
    setTheme(value);
  };
  return (
    <BaseModal width={400} title='主题设置' footer={null} closable={true} {...props}>
      <h4>可选主题</h4>
      <div style={{ display: 'flex' }}>
        {colors.map((i) => (
          <ThemeItem color={i.color} key={i.color} onClick={() => setThemeChange(i.name)}>
            {activeTheme === i.name && <CheckOutlined />}
          </ThemeItem>
        ))}
      </div>
    </BaseModal>
  );
};

export default ThemeModal;
