import { FC } from 'react';

const FragmentLayout: FC = (props) => {
  return <>{props.children}</>;
};

export default FragmentLayout;
