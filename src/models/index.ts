import { Enum, EnumValueType } from '@/utils/enum';

export const PageTypeEnum = Enum({
  all: {
    label: '全部查询',
    value: '01'
  },
  page: {
    label: '分页',
    value: '00'
  }
});

export type PageParams<T> = {
  pageNum?: number;
  pageSize?: number;
} & T;
export interface Page<T extends Record<string, unknown>> {
  rowCount: number; // 条数
  list: T[]; // 数据数组
  pageNum: number;
  pageSize: number;
}

export interface BaseModel {
  id: string;
  createDateTime?: string;
  creator?: string;
  updateDateTime?: string;
  updater?: string;
}

export const EnableStatusEnum = Enum({
  enable: {
    label: '启用',
    value: 1
  },
  disable: {
    label: '停用',
    value: 0
  }
} as const);

export const TrueFalseEnum = Enum({
  true: {
    label: '是',
    value: 1
  },
  false: {
    label: '否',
    value: 0
  }
});

export type TrueFalseEnumValueType = EnumValueType<typeof TrueFalseEnum.enum>;
export type EnableStatusEnumValueType = EnumValueType<typeof EnableStatusEnum.enum>;
