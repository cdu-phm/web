import { history } from '@/router';
import useUserStore from '@/store/user';
import { range } from '@/utils/array';
import { ReloadOutlined } from '@ant-design/icons';
import { Affix, Alert, Space, Tooltip } from 'antd';
import type { FC, ReactNode } from 'react';
import Marquee from 'react-fast-marquee';
import styled from 'styled-components';

const FrameLayout = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;
const FrameHead = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 40px;
  padding: 0 20px;
  background: ${(props) => props.theme.nomalBackground};
  box-shadow: 0 2px 5px ${(props) => props.theme.borderColor};
  font-size: 16px;
  .title {
    font-weight: 500;
    font-size: 16px;
  }
`;
const FrameBody = styled.div`
  flex: 1;
  padding: 10px;
  .frame-content {
    height: 100%;
    &.flex {
      display: flex;
      overflow: hidden;
      gap: 12px;
    }
    &.background {
      background: ${(props) => props.theme.nomalBackground};
    }
  }
`;
const FrameFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  padding: 10px;
  background: ${(props) => props.theme.nomalBackground};
  box-shadow: 0 -2px 5px ${(props) => props.theme.borderColor};
`;
interface PageFrameProps {
  /**
   * rightRender自带Space,会自动分隔所有的模块
   *
   * rightRender={[<Button>用法1</Button>, <Button>用法2</Button>]}
   */
  rightRender?: ReactNode[];
  title?: string;
  /**
   * body 是否渲染背景色， 默认关闭
   */
  background?: boolean;
  footer?: ReactNode;
  /**
   * frame body模块是否开启flex模式，默认关闭
   */
  bodyFlex?: boolean;
}
const BasePageFrame: FC<PageFrameProps> = (props) => {
  const {
    title = document.title,
    bodyFlex = false,
    rightRender = null,
    background = false,
    footer
  } = props;
  const {
    userinfo: { area }
  } = useUserStore();
  return (
    <FrameLayout>
      <Affix offsetTop={0} target={() => document.getElementById('scrollBox')}>
        <FrameHead>
          <div className='title'>{title}</div>
          <Space>
            {rightRender}
            <Tooltip title='刷新' placement='bottom'>
              <ReloadOutlined
                onClick={() => {
                  history.replace(`/redirect?redirect=${history.location.pathname}`);
                }}
              />
            </Tooltip>
          </Space>
        </FrameHead>
        {area.length === 0 && (
          <Alert
            closable={false}
            type='warning'
            banner
            message={
              <Marquee pauseOnHover gradient={false}>
                {range(5).map((i) => (
                  <span key={i} style={{ margin: '0 16px' }}>
                    当前用户暂未绑定区域，请先让管理员绑定区域后再进行操作！
                  </span>
                ))}
              </Marquee>
            }
          />
        )}
      </Affix>
      <FrameBody>
        <div
          className={`${background ? 'background' : ''} ${bodyFlex ? 'flex' : ''} frame-content`}
        >
          {props.children}
        </div>
      </FrameBody>
      {footer && (
        <Affix offsetBottom={0}>
          <FrameFooter>{footer}</FrameFooter>
        </Affix>
      )}
    </FrameLayout>
  );
};
export default BasePageFrame;
