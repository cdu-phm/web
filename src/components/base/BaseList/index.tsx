import type { Result } from '@/utils/request/type';
import type { ProListMetas, ProListProps } from '@ant-design/pro-list';
import ProList from '@ant-design/pro-list';
import type { ActionType } from '@ant-design/pro-table';
import type { CSSProperties } from 'react';
import { useCallback, useImperativeHandle, useRef } from 'react';
import styled from 'styled-components';
import type { Request } from '../BaseTable';

export interface BaseListProps<
  RecordType extends Record<string, any>,
  U extends Record<string, any>
> extends Omit<ProListProps<RecordType, U>, 'request' | 'scroll' | 'metas'> {
  metas?: ProListMetas<RecordType>;
  request?: Request<RecordType>;
  height?: number | string;
  hoverable?: boolean;
  bodyStyle?: CSSProperties;
  bordered?: boolean;
}

const ListWrap = <RecordType, U>(props: BaseListProps<RecordType, U>) => {
  const { request, actionRef, dataSource, options, pagination, ...restprops } = props;
  const tableref = useRef<ActionType>();
  // 自定义ref暴露值
  useImperativeHandle(actionRef, () => tableref.current, [tableref.current]);
  const customRequest = useCallback(
    async (params, sort, filter) => {
      let res: { data: RecordType[]; success: true; total: number } = {
        data: [],
        success: true,
        total: 0
      };
      if (request) {
        const resFn = request(params, sort, filter) as unknown as Result<{
          length: number;
          data: any[];
        }>;
        const data = await resFn;
        if (data.ok) {
          res = {
            data: data.data.data || [],
            // success 请返回 true，
            // 不然 table 会停止解析数据，即使有数据
            success: true,
            // 不传会使用 data 的长度，如果是分页一定要传
            total: data.data.length || 0
          };
        }
      } else {
        res.data = (dataSource as RecordType[]) || [];
        res.total = dataSource?.length || 0;
      }
      return res;
    },
    [request, dataSource]
  );
  return (
    <ProList
      {...restprops}
      actionRef={tableref}
      options={
        typeof options === 'boolean'
          ? options
          : { fullScreen: false, ...options, setting: false, density: false }
      }
      pagination={
        pagination === false
          ? false
          : {
              simple: false,
              size: 'default',
              pageSize: 20,
              showSizeChanger: false,
              defaultCurrent: 1,
              current: 1,
              showTotal: (total) => `共${total}条`,
              ...(pagination as any)
            }
      }
      request={customRequest}
    />
  );
};
const BaseList = styled(ListWrap)`
  &.ant-pro-list .ant-list > .ant-spin-nested-loading > .ant-spin-container {
    height: ${({ height }) => (typeof height === 'number' ? `${height}px` : height)};
    overflow-x: hidden;
    overflow-y: auto;
  }
  &&& .ant-list .ant-list-item-meta-avatar {
    display: ${(props) => (props.metas === undefined || 'avatar' in props.metas ? 'flex' : 'none')};
    color: ${(props) => props.theme.primaryColor};
  }
` as typeof ListWrap;
export default BaseList;
