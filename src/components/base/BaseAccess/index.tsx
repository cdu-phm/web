import { history } from '@/router';
import useMenuStore from '@/store/menu';
import { isProd } from '@/utils/is';
import { isArray } from 'lodash-es';
import { FC } from 'react';

const BaseAccess: FC<{ code: string | string[] }> = (props) => {
  const { children, code = '' } = props;
  const { buttonMap } = useMenuStore.getState();
  const { location } = history;
  const permissions = buttonMap[location.pathname] ?? [];
  let hasPer = !isProd;
  if (isArray(code)) {
    const coditem = code.find((i) => permissions.includes(i));
    if (coditem) {
      hasPer = true;
    }
  } else if (code === '' || permissions.includes(code)) {
    hasPer = true;
  }
  return <>{hasPer ? children : null}</>;
};

export default BaseAccess;
