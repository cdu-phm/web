import { EChartsOption } from 'echarts';
import ReactEchart, { EChartsReactProps } from 'echarts-for-react';
import { forwardRef } from 'react';

export interface BaseEchartProps extends EChartsReactProps {
  option: EChartsOption;
}
const BaseEchart = forwardRef<ReactEchart, BaseEchartProps>((props, ref) => {
  return <ReactEchart ref={ref} {...props} />;
});

export default BaseEchart;
