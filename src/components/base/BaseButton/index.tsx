import { Button, ButtonProps } from 'antd';
import { FC } from 'react';
import BaseAccess from '../BaseAccess';

const BaseButton: FC<ButtonProps & { code?: string | string[] }> = ({ code = '', ...props }) => {
  return (
    <BaseAccess code={code}>
      <Button {...props} />
    </BaseAccess>
  );
};

export default BaseButton;
