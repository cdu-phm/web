import { Spin } from 'antd';
import { FC, useEffect, useState } from 'react';
import styled from 'styled-components';

const Loading = styled.div`
  text-align: center;
`;
const BaseLoading: FC = (props) => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    const timeout = setTimeout(() => {
      setShow(true);
    }, 500);
    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return (
    <>
      {show && (
        <Loading>
          <Spin size='large'>{props.children}</Spin>
        </Loading>
      )}
    </>
  );
};

export default BaseLoading;
