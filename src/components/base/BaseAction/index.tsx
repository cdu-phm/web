import { LoadingOutlined } from '@ant-design/icons';
import { FC, HTMLAttributes, ReactNode } from 'react';
import styled from 'styled-components';
import BaseAccess from '../BaseAccess';

const Action = styled.a`
  display: inline-block;
  transition: all ease-out 0.3s;
  white-space: nowrap;
  &.is-disabled {
    opacity: 0.5;
    pointer-events: none;
  }
  &:hover {
    transform: scale(1.05);
  }
  &.blue {
    color: ${(props) => props.theme.blueColor};
  }
  &.gold {
    color: ${(props) => props.theme.goldColor};
  }
  &.green {
    color: ${(props) => props.theme.greenColor};
  }
  &.red {
    color: ${(props) => props.theme.redColor};
  }
`;
const defaultColor = ['blue', 'green', 'gold', 'red'];
const BaseAction: FC<
  HTMLAttributes<HTMLAnchorElement> & {
    icon?: ReactNode;
    title: string;
    color?: 'blue' | 'gold' | 'green' | 'red' | string;
    loading?: boolean;
    code?: string | string[];
  }
> = (props) => {
  const {
    icon = null,
    title,
    loading = false,
    className = '',
    color = 'blue',
    code = '',
    ...restProps
  } = props;
  const isDefaultColor = defaultColor.includes(color);
  return (
    <BaseAccess code={code}>
      <Action
        className={`${className} ${isDefaultColor ? color : ''} ${loading ? 'is-disabled' : ''}`}
        style={{ color: isDefaultColor ? undefined : color }}
        title={title}
        {...restProps}
      >
        {loading ? <LoadingOutlined /> : icon} <span>{title}</span>
      </Action>
    </BaseAccess>
  );
};

export default BaseAction;
