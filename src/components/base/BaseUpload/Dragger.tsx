import { apiUrl } from '@/config/config';
import useUserStore from '@/store/user';
import { InboxOutlined } from '@ant-design/icons';
import { Upload } from 'antd';
import { DraggerProps } from 'antd/lib/upload';
import { FC, ReactNode } from 'react';
import { beforeUpload, singleFileOnChange } from './utils';

const Dragger: FC<{
  action?: string;
  accept?: string;
  onChange?: (v: string) => void;
  title?: ReactNode;
  desc?: ReactNode;
  defaultValue?: string;
  value?: string;
  disabled?: boolean;
  data?: Record<string, any>;
}> = (props) => {
  const { onChange, action, title, desc, ...rest } = props;
  const { token } = useUserStore.getState();
  const realProps: DraggerProps = {
    action: `${apiUrl}${action}`,
    headers: { PHMToken: `${token}` },
    onChange(info) {
      singleFileOnChange(info, onChange);
    },
    beforeUpload: (file) => beforeUpload(file, props.accept),
    progress: {
      strokeColor: {
        '0%': '#108ee9',
        '100%': '#87d068'
      },
      strokeWidth: 8,
      format: (percent) => `${parseFloat((percent || 0).toFixed(2))}%`
    },
    ...rest
  };
  return (
    <Upload.Dragger {...realProps}>
      <p className='ant-upload-drag-icon'>
        <InboxOutlined />
      </p>
      <p className='ant-upload-text'>{title || '拖拽上传'}</p>
      <p className='ant-upload-hint'>{desc}</p>
    </Upload.Dragger>
  );
};
export default Dragger;
