import DraggerUpload from './Dragger';
import SigleUpload from './single';

type UploadType = typeof SigleUpload;
export interface BaseUploadInterface extends UploadType {
  Dragger: typeof DraggerUpload;
}
const BaseUpload = SigleUpload as BaseUploadInterface;
BaseUpload.Dragger = DraggerUpload;
export default BaseUpload;
