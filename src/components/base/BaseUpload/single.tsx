import { apiUrl, getRealFileUrl } from '@/config/config';
import useUserStore from '@/store/user';
import { DeleteOutlined, UploadOutlined } from '@ant-design/icons';
import { Button, Input, Upload } from 'antd';
import { CSSProperties, FC, ReactNode, useState } from 'react';
import styled from 'styled-components';
import { beforeUpload, singleFileOnChange } from './utils';

const UploadLayout = styled.div`
  /* display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center; */
`;
const UploadContent = styled.div`
  display: flex;
  width: 100%;
  && .ant-input-affix-wrapper {
    flex: 1;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
  }
  &&& .upload {
    flex-shrink: 0;
    .ant-btn {
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    }
  }
`;
export interface SingleUploadProps {
  accept?: string;
  onChange?: (v: string) => void;
  style?: CSSProperties;
  defaultValue?: string;
  value?: string;
  placeholder?: string;
  overlay?: ((url: string) => ReactNode) | ReactNode;
  showUrl?: boolean;
  preview?: (url: string) => ReactNode;
}

// const CopySvg = styled(CopyOutlined)`
//   :hover {
//     color: ${(props) => props.theme.primaryColor};
//   }
// `;
const SingleUpload: FC<SingleUploadProps> = (props) => {
  const {
    style,
    accept,
    value,
    onChange,
    showUrl = true,
    preview,
    placeholder = '请上传文件'
  } = props;
  const { token } = useUserStore.getState();
  const [url, setUrl] = useState(() => value ?? '');
  const realUrl = getRealFileUrl(url);
  return (
    <UploadLayout style={style}>
      <UploadContent>
        {showUrl && (
          <Input
            value={realUrl}
            readOnly
            placeholder={placeholder}
            // suffix={
            //   <Space>
            //     <CopyToClipboard
            //       text={realUrl}
            //       onCopy={() => {
            //         message.success('地址复制成功');
            //       }}
            //     >
            //       <CopySvg />
            //     </CopyToClipboard>
            //   </Space>
            // }
          />
        )}

        <Button className='upload'>
          <Upload
            showUploadList={false}
            action={`${apiUrl}/pc/file/upload`}
            headers={{ PHMToken: `${token}` }}
            accept={accept}
            beforeUpload={(file) => beforeUpload(file, props.accept)}
            onChange={(info) => {
              singleFileOnChange(info, (data) => {
                setUrl(data);
                onChange && onChange(data);
              });
            }}
          >
            <UploadOutlined />
            点击上传
          </Upload>
        </Button>
        {url && (
          <Button
            title='删除上传文件'
            onClick={() => {
              setUrl('');
              onChange && onChange('');
            }}
            icon={<DeleteOutlined />}
          />
        )}
      </UploadContent>
      {preview && preview(realUrl)}
    </UploadLayout>
  );
};

export default SingleUpload;
