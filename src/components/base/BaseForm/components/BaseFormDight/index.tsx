import { ProFormDigit } from '@ant-design/pro-form';
import { ProFormDigitProps } from '@ant-design/pro-form/lib/components/Digit';
import { FC } from 'react';

const BaseFormDight: FC<ProFormDigitProps> = (props) => {
  return (
    <ProFormDigit
      {...props}
      fieldProps={{
        max: 99999999,
        min: -99999999,
        type: 'number',
        ...props.fieldProps
      }}
    />
  );
};

export default BaseFormDight;
