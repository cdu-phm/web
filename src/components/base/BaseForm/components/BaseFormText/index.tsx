import BaseInput, { BaseInputProps } from '@/components/base/BaseInput';
import { ProFormField, ProFormText } from '@ant-design/pro-form';
import { ProFormFieldItemProps } from '@ant-design/pro-form/lib/interface';
import type { FC } from 'react';

const BaseFormText: FC<ProFormFieldItemProps<BaseInputProps>> & {
  Password: typeof ProFormText.Password;
} = (props) => {
  return (
    <ProFormField
      {...props}
      fieldProps={{ maxLength: 20, showCount: true, ...props.fieldProps }}
      renderFormItem={(_, fieldProps) => {
        return <BaseInput {...fieldProps} />;
      }}
    />
  );
};
BaseFormText.Password = ProFormText.Password;
export default BaseFormText;
