import BaseSelect, { BaseSelectProps } from '@/components/base/BaseSelect/index';
import { FormatOption } from '@/utils';
import { ProFormFieldItemProps } from '@ant-design/pro-form/lib/interface';
import { Form } from 'antd';
import { FC, ReactNode, useEffect } from 'react';

const BaseFormSelect: FC<
  ProFormFieldItemProps<Omit<BaseSelectProps, 'request'>> & {
    multiple?: BaseSelectProps['multiple'];
    request: BaseSelectProps['request'];
    hiddenItem?: boolean;
    hiddenRender?: () => ReactNode;
    actionRef?: BaseSelectProps['actionRef'];
  }
> = (props) => {
  const {
    fieldProps = {},
    placeholder = '请选择',
    request,
    disabled,
    hiddenItem,
    hiddenRender = () => null,
    readonly,
    multiple,
    params,
    actionRef,
    ...anyProps
  } = props;
  const { defaultSelectedOptions, fieldNames } = fieldProps;
  const activeFieldNames: FormatOption = { label: 'label', value: 'value', ...fieldNames };
  useEffect(() => {
    console.log('mount');
  }, []);
  return (
    <Form.Item {...anyProps}>
      {
        // eslint-disable-next-line no-nested-ternary
        readonly ? (
          <div>
            {defaultSelectedOptions && defaultSelectedOptions.length > 0
              ? defaultSelectedOptions
                  .filter((i) => i[activeFieldNames.value])
                  .map((i) => (
                    <span key={i[activeFieldNames.value]}>
                      {typeof activeFieldNames.label === 'function'
                        ? activeFieldNames.label(i)
                        : i[activeFieldNames.label]}
                    </span>
                  ))
              : '-'}
          </div>
        ) : hiddenItem ? (
          hiddenRender()
        ) : (
          <BaseSelect
            multiple={multiple}
            placeholder={placeholder}
            request={request}
            actionRef={actionRef}
            disabled={disabled}
            params={params}
            {...fieldProps}
          />
        )
      }
    </Form.Item>
  );
};
export default BaseFormSelect;
