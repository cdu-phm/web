import BaseFormDight from './BaseFormDight';
import BaseFormSelect from './BaseFormSelect';
import BaseFormText from './BaseFormText';
import BaseFormTextArea from './BaseFormTextArea';
import BaseFormUpload from './BaseFormUpload';

export { BaseFormDight, BaseFormSelect, BaseFormUpload, BaseFormText, BaseFormTextArea };
