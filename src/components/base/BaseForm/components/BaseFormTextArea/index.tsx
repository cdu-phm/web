import { ProFormField } from '@ant-design/pro-form';
import { ProFormFieldItemProps } from '@ant-design/pro-form/lib/interface';
import Input, { TextAreaProps } from 'antd/lib/input';
import type { FC } from 'react';

const BaseFormTextArea: FC<ProFormFieldItemProps<TextAreaProps>> = (props) => {
  return (
    <ProFormField
      {...props}
      fieldProps={{ rows: 4, maxLength: 240, showCount: true, ...props.fieldProps }}
      renderFormItem={(_, { onChange, ...fieldProps }) => {
        return (
          <Input.TextArea
            {...fieldProps}
            onChange={(e) => {
              const { value } = e.target;
              const realValue = value ? String(value).trim() : value;
              e.target.value = realValue;
              onChange && onChange(realValue);
            }}
          />
        );
      }}
    />
  );
};
export default BaseFormTextArea;
