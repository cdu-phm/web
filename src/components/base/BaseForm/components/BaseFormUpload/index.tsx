import BaseUpload from '@/components/base/BaseUpload';
import { SingleUploadProps } from '@/components/base/BaseUpload/single';
import { ProFormFieldItemProps } from '@ant-design/pro-form/lib/interface';
import { Form } from 'antd';
import { FC } from 'react';

const BaseFormUpload: FC<ProFormFieldItemProps<SingleUploadProps> & { accept?: string }> = (
  props
) => {
  const { fieldProps, accept, ...anyProps } = props;
  return (
    <Form.Item extra='文件大小不可超过100M' {...anyProps}>
      <BaseUpload accept={accept} {...fieldProps} />
    </Form.Item>
  );
};
export default BaseFormUpload;
