import type { ModalFormProps } from '@ant-design/pro-form';
import ProForm, { ModalForm } from '@ant-design/pro-form';
import { Alert } from 'antd';
import { useRef, useState } from 'react';
import type { DraggableEventHandler } from 'react-draggable';
import Draggable from 'react-draggable';
import BaseModal from '../BaseModal';
// 导出proform的所有组件
export * from '@ant-design/pro-form';
// 导出自定义的组件
export * from './components';
/**
 * BaseModalForm封装了modal和基础的form信息，可快速实现modal形式的表单
 */
export const BaseModalForm = <T extends Record<string, any>>({
  submitter = {},
  ...props
}: ModalFormProps<T> & { showConfirm?: boolean; onCancel?: () => void }) => {
  const [bounds, setBounds] = useState({
    left: 0,
    top: 0,
    bottom: 0,
    right: 0
  });
  const [disabled, setDisabled] = useState(true);
  const draggleRef = useRef<HTMLDivElement>(null);

  const onStart: DraggableEventHandler = (event, uiData) => {
    const { clientWidth, clientHeight } = window?.document?.documentElement;
    const targetRect = draggleRef?.current?.getBoundingClientRect();
    if (targetRect) {
      setBounds({
        left: uiData?.x - targetRect?.left,
        right: clientWidth - (targetRect?.right - uiData?.x),
        top: uiData?.y - targetRect?.top,
        bottom: clientHeight - (targetRect?.bottom - uiData?.y)
      });
    }
  };
  const onVisibleChange = (visible: boolean) => {
    if (!visible) {
      if (props.showConfirm) {
        BaseModal.confirm({
          content: '您的数据暂无保存，关闭后数据将会重置，确认要关闭吗',
          onOk: props.onCancel
        });
      } else {
        props.onCancel && props.onCancel();
      }
    }
  };
  return (
    <ModalForm
      layout='horizontal'
      {...props}
      submitter={
        submitter
          ? {
              ...submitter,
              render: (props, dom) => {
                dom.reverse();
                const { render } = submitter;
                return render ? render(props, dom) : dom;
              }
            }
          : submitter
      }
      onVisibleChange={onVisibleChange}
      modalProps={{
        keyboard: false,
        maskClosable: false,
        ...props.modalProps,
        title: (
          <div
            style={{ width: '100%', cursor: 'move' }}
            onMouseOver={() => setDisabled(false)}
            onMouseOut={() => setDisabled(true)}
          >
            {props.title || <Alert type='warning' message='请补充弹窗标题' />}
          </div>
        ),
        modalRender: (modal) => {
          return (
            <Draggable onStart={onStart} bounds={bounds} disabled={disabled}>
              <div ref={draggleRef}>{modal}</div>
            </Draggable>
          );
        }
      }}
    />
  );
};
/**
 * 导出ProForm
 * @param props
 * @returns
 */
const BaseForm: typeof ProForm = (props) => {
  return <ProForm omitNil={false} submitter={false} {...props} />;
};
BaseForm.Group = ProForm.Group;
BaseForm.useForm = ProForm.useForm;
BaseForm.Item = ProForm.Item;

export default BaseForm;
