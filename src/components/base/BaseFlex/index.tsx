import type { CSSProperties } from 'styled-components';
import styled from 'styled-components';

export interface FlexProps {
  justify?: CSSProperties['justifyContent'];
  align?: CSSProperties['alignItems'];
  width?: number | string;
  gutter?: number | string;
}

const Flex = styled.div<FlexProps>`
  display: flex;
  align-items: ${(props) => props.align};
  justify-content: ${(props) => props.justify};
  width: ${(props) => (typeof props.width === 'number' ? `${props.width}px` : props.width)};
  row-gap: ${(props) => (typeof props.gutter === 'number' ? `${props.gutter}px` : props.gutter)};
`;

export interface FlexItemProps {
  justify?: CSSProperties['justifySelf'];
  align?: CSSProperties['alignSelf'];
  flex?: number | 'none' | 'auto' | string;
  grow?: number;
  shrink?: number;
  basis?: string | number;
}
const Item = styled.div<FlexItemProps>`
  flex: ${(props) => props.flex};
  flex-basis: ${(props) => (typeof props.basis === 'number' ? `${props.basis}px` : props.basis)};
  flex-grow: ${(props) => props.grow};
  flex-shrink: ${(props) => props.shrink};
  align-self: ${(props) => props.align};
  justify-self: ${(props) => props.justify};
`;

const BaseFlex: typeof Flex = Flex;
(BaseFlex as typeof Flex & { Item: typeof Item }).Item = Item;

export default BaseFlex as typeof Flex & { Item: typeof Item };
