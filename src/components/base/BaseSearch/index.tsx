import { ConfigProvider } from 'antd';
import type { SizeType } from 'antd/lib/config-provider/SizeContext';
import Search from 'antd/lib/input/Search';
import { debounce as fn } from 'lodash-es';
import type { ChangeEvent, CSSProperties, FC, ReactNode } from 'react';
import { useContext } from 'react';

export interface BaseSearchProps {
  defaultValue?: string;
  value?: string;
  allowClear?: boolean;
  placeholder?: string;
  onChange?: (value: string) => void;
  enterButton?: ReactNode;
  debounceTime?: number;
  loading?: boolean;
  style?: CSSProperties;
  size?: SizeType;
}

const BaseSearch: FC<BaseSearchProps> = (props: BaseSearchProps) => {
  const sizeContext = useContext(ConfigProvider.SizeContext);
  const { onChange, debounceTime, size = sizeContext, ...anyProps } = props;
  const handleChange = fn((val: string) => {
    onChange && onChange(val);
  }, debounceTime ?? 500);
  const onchange = (e: ChangeEvent<HTMLInputElement>) => {
    handleChange(e.target.value);
  };
  return (
    <Search
      placeholder='请输入搜索内容'
      size={size}
      maxLength={50}
      allowClear={true}
      {...anyProps}
      onChange={onchange}
      onSearch={onChange}
    />
  );
};

export default BaseSearch;
