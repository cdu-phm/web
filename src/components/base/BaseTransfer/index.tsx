import type { TransferProps } from 'antd';
import { Transfer } from 'antd';
import type { ListStyle, TransferItem } from 'antd/lib/transfer';
import type { CSSProperties } from 'react';
import styled from 'styled-components';

export type BaseTransferProps<T> = Omit<TransferProps<T>, 'listStyle'> & {
  listStyle?: ((style: ListStyle) => CSSProperties) | CSSProperties;
  height?: string | number;
};
const TransferWrap = styled(Transfer as any)`
  .ant-transfer-list {
    flex: 1 1 auto;
    height: ${({ height }) => (typeof height === 'number' ? `${height}px` : height)};
  }
`;
const BaseTransfer = <T extends TransferItem>(props: BaseTransferProps<T>) => {
  return <TransferWrap height={300} {...props} />;
};

export default BaseTransfer;
