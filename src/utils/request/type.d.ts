import type { Page } from '@/models';
import type { AxiosError, AxiosRequestConfig, AxiosResponse, Method } from 'axios';
import type { ReactNode } from 'react';

export interface ResultError {
  message: string;
  name: 'user' | 'system';
}
export interface ResponseResult<T> {
  err: ResultError | null;
  data: T;
  ok: boolean;
}
export interface RequestConfig<T> extends AxiosRequestConfig {
  confirm?: { type?: 'warning' | 'error'; content: string | ReactNode };
  success?: string | { message: string; duration?: number; callback?: () => void };
  error?:
    | string
    | { message: string; duration?: number }
    | ((err: { message: string; name: 'user' | 'system' }) => void)
    | boolean;
  loading?: boolean;
  onConfirm?: (bool: boolean) => void;
  onSuccess?: (data: T, response: AxiosResponse) => void;
  onError?: (err: ResultError, resError: AxiosResponse | AxiosError) => void;
}

export interface CreateRequestConfig extends AxiosRequestConfig {
  requestInterceptor?: (requestConfig: AxiosRequestConfig) => AxiosRequestConfig;
  successInterceptor?: (
    res: ResponseResult<unknown>,
    response: AxiosResponse,
    option: RequestConfig<unknown>
  ) => ResponseResult<unknown>;
  errorInterceptor?: (
    res: ResponseResult<unknown>,
    error: AxiosError,
    option: RequestConfig<unknown>
  ) => ResponseResult<unknown>;
}

export type Result<T> = Promise<ResponseResult<T>>;

export type RequestMethod = <T>(url: string, data?: unknown, opt?: RequestConfig<T>) => Result<T>;

export interface Request {
  <T>(url: string, method: Method, data?: unknown, opt?: RequestConfig<T>): Result<T>;
  list: <T = Record<string, unknown>>(
    url: string,
    data?: unknown,
    opt?: RequestConfig<T>
  ) => Result<Page<T>>;
  get: RequestMethod;
  post: RequestMethod;
  put: RequestMethod;
  delete: RequestMethod;
  patch: RequestMethod;
}

type Service<R, P extends any[]> = (...args: P) => Promise<ResponseResult<R>> | Promise<R>;
