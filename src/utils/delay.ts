/**
 *
 * @param d
 * @returns
 */
export function delay(d: number) {
  return new Promise((r) => setTimeout(r, d));
}
/**
 *
 * @param promise
 * @param minimumDelay
 * @param param2
 * @returns
 */
export async function minDelay(
  promise: Promise<any>,
  minimumDelay = 200,
  { delayRejection }: { delayRejection: boolean } = { delayRejection: true }
) {
  const fn = delayRejection ? Promise.allSettled : Promise.all;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  await (fn as any)([promise, delay(minimumDelay)]);
  return promise;
}
