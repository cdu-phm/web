/* eslint-disable @typescript-eslint/ban-types */
/**
 * 判断变量是否是一个方法
 * @param {any} obj 需要验证的变量
 * @returns {boolean} 判断结果
 */
export function isFunction(obj: any): obj is Function {
  return typeof obj === 'function';
}
/**
 * 判断变量是否是 undefined
 * @param {T} data 需要验证的变量
 * @returns {boolean} 判断结果
 */
export function isUndefined<T>(data: T | undefined): data is undefined {
  return typeof data === 'undefined';
}
/**
 * 判断变量是否是 null
 * @param {T} data 需要验证的变量
 * @returns {boolean} 判断结果
 */
export function isNull<T>(data: T | null): data is null {
  return data === null;
}
/**
 * 判断变量是否是 undefined|null
 * @param {T} data 需要验证的变量
 * @returns {boolean} 判断结果
 */
export function isEmpty<T>(data: T): boolean {
  return isUndefined(data) || isNull(data);
}
/**
 * 是否是生产环境
 */
export const isProd = import.meta.env.PROD === true;
