/**
 * 根据长度创建一个相应长度的数组
 * @param {number} len 数组长度
 * @param {number} start 起始数组下标
 * @param {function} formatter 数组元素格式化方法
 * @returns {array} 结果数组
 */
export function range<T = number>(len: number, start = 0, formatter?: (num: number) => T) {
  const arr: T[] = [];
  for (let i = start; i < len + start; i += 1) {
    arr.push((formatter ? formatter(i) : i) as T);
  }
  return arr;
}
