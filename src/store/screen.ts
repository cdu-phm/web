import moment from 'moment';
import create from 'zustand';

export interface ScreenStore {
  time: string;
  score: number;
  setScore: (score: number) => void;
  setTime: (payload: string) => void;
}

/**
 * Screen store
 */
const useScreenStore = create<ScreenStore>((set) => ({
  time: moment().format('yyyy-MM-DD HH:mm:ss'),
  score: 0,
  setTime(payload: string) {
    set({ time: payload });
  },
  setScore(score: number) {
    set({ score });
  }
}));
export default useScreenStore;
