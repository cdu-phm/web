import create from 'zustand';
import { persist } from 'zustand/middleware';

export interface ThemeStore {
  activeTheme: string;
  setTheme: (payload: string) => void;
}

/**
 * 主题 store
 */
const useThemeStore = create<ThemeStore>(
  persist(
    (set) => ({
      activeTheme: 'blue',
      setTheme(payload: string) {
        set({ activeTheme: payload });
      }
    }),
    { name: 'theme' }
  )
);
export default useThemeStore;
