import create from 'zustand';
import { persist } from 'zustand/middleware';

interface RouteMeta {
  title: string;
}
interface MatchRoute {
  path: string;
  name: string;
  meta?: RouteMeta;
}

export type CacheTab = {
  title: string;
  pathname: string;
  query?: Record<string, unknown>;
  params?: Record<string, unknown>;
  closable?: boolean;
};
interface TabsStore {
  activeTabs: string[];
  matchedRoutes: MatchRoute[];
  setMatchRoutes: (matchedR: MatchRoute[]) => void;
  setActiveTab: (activeTabs: string[]) => void;
}

/**
 * tab页缓存 store
 */
const useTabStore = create<TabsStore>(
  persist(
    (set) => ({
      activeTabs: [],
      matchedRoutes: [],
      setMatchRoutes(matchedR: MatchRoute[]) {
        set({ matchedRoutes: matchedR });
      },
      setActiveTab(activeTabs: string[]) {
        set({ activeTabs });
      }
    }),
    {
      name: 'tab'
    }
  )
);

export default useTabStore;
