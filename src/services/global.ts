import { PageParams } from '@/models';
import { history } from '@/router';
import { UserInfo } from '@/store/user';
import { request } from '@/utils/request';

export interface UpdatePasswordParams {
  confirmPassword: string;
  newPassword: string;
  oldPassword: string;
}

/**
 * 根据用户id修改用户密码
 * @param data
 * @returns
 */
export function updateUserPassword(data: UpdatePasswordParams) {
  return request.put('/pc​/user​/v1​/users/update', data, {
    confirm: { content: '确认修改密码' },
    success: {
      message: '密码修改成功,即将跳转至登录页面',
      callback() {
        history.replace('/logout');
      }
    }
  });
}

/**
 * 获取当前登录用户信息
 * @param id
 * @returns
 */
export function getUserInfo() {
  return request.get<UserInfo>('/pc​/user​/v1​/users/userInfo');
}

export interface messageCount {
  unreadCount: number;
}
/**
 * 查询未读消息数量
 * @returns
 */
export function getMessageCount() {
  // return request.get<number>('/pc/user/v1/sysMessagesCount');
  return request.get<messageCount>('/pc/user/v1/sysMessagesCount');
}

/**
 * 查询消息
 * @returns
 */
export function getMessages(params: PageParams<{ status?: string }>) {
  return request.list<Record<string, any>>('​/pc​/user​/v1​/sysMessages', params);
}

/**
 * 读取消息
 * @returns
 */
export function readMessage(id: string) {
  return request.get('​/pc​/user​/v1​/sysMessagesSetRead', { id });
}

/**
 * 读取消息
 * @returns
 */
export function readAllMessage() {
  return request.get('​/pc/user/v1/sysMessagesSetAllRead', {}, { success: '消息已全部设置为已读' });
}
