import { css } from 'styled-components';

const CardStyle = css`
  .ant-card {
    .ant-card-head {
      min-height: auto;
      padding-left: 15px;
      border-bottom: 0;
      .ant-card-head-title {
        padding: 8px 0;
        font-weight: 600;
      }
    }
  }
`;

export default CardStyle;
