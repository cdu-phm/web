import { css } from 'styled-components';

const RadioStyle = css`
  .ant-radio-checked {
    .ant-radio-inner {
      border-color: ${(props) => props.theme.primaryColor};
      &::after {
        background-color: ${(props) => props.theme.primaryColor};
      }
    }
  }
  .ant-radio-group {
    &.ant-radio-group-solid
      .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
      background: ${(props) => props.theme.primaryColor};
      border-color: ${(props) => props.theme.primaryColor};
    }
  }
`;
export default RadioStyle;
