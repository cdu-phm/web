import { css } from 'styled-components';

const InputStyle = css`
  .ant-input {
    border-color: ${(props) => props.theme.weakenBorderColor};
    &:focus,
    &-focused,
    &:hover {
      border-color: ${(props) => props.theme.primaryColor};
    }
  }
`;
export default InputStyle;
