import { css } from 'styled-components';

const SpaceStyle = css`
  .ant-space-vertical {
    margin-bottom: 0 !important;
  }
`;
export default SpaceStyle;
