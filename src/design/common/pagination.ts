import { css } from 'styled-components';

const PaginationStyle = css`
  .ant-pagination-total-text {
    order: 100;
    margin-left: 8px;
  }
`;
export default PaginationStyle;
