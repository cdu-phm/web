import { css } from 'styled-components';

const TableStyle = css`
  .ant-table {
    td,
    th {
      border-radius: 0 !important;
    }
    &&.ant-table-bordered > .ant-table-container {
      border-top: 0;
      border-left: 0;
    }
    & &-body {
      overflow-y: auto !important;
    }
    & table &-tbody {
      & > tr {
        height: 40px;
        & > td {
          height: 40px;
          padding: 4px 8px 3px;
        }
      }
      & > tr:not(.ant-table-measure-row):not(.ant-table-row-selected) {
        & > td {
          border-color: ${(props) => props.theme.weakenBorderColor} !important;
          &.ant-table-cell-fix-left,
          &.ant-table-cell-fix-right,
          &:first-child {
            border-left: 1px solid ${(props) => props.theme.weakenBorderColor};
          }
        }
        &.odd {
          background-color: ${(props) => props.theme.lightBackground};
          & > td {
            background-color: ${(props) => props.theme.lightBackground};
          }
        }
      }
    }
  }
`;

export default TableStyle;
