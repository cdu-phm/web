import { css } from 'styled-components';

const TabsStyle = css`
  .ant-tabs {
    &&&.ant-tabs-top > .ant-tabs-nav {
      margin-bottom: 0;
    }
    &&& &-tab {
      color: ${(props) => props.theme.nomalBackground};
      background: ${(props) => props.theme.weakenBorderColor};
      &-with-remove {
        .ant-tabs-tab-btn {
          position: relative;
          left: 5px;
        }
      }
      &-active {
        background: ${(props) => props.theme.primaryColor} !important;
        .ant-tabs-tab-btn,
        .ant-tabs-tab-remove {
          color: ${(props) => props.theme.nomalBackground} !important;
        }
      }
      &-remove {
        float: right;
        padding: 0;
        opacity: 0;
        &:hover {
          transform: scale(1.2);
        }
      }
      &:hover {
        .ant-tabs-tab-remove {
          opacity: 1;
        }
      }
    }
    && &-nav {
      &::before {
        border-bottom: none;
      }
    }
  }
`;

export default TabsStyle;
