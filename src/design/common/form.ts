import { css } from 'styled-components';

const FormStyle = css`
  .ant-form {
    &-item {
      &&-has-error {
        .ant-input,
        .ant-input-affix-wrapper,
        .ant-input:hover,
        .ant-input-affix-wrapper:hover,
        .ant-select:not(.ant-select-disabled):not(.ant-select-customize-input)
          .ant-select-selector {
          border-color: ${(props) => props.theme.dangerColor} !important;
        }
        /* input::placeholder,
        .ant-select-selection-placeholder {
          color: ${(props) => props.theme.dangerColor};
        } */
      }
      & &-explain-error {
        color: ${(props) => props.theme.dangerColor};
      }
    }
    .ant-row.ant-form-item {
      flex-wrap: nowrap;
    }
    .ant-col.ant-form-item-label {
      flex-shrink: 0;
    }
    .ant-col.ant-form-item-control {
      flex-wrap: nowrap;
      width: 0;
    }
  }
`;

export default FormStyle;
