import { css } from 'styled-components';

const TransferStyle = css`
  .ant-transfer {
    display: flex;
    justify-content: center;
    &-operation {
      .ant-btn {
        width: 40px;
        height: 40px;
        span {
          transform: scale(1.5);
        }
        &:first-child {
          margin-bottom: 10px;
        }
      }
    }
    &-list {
      overflow: hidden;
      &-body-customize-wrapper {
        flex: 1;
        overflow: hidden;
      }
    }
  }
`;

export default TransferStyle;
