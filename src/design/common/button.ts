import { css } from 'styled-components';

const ButtonStyle = css`
  &&-link {
    color: ${(props) => props.theme.primaryColor};
  }
`;
export default ButtonStyle;
