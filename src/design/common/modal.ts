import { css } from 'styled-components';

const ModalStyle = css`
  .ant-modal-confirm-btns {
    display: flex;
    flex-direction: row-reverse;
    gap: 8px;
  }
  .ant-modal-content {
    display: flex;
    flex-direction: column;
    max-height: 95vh;
    .ant-modal-body {
      overflow: auto;
      flex: 1;
    }
  }
`;

export default ModalStyle;
