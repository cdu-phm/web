import type { DefaultTheme } from 'styled-components';
import { createGlobalStyle } from 'styled-components';
import ButtonStyle from './common/button';
import CardStyle from './common/card';
import GridStyle from './common/col';
import DescriptionsStyle from './common/descriptions';
import FormStyle from './common/form';
import InputStyle from './common/input';
import InputNumberStyle from './common/inputNumber';
import ModalStyle from './common/modal';
import PaginationStyle from './common/pagination';
import RadioStyle from './common/radio';
import SpaceStyle from './common/space';
import TableStyle from './common/table';
import TabsStyle from './common/tabs';
import TransferStyle from './common/transfer';

export const themes: Record<string, () => Promise<{ default: DefaultTheme }>> = {
  blue: () => import('./theme/blue'),
  green: () => import('./theme/green')
};
const GlobalStyle = createGlobalStyle`
  * {
    font-family: 'Roboto Mono Medium for Powerline' !important;
  }

  html {
    min-width: 1280px;
    min-height: 720px;
    width:100vw;
    height: 100vh;
    body {
      background: #f5f7fa;
    }
    .ant-layout {
      /* background: transparent; */
    }

  }
  ${GridStyle}
  ${CardStyle}
  ${InputStyle}
  ${ButtonStyle}
  ${DescriptionsStyle}
  ${FormStyle}
  ${TransferStyle}
  ${TableStyle}
  ${RadioStyle}
  ${InputNumberStyle}
  ${SpaceStyle}
  ${PaginationStyle}
  ${TabsStyle}
  ${ModalStyle}

  ::-webkit-scrollbar {
      width: 6px;
      height: 6px;
      border-radius: 3px;
  }

  ::-webkit-scrollbar-track {
    background: ${(props) => props.theme.lightBackground};
  }

  ::-webkit-scrollbar-thumb {
    background: #3332;
  }

  ::-webkit-scrollbar-thumb:hover {
    background:${(props) => props.theme.nomalTextColor};
  }
  .ant-modal-content {
    max-height: 90vh;
  }

`;
export default GlobalStyle;
