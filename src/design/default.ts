import { presetPalettes } from '@ant-design/colors';

const gray1 = '#333';
const gray2 = '#666';
const gray4 = '#f0f0f0';
const gray5 = '#eee';
const white1 = '#fff';
const white2 = '#f5f7fa';
const white3 = '#fafafa';
const red1 = '#F66F6A';

const defaultTheme = {
  goldColor: presetPalettes.gold.primary as string,
  blueColor: presetPalettes.blue.primary as string,
  greenColor: presetPalettes.green.primary as string,
  redColor: presetPalettes.red.primary as string,
  importantTextColor: gray1,
  nomalTextColor: gray2,
  borderColor: gray5,
  dangerColor: red1,
  weakenBorderColor: gray4,
  nomalBackground: white1,
  lightBackground: white2,
  siderBackground: white3,
  borderRadius: '2px'
};
export default defaultTheme;
