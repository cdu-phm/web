import type { DefaultTheme } from 'styled-components';
import defaultTheme from '../default';

const theme: DefaultTheme = {
  primaryColor: '#1890ff',
  ...defaultTheme
};
export default theme;
