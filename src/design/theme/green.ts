import type { DefaultTheme } from 'styled-components';
import defaultTheme from '../default';

const theme: DefaultTheme = {
  primaryColor: '#237804',
  ...defaultTheme
};
export default theme;
