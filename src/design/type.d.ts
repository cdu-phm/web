import 'styled-components';

// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    primaryColor: string;
    goldColor: string;
    blueColor: string;
    greenColor: string;
    redColor: string;
    importantTextColor: string;
    nomalTextColor: string;
    dangerColor: string;
    borderColor: string;
    weakenBorderColor: string;
    lightBackground: string;
    siderBackground: string;
    nomalBackground: string;
    borderRadius: string;
  }
}
