import 'umi-request';

declare module 'umi-request' {
  interface RequestOptionsInit {
    success?: { message: string; duration?: number };
    error?: { message: string; duration?: number } | false;
  }
}
