import { History } from 'history';

export type PromiseFn<T> = (...args: any) => Promise<T>;

export type PickTypeByKey<U extends Record<string, any>, K extends keyof U> = U[K];

export type ArrayItemType<U extends any[]> = U[number];

export type ObjectValueType<D extends Record<string | number | symbol, unknown>> = D[keyof D];

declare global {
  interface Window {
    reactHistory: History<unknown>;
    setPage: () => void;
    fileSave: (...data: any) => void;
    fileSaveSupported: boolean;
    APP_INFO: {
      pkg: {
        name: string;
        version: string;
        dependencies: Recordable<string>;
        devDependencies: Recordable<string>;
      };
      lastBuildTime: string;
    };
  }
}
