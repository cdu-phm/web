import { MockMethod } from 'vite-plugin-mock';

export default [
  {
    url: '/api/login',
    method: 'post',
    response: () => {
      return {
        code: 200,
        message: '登录成功',
        data: {
          token: '123',
          userinfo: {
            userId: '123',
            userName: '123'
          }
        }
      };
    }
  }
] as MockMethod[];
