import { uniqueId } from 'lodash-es';
import { MockMethod } from 'vite-plugin-mock';

const list = [];
export default [
  {
    url: '/api/userinfo/list',
    method: 'get',
    response: ({ query }) => {
      const { pageNum, pageSize } = query;
      const len = pageNum * pageSize;
      return {
        code: 200,
        message: '查询成功',
        data: {
          list: list.slice(len - pageSize, len),
          rowCount: list.length,
          pageNum,
          pageSize
        }
      };
    }
  },
  {
    url: '/api/userinfo/add',
    method: 'post',
    response: ({ body }) => {
      const id = uniqueId();
      list.push({ id, ...body, created: new Date() });
      return {
        code: 200,
        message: '添加成功'
      };
    }
  },
  {
    url: '/api/userinfo/edit',
    method: 'put',
    response: ({ body }) => {
      const { id } = body;
      const index = list.findIndex((i) => i.id === id);
      const info = list[index];
      list.splice(index, 1, { ...info, ...body });
      return {
        code: 200,
        message: '编辑成功'
      };
    }
  },
  {
    url: '/api/userinfo/del',
    method: 'delete',
    response: ({ body }) => {
      const { id } = body;
      const index = list.findIndex((i) => i.id === id);
      list.splice(index, 1);
      return {
        code: 200,
        message: '添加成功'
      };
    }
  }
] as MockMethod[];
