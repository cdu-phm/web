import { uniqueId } from 'lodash-es';
import { MockMethod } from 'vite-plugin-mock';

const list = [{ id: '1', name: '角色1', remark: '角色1', created: new Date() }];
export default [
  {
    url: '/api/roleinfo/list',
    method: 'get',
    response: ({ query }) => {
      const { pageNum, pageSize } = query;
      const len = pageNum * pageSize;
      return {
        code: 200,
        message: '查询成功',
        data: {
          list: list.slice(len - pageSize, len),
          rowCount: list.length,
          pageNum,
          pageSize
        }
      };
    }
  },
  {
    url: '/api/roleinfo/all',
    method: 'get',
    response: () => {
      return {
        code: 200,
        message: '查询成功',
        data: list
      };
    }
  },
  {
    url: '/api/roleinfo/add',
    method: 'post',
    response: ({ body }) => {
      const { name, remark } = body;
      const id = uniqueId();
      list.push({ id, name, remark, created: new Date() });
      return {
        code: 200,
        message: '添加成功'
      };
    }
  },
  {
    url: '/api/roleinfo/edit',
    method: 'put',
    response: ({ body }) => {
      const { id, name, remark } = body;
      const index = list.findIndex((i) => i.id === id);
      const info = list[index];
      list.splice(index, 1, { ...info, id, name, remark });
      return {
        code: 200,
        message: '编辑成功'
      };
    }
  },
  {
    url: '/api/roleinfo/del',
    method: 'delete',
    response: ({ body }) => {
      const { id } = body;
      const index = list.findIndex((i) => i.id === id);
      list.splice(index, 1);
      return {
        code: 200,
        message: '添加成功'
      };
    }
  }
] as MockMethod[];
