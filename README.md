# phm-web

## 主要依赖

在了解html、css、javascript的基础上，
请在代码查看和编写前了解以下依赖

[ECMAScript](http://caibaojian.com/es6/)<br/>
[pnpm](https://pnpm.io/zh/)<br/>
[vite](https://cn.vitejs.dev/)<br/>
[react](https://react.docschina.org/)<br/>
[react-router](https://reactrouter.com/web)<br/>
[typescript](https://www.typescriptlang.org/)<br/>
[zustand](https://github.com/pmndrs/zustand)<br/>
[styled-components](https://styled-components.com/)<br/>
[antd](https://ant.design/index-cn)<br/>
[pro-components](https://procomponents.ant.design/)<br/>
[socket.io](https://socket.io/)<br/>
[ahooks](https://ahooks.js.org)<br/>
[lodash-es](https://www.lodashjs.com/)<br/>

## Getting Started

Install dependencies,

```bash
$ pnpm i
```

Start the dev serverpnpm i

```bash
$ pnpm dev
```

## 注意

如果开发过程中遇到 vscode自动补全不生效的情况，请在ts/tsx文件下， 使用 `Ctrl+Shift+P` 唤出工具栏，输入 TypeScript: Restart TS Server 重启ts服务
