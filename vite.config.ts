// import babel from '@rollup/plugin-babel';
import legacy from '@vitejs/plugin-legacy';
import reactRefresh from '@vitejs/plugin-react-refresh';
import moment from 'moment';
import { resolve } from 'path';
import { ConfigEnv, defineConfig } from 'vite';
import compressPlugin from 'vite-plugin-compression';
import { viteMockServe } from 'vite-plugin-mock';
import svgrPlugin from 'vite-plugin-svgr';
import reactJsx from 'vite-react-jsx';
import pkg from './package.json';

const { dependencies, devDependencies, name, version } = pkg;
// eslint-disable-next-line no-underscore-dangle
const APP_INFO = {
  pkg: { dependencies, devDependencies, name, version: `MRPHMP V${version}` },
  lastBuildTime: moment().format('YYYY-MM-DD HH:mm:ss')
};
// https://vitejs.dev/config/
export default ({ mode }: ConfigEnv) => {
  const isProd = mode === 'production';
  return defineConfig({
    define: {
      APP_INFO: JSON.stringify(APP_INFO)
    },
    plugins: [
      reactRefresh(),
      reactJsx(),
      svgrPlugin(),
      // isProd && babel({ babelHelpers: 'bundled' }),
      isProd && legacy(),
      // isProd &&
      //   visualizer({
      //     filename: './node_modules/.cache/visualizer/stats.html',
      //     open: true,
      //     gzipSize: true,
      //     brotliSize: true
      //   }),
      viteMockServe({
        // default
        mockPath: 'mock'
      }),
      // vitePluginImp({
      //   libList: [
      //     {
      //       libName: 'antd',
      //       style: (name) => `antd/es/${name}/style`,
      //       libDirectory: 'es'
      //     }
      //   ]
      // }),
      compressPlugin({
        ext: '.gz'
      })
    ],
    server: {
      port: 8888
    },
    build: {
      target: ['es2015'],
      rollupOptions: {
        external: ['/public/brower-file.js']
      }
    },
    resolve: {
      alias: [
        { find: '@', replacement: resolve(__dirname, 'src') },
        { find: '@com', replacement: resolve(__dirname, 'src/components') },
        { find: /^~/, replacement: '' }
      ]
    },
    css: {
      preprocessorOptions: {
        less: {
          // 支持内联 JavaScript
          javascriptEnabled: true
        }
      }
    }
  });
};
